﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OPENTK
{
    public partial class Notification : Form
    {
       public int mspassed = 0;
      public  MessageBoxButtons buttonState = MessageBoxButtons.OK;
      Form1 mainWindow;
        public Notification(Form1 _mainWindow)
        {
            InitializeComponent();
           // pictureBox1.Image = Bitmap.FromHicon(icon);
           // label1.Text = Title;
           // label2.Text = Content;
           //// timer1.Enabled = true;
           // this.TopLevel = false;
           // mainWindow = _mainWindow;
           // timer1.Enabled = true;
            this.StartPosition = FormStartPosition.Manual;
            this.TopMost = true;
            mainWindow = _mainWindow;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (mspassed <= 100)
            {
                mspassed += 1;
                this.Opacity += .01;
            }
            else if(mspassed > 100 && mspassed <= 300)
            {
                if(buttonState != MessageBoxButtons.YesNo)
                mspassed += 1;
            }
            else if(mspassed > 300 && mspassed <= 400)
            {
                mspassed += 1;
                this.Opacity -= .01;
            }
            else if(mspassed > 400)
            {
                
                timer1.Enabled = false;
                mspassed = 0;
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mainWindow.NotificationResult = "No";
            this.Close();
        }



        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if(this.buttonState == MessageBoxButtons.OK)
            {
                button3.Text = "OK";
                button2.Visible = false;
            }
            else if(this.buttonState == MessageBoxButtons.YesNo)
            {
                button3.Text = "No";
                button2.Text = "Yes";
                button2.Visible = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.buttonState == MessageBoxButtons.YesNo)
            {
                mainWindow.NotificationResult = "Yes";
            }
            this.Close();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (this.buttonState == MessageBoxButtons.OK)
            {
                mainWindow.NotificationResult = "OK";
            }
            else if (this.buttonState == MessageBoxButtons.YesNo)
            {
                mainWindow.NotificationResult = "No";
            }
            this.Close();
        }

        private void Notification_MouseEnter(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            mainWindow.GLControlInvalidate();
        }

        private void Notification_MouseLeave(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            mspassed = 100;
            this.Opacity = 100;
            mainWindow.GLControlInvalidate();
        }

    }
}

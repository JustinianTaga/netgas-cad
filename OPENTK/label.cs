﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace OPENTK
{
    public class label
    {
        public string Text;
        public float X, Y, auxX, auxY;
        public bool MarkedLabel = true;
        public Font font = null;
        public bool Static = false;
        public int Index;
        public int nodeAssignedIndex = -1, pipeAssignedIndex = -1;
        public int assignmentStartPosition = 1;
        public label(int _Index, string _Text, float _X, float _Y)
        {
            Index = _Index;
            Text = _Text;
            X = _X;
            Y = _Y;
            auxX = _X;
            auxY = _Y;
        }
    }
}

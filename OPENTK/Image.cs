﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OPENTK
{
    public class Image
    {
        public int Index;
        public string FileName;
        public float X, Y, auxX, auxY, Width, Height;
        public bool Border = false;
        public bool Static = true;
        public Image(int _Index, string _FileName, float _X, float _Y, float _Width, float _Height)
        {
            auxX = _X;
            auxY = _Y;
            Index = _Index;
            FileName = _FileName;
            X = _X;
            Y = _Y;
            Width = _Width;
            Height = _Height;
        }
    }
}

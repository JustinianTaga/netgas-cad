﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Threading;
using OpenTK.Input;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Collections;
namespace OPENTK
{
    public partial class Form1 : Form
    {
        #region Fields & Properties
        #region Lists
        public List<string> Subnetworks;
        public List<Image> Images;
        public List<Node> Nodes { get; private set; }
        public List<Pipe> Pipes { get; private set; }
        public List<Node> selectedNodes { get; private set; }
        public List<Pipe> selectedPipes { get; private set; }
        public List<Node> moveNodes { get; private set; }
        public List<Pipe> movePipes { get; private set; }

        public List<label> Labels { get; private set; }
        public List<Layer> Layers { get; private set; }
        #endregion

        #region Font stuff
        Font tempFont;
        FontConverter fontConverter = new FontConverter();
        OpenTK.Graphics.TextPrinter textPrinter;
        public Font font = new Font("Arial", 7, FontStyle.Regular); //general font (global)
        #endregion

        #region Images
        int imageBorderWidth = 2;
        ContextMenuStrip imageContextMenu = new ContextMenuStrip();
        ToolStripMenuItem imageProperties;
        #endregion

        #region Overlapping snap stuff
        bool overlappedNodes = false;
        bool overlappedNodeWithLine = false;
        Node overlappedNodeWithLineNode;
        Pipe overlappedNodeWithLinePipe;
        Node overlapNode1, overlapNode2;
    
        #endregion

        #region Other stuff
        StreamWriter swLog = new StreamWriter("log.txt");
        int originalZoomLevel = 74;
        int zoomLevel = 74;
        int oldZoom = 74;
        bool drawTextBool = true;
        public string openedMap = "";
        int zoomRatio = 6;
        public GLControl GLControl { get; private set; }
        Node nod;
        public OpenFileDialog openFileDialog1 = new OpenFileDialog();
        int init_x = 0, init_y = 0;
        Thread t;
        System.Windows.Forms.MouseEventArgs mea;
        Graphics GLGraphics;

        Pipe tempPipe = new Pipe(-2, new Node(new Vector2(0, 0), -2), new Node(new Vector2(0, 0), -2));
        Node tempNode = new Node(new Vector2(0, 0), -2);
        int anteLastNodeContinous = -2;
        bool drawTempNode = true;
        #endregion

        #region Mouse stuff
        bool panningBool = false; //is currently panning
        bool isMouseDown = false;
        Point mouseDown = Point.Empty; //is mouse down for selection tool (start point for rectangle)
        Rectangle selectionRectangle = new Rectangle(); //rectangle for selection tool
        Point MouseLocation = new Point(0, 0); //mouse location
        #endregion

        #region Indexes
        int selectedIndex = -1, selectedIndex2 = -1, selectedControlPointIndex = -1, selectedPipeIndex = -1, selectedNodeIndex = -1;
        int selectedImageIndex = -1;
        int continousLastNodeIndex = -1;
        int selectedLabelIndex = -1;
        #endregion

        #region Context menu
        ContextMenuStrip nodContextMenu = new ContextMenuStrip();
        ContextMenuStrip pipeContextMenu = new ContextMenuStrip();
        ContextMenuStrip labelContextMenu = new ContextMenuStrip();
        ToolStripMenuItem labelProperties, deleteLabel;
        ToolStripMenuItem nodeProperties, pipeProperties, pipeToLine, pipeToCurve, deleteNode, deletePipe, detachPipe, nodeEnterExit;
        ToolStripMenuItem addNode, changeTo, changeToObject1, changeToObject2, changeToObject3, changeToObject4, changeDirection;
        ToolStripMenuItem nodeEnter, nodeExit, nodeNone;
        #endregion

        #region Grid
        bool need = true;
        public List<gridLine> Grid;
        int cateDeasupra = 0, cateDedesupt = 0, hMinID = 0, hMaxID = 0;
        int cateStanga = 0, cateDreapta = 0, vMinID = 0, vMaxID = 0;
        float gridWidth = 100, gridHeight = 100;
        bool drawGridBool = true;
        #endregion

        #region Tool enumerations
        enum Actions
        {
            continousLines,
            drawNodes,
            drawLines,
            drawCurves,
            select,
            panning,
            moveNodes,
            label
        }

        Actions currentAction = Actions.continousLines;
        Actions lastAction = Actions.continousLines; //used for resuming the shortcuts
        #endregion

        #region Network type
        public enum NetworkTypes
        {
            Gas,
            Liquid,
            GasCondensed
        }
        public NetworkTypes NetworkType = NetworkTypes.Gas;
        #endregion

        #region Global properties
        public Hashtable NodeGlobalProperties = new Hashtable();
        public Hashtable PipeGlobalProperties = new Hashtable();
        #endregion

        #region Pop-up notification
        Notification notification;
        Point NotificationLocation = new Point(0, 0);
        public string NotificationResult = "";
        #endregion

        #region Labels (text)
        TextBox labelTempTextBox = new TextBox();
        RectangleF labelRectangle = new RectangleF();
        bool drawLabelRectangle = false;
        #endregion

        #endregion

        #region Constructors
        public Form1()
        {
            InitializeComponent();
            toolStripStatusLabel1.Text = "Network type: Gas";
            Images = new List<Image>();
            Nodes = new List<Node>();
            Grid = new List<gridLine>();
            Pipes = new List<Pipe>();
            moveNodes = new List<Node>();
            movePipes = new List<Pipe>();
            selectedNodes = new List<Node>();
            selectedPipes = new List<Pipe>();
            Labels = new List<label>();
            Subnetworks = new List<String>();
            Layers = new List<Layer>();

            initCanvas();
            Layer newLayer = new Layer(newLayerIndex(), "Layer " + newLayerIndex().ToString());
            newLayer.isSelected = true;
            Layers.Add(newLayer);
            loadLayersList();
            initContextMenus();
            textPrinter = new OpenTK.Graphics.TextPrinter();
            toolStripMenuItem1.PerformClick();
            zoomLevel = originalZoomLevel;
            int tempGridID = 0;
            //for (int i = -10000; i <= 10000; i += 10)
            //{
            //    tempGridID = newgridLineIndex();
            //    Grid.Add(new gridLine(tempGridID, 0, i, GLControl.Width, i, gridLine._Type.Horizontal));
            //    horizontalGridIDs.Add(tempGridID);
            //}
            //for (int i = -10000; i <= 19000; i += 10)
            //{
            //    tempGridID = newgridLineIndex();
            //    Grid.Add(new gridLine(newgridLineIndex(), i, 0, i, GLControl.Height, gridLine._Type.Vertical));
            //    verticalGridIDs.Add(tempGridID);
            //}
            for (int i = -100; i <= 1000; i += 10)
            {
                tempGridID = newgridLineIndex();
                Grid.Add(new gridLine(tempGridID, 0, i, GLControl.Width, i, gridLine._Type.Horizontal));

            }
            for (int i = -100; i <= 1900; i += 10)
            {
                tempGridID = newgridLineIndex();
                Grid.Add(new gridLine(newgridLineIndex(), i, 0, i, GLControl.Height, gridLine._Type.Vertical));

            }
            cateDeasupra = Grid.Where(item => item.Type == gridLine._Type.Horizontal && item.Y1 < GLControl.Top).Count();
            cateDedesupt = Grid.Where(item => item.Type == gridLine._Type.Horizontal && item.Y1 > GLControl.Bottom).Count();
            hMinID = Grid.Where(item => item.Type == gridLine._Type.Horizontal).OrderByDescending(line => line.Y1).Last().Index;
            hMaxID = Grid.Where(item => item.Type == gridLine._Type.Horizontal).OrderByDescending(line => line.Y1).First().Index;
            cateStanga = Grid.Where(item => item.Type == gridLine._Type.Vertical && item.X1 < GLControl.Left).Count();
            cateDreapta = Grid.Where(item => item.Type == gridLine._Type.Vertical && item.X1 > GLControl.Right).Count();
            vMinID = Grid.Where(item => item.Type == gridLine._Type.Vertical).OrderByDescending(line => line.X1).Last().Index;
            vMaxID = Grid.Where(item => item.Type == gridLine._Type.Vertical).OrderByDescending(line => line.X1).First().Index;

            gridHeight = Grid[vMinID + 1].X1 - Grid[vMinID].X1;
            gridWidth = Grid[hMinID + 1].Y1 - Grid[hMinID].Y1;


            labelTempTextBox.KeyDown += new KeyEventHandler(labelTempTextBox_KeyDown);
            labelTempTextBox.Visible = false;
            GLControl.Controls.Add(labelTempTextBox);

            mea = new System.Windows.Forms.MouseEventArgs(System.Windows.Forms.MouseButtons.Middle, 0, GLControl.Width / 2, GLControl.Height / 2, 1);
            notification = new Notification(this);
        }


        private void initContextMenus()
        {
            #region init
            nodeProperties = new ToolStripMenuItem();
            pipeProperties = new ToolStripMenuItem();
            pipeToLine = new ToolStripMenuItem();
            pipeToCurve = new ToolStripMenuItem();
            deleteNode = new ToolStripMenuItem();
            deletePipe = new ToolStripMenuItem();
            addNode = new ToolStripMenuItem();
            changeTo = new ToolStripMenuItem();
            changeToObject1 = new ToolStripMenuItem();
            changeToObject2 = new ToolStripMenuItem();
            changeToObject3 = new ToolStripMenuItem();
            changeToObject4 = new ToolStripMenuItem();
            nodeEnterExit = new ToolStripMenuItem();
            nodeEnter = new ToolStripMenuItem();
            nodeExit = new ToolStripMenuItem();
            nodeNone = new ToolStripMenuItem();
            changeDirection = new ToolStripMenuItem();
            imageProperties = new ToolStripMenuItem();
            detachPipe = new ToolStripMenuItem();
            labelProperties = new ToolStripMenuItem();
            deleteLabel = new ToolStripMenuItem();
            #endregion

            labelProperties.Text = "Label properties";
            labelContextMenu.Items.Add(labelProperties);
            labelProperties.Click += new EventHandler(labelProperties_Click);

            deleteLabel.Text = "Delete label";
            labelContextMenu.Items.Add(deleteLabel);
            deleteLabel.Click += new EventHandler(deleteLabel_Click);

            imageProperties.Text = "Image properties";
            imageContextMenu.Items.Add(imageProperties);
            imageProperties.Click += new EventHandler(imageProperties_Click);

            nodeProperties.Text = "Node properties";
            nodContextMenu.Items.Add(nodeProperties);
            nodeProperties.Click += new EventHandler(nodeProperties_Click);

            pipeProperties.Text = "Pipe properties"; 
            pipeContextMenu.Items.Add(pipeProperties);
            pipeProperties.Click += new EventHandler(pipeProperties_Click);

            detachPipe.Text = "Detach pipe";
            pipeContextMenu.Items.Add(detachPipe);
            detachPipe.Click += new EventHandler(detachPipe_Click);

            pipeToLine.Text = "Change pipe type to line";
            pipeContextMenu.Items.Add(pipeToLine);
            pipeToLine.Click += new EventHandler(pipeToLine_Click);



            pipeToCurve.Text = "Change pipe type to Curve";
            pipeContextMenu.Items.Add(pipeToCurve);
            pipeToCurve.Click += new EventHandler(pipeToCurve_Click);


            deleteNode.Text = "Delete this node";
            nodContextMenu.Items.Add(deleteNode);
            deleteNode.Click += new EventHandler(deleteNode_Click);

            deletePipe.Text = "Delete this pipe";
            pipeContextMenu.Items.Add(deletePipe);
            deletePipe.Click += new EventHandler(deletePipe_Click);

            addNode.Text = "Add node and split in 2 pipes";
            pipeContextMenu.Items.Add(addNode);
            addNode.Click += new EventHandler(addNodeC_Click);


            nodeEnterExit.Text = "Type";
            nodContextMenu.Items.Add(nodeEnterExit);


            changeToObject1.Text = "Object type 1";
            changeToObject1.Click += new EventHandler(changeToObject1_Click);
            changeToObject2.Text = "Object type 2";
            changeToObject2.Click += new EventHandler(changeToObject2_Click);
            changeToObject3.Text = "Object type 3";
            changeToObject3.Click += new EventHandler(changeToObject3_Click);
            changeToObject4.Text = "Object type 4";
            changeToObject4.Click += new EventHandler(changeToObject4_Click);



            nodeEnter.Text = "Change to entering node";
            nodeExit.Text = "Change to exiting node";
            nodeNone.Text = "Change to none of them node";
            nodeEnter.Click += new EventHandler(nodeEnter_Click);
            nodeExit.Click += new EventHandler(nodeExit_Click);
            nodeNone.Click += new EventHandler(nodeNone_Click);

            nodeEnterExit.DropDownItems.Add(nodeEnter);
            nodeEnterExit.DropDownItems.Add(nodeExit);
            nodeEnterExit.DropDownItems.Add(nodeNone);

            changeDirection.Text = "Change pipe direction";
            changeDirection.Click += new EventHandler(changeDirection_Click);
            pipeContextMenu.Items.Add(changeDirection);

            changeTo.Text = "Change to object";
            changeTo.DropDownItems.Add(changeToObject1);
            changeTo.DropDownItems.Add(changeToObject2);
            changeTo.DropDownItems.Add(changeToObject3);
            changeTo.DropDownItems.Add(changeToObject4);
            pipeContextMenu.Items.Add(changeTo);

        }
        private void initCanvas()
        {
            OpenTK.Graphics.ColorFormat cf = new OpenTK.Graphics.ColorFormat(10, 10, 10, 2);
            GLControl = new GLControl(new OpenTK.Graphics.GraphicsMode(32, 24, 8, 8, cf, 4));
           
            this.splitContainer2.Panel1.Controls.Add(GLControl);
            GLControl.Size = new Size(splitContainer1.Panel2.Width, splitContainer2.Panel1.Height);
            splitContainer1.Cursor = Cursors.Default;
            splitContainer2.Cursor = Cursors.Default;
            splitContainer3.Cursor = Cursors.Default;
            GLControl.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            GLGraphics = GLControl.CreateGraphics();
            
            GLControl.Load += new EventHandler(GLControl_Load);
            GLControl.Paint += new PaintEventHandler(GLControl_Paint);
            GLControl.MouseClick += new MouseEventHandler(GLControl_MouseClick);
            GLControl.MouseDown += new MouseEventHandler(GLControl_MouseDown);
            GLControl.MouseUp += new MouseEventHandler(GLControl_MouseUp);
            GLControl.MouseMove += new MouseEventHandler(GLControl_MouseMove);
            GLControl.MouseWheel += new MouseEventHandler(GLControl_MouseWheel);
            GLControl.MouseLeave += new EventHandler(GLControl_MouseLeave);
            GLControl.Cursor = Cursors.Cross;
           
        }
        #endregion
      

        #region Context menu items and events
        private void detachPipe_Click(object sender, EventArgs e)
        {
            Node n1 = null, n2 = null;
            selectedNodes.Clear();
            selectedPipes.Clear();
            int  pipesFromNode1 =0, pipesFromNode2 = 0;
            int myPipeIndex = Int32.Parse(((ToolStripMenuItem)sender).Tag.ToString());
            Pipe myPipe = Pipes.Where(item => item.Index == myPipeIndex).Single();

            pipesFromNode1 += Pipes.Where(item => item.Node1.Index == myPipe.Node1.Index && item.Index != myPipeIndex).Count();
            pipesFromNode1 += Pipes.Where(item => item.Node2.Index == myPipe.Node1.Index && item.Index != myPipeIndex).Count();
            pipesFromNode2 += Pipes.Where(item => item.Node1.Index == myPipe.Node2.Index && item.Index != myPipeIndex).Count();
            pipesFromNode2 += Pipes.Where(item => item.Node2.Index == myPipe.Node2.Index && item.Index != myPipeIndex).Count();

            if (pipesFromNode1 != 0)
            {
                n1 = new Node(myPipe.Node1.Vector, newNodeIndex());
                foreach(object key in myPipe.Node1.Data.Properties.Keys)
                {
                    n1.Data.Properties.Add(key, myPipe.Node1.Data.Properties[key]);
                }
                Nodes.Add(n1);

            }

            if (pipesFromNode2 != 0)
            {
                n2 = new Node(myPipe.Node2.Vector, newNodeIndex());
                foreach (object key in myPipe.Node2.Data.Properties.Keys)
                {
                    n2.Data.Properties.Add(key, myPipe.Node2.Data.Properties[key]);
                }
                Nodes.Add(n2);
            }


            if (n1 != null)
            {
                if (n2 != null)
                {
                    Pipes.Where(item => item.Index == myPipeIndex).Single().Node1 = n1;
                    Pipes.Where(item => item.Index == myPipeIndex).Single().Node2 = n2;
                    selectedNodes.Add(n1);
                    selectedNodes.Add(n2);
                    selectedPipes.Add(Pipes.Where(item => item.Index == myPipeIndex).Single());
                }
                else
                {

                    Pipes.Where(item => item.Index == myPipeIndex).Single().Node1 = n1;
                    Pipes.Where(item => item.Index == myPipeIndex).Single().Node2 = Pipes.Where(item => item.Index == myPipeIndex).Single().Node2;
                    selectedNodes.Add(Pipes.Where(item => item.Index == myPipeIndex).Single().Node2);
                    selectedNodes.Add(n1);
                    selectedPipes.Add(Pipes.Where(item => item.Index == myPipeIndex).Single());
                }
            }
            else
            {
                if (n2 != null)
                {
                    Pipes.Where(item => item.Index == myPipeIndex).Single().Node1 = Pipes.Where(item => item.Index == myPipeIndex).Single().Node1;
                    Pipes.Where(item => item.Index == myPipeIndex).Single().Node2 = n2;
                    selectedNodes.Add(Pipes.Where(item => item.Index == myPipeIndex).Single().Node1);
                    selectedNodes.Add(n2);
                    selectedPipes.Add(Pipes.Where(item => item.Index == myPipeIndex).Single());
                }
                else
                {
                    return;
                }
            }

            toolStripButton4.PerformClick();
        }
        private void imageProperties_Click(object sender, EventArgs e)
        {
            Image img = Images.Where(item => item.Index == Int32.Parse(((ToolStripMenuItem)sender).Tag.ToString())).Single();
            objectProperties obj = new objectProperties(null, null, img, this);
            obj.ShowDialog();
        }
        private void nodeNone_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)sender).Tag != null)
                Nodes.Where(item => item.Index == Int32.Parse(((ToolStripMenuItem)sender).Tag.ToString())).Single().Data.currentEnterExit = AdditionalNodeData.EnterExitType.None;
            else showNotification("Cannot change node type", "This node has multiple pipes connected to it.\nError: Maximum 1 pipe", SystemIcons.Error.Handle, MessageBoxButtons.OK, false);
           

        }
        private void nodeExit_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)sender).Tag != null)
            Nodes.Where(item => item.Index == Int32.Parse(((ToolStripMenuItem)sender).Tag.ToString())).Single().Data.currentEnterExit = AdditionalNodeData.EnterExitType.Exit;
            else showNotification("Cannot change node type", "This node has multiple pipes connected to it.\nError: Maximum 1 pipe", SystemIcons.Error.Handle, MessageBoxButtons.OK, false);
        }
        private void changeDirection_Click(object sender, EventArgs e)
        {
            Pipe pipeAux = Pipes.Where(item => item.Index == Int32.Parse(((ToolStripMenuItem)sender).Tag.ToString())).Single();
            Node nodeAux;
            nodeAux = pipeAux.Node1;
            pipeAux.Node1 = pipeAux.Node2;
            pipeAux.Node2 = nodeAux;

        }
        private void nodeEnter_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)sender).Tag != null)
            Nodes.Where(item => item.Index == Int32.Parse(((ToolStripMenuItem)sender).Tag.ToString())).Single().Data.currentEnterExit = AdditionalNodeData.EnterExitType.Enter;
            else showNotification("Cannot change node type", "This node has multiple pipes connected to it.\nError: Maximum 1 pipe", SystemIcons.Error.Handle, MessageBoxButtons.OK, false);
        }
        private void addNodeC_Click(object sender, EventArgs e)
        {
            string[] parameters = new string[4]; // [0] = Pipe ID, [1] = e.X, [2] = e.Y
            parameters = ((ToolStripItem)sender).Tag.ToString().Split(';');
            Node auxNode1 = Pipes.Where(item => item.Index == Int32.Parse(parameters[0])).Single().Node1;
            Node auxNode2 = Pipes.Where(item => item.Index == Int32.Parse(parameters[0])).Single().Node2;
            Pipes.Remove(Pipes.Where(item => item.Index == Int32.Parse(parameters[0])).Single());
            Node nodeToAdd = new Node(new Vector2(Int32.Parse(parameters[1]), Int32.Parse(parameters[2])), newNodeIndex());
            Nodes.Add(nodeToAdd);
            Pipes.Add(new Pipe(newPipeIndex(), auxNode1, nodeToAdd));
            Pipes.Add(new Pipe(newPipeIndex(), nodeToAdd, auxNode2));

        }
        private void deletePipe_Click(object sender, EventArgs e)
        {
            Pipes.Remove(Pipes.Where(item => item.Index == Int32.Parse(deletePipe.Tag.ToString())).Single());
        }
        private void deleteNode_Click(object sender, EventArgs e)
        {
            int[] removePipeIndexes = new int[2147483591 / Marshal.SizeOf(typeof(int))]; int i = 0;
            foreach (Pipe p in Pipes.Where(item => item.Node1 == Nodes.Where(item2 => item2.Index == Int32.Parse(deleteNode.Tag.ToString())).Single()))
            {
               
                removePipeIndexes[i] = p.Index;
                i++;
            }
            foreach (Pipe p in Pipes.Where(item => item.Node2 == Nodes.Where(item2 => item2.Index == Int32.Parse(deleteNode.Tag.ToString())).Single()))
            {
                
                removePipeIndexes[i] = p.Index;
                i++;
            }
            if(i > 0)
            {
                for (int j = 0; j < i; j++)
                {
                    try
                    {
                        Pipes.Remove(Pipes.Where(item => item.Index == removePipeIndexes[j]).Single());
                    }
                    catch(Exception)
                    {

                    }
                }
            }
                Nodes.Remove(Nodes.Where(item => item.Index == Int32.Parse(deleteNode.Tag.ToString())).Single());
                selectedNodes.Clear();
                selectedPipes.Clear();
                mouseDown = Point.Empty;
        }
        private void pipeToCurve_Click(object sender, EventArgs e)
        {
            Pipes[Int32.Parse(pipeToLine.Tag.ToString())].PipeType = Pipe.PipeTypes.Curve;
            float ctrlPointLocationX = (Pipes[Int32.Parse(pipeToLine.Tag.ToString())].Node1.Vector.X + Pipes[Int32.Parse(pipeToLine.Tag.ToString())].Node2.Vector.X) / 2;
            float ctrlPointLocationY = (Pipes[Int32.Parse(pipeToLine.Tag.ToString())].Node1.Vector.Y + Pipes[Int32.Parse(pipeToLine.Tag.ToString())].Node2.Vector.Y) / 2;
            if (ctrlPointLocationX < Pipes[Int32.Parse(pipeToLine.Tag.ToString())].Node1.Vector.X + 100 || ctrlPointLocationX < Pipes[Int32.Parse(pipeToLine.Tag.ToString())].Node1.Vector.X - 100) ctrlPointLocationX += ctrlPointLocationX * 0.5f;
            if (ctrlPointLocationY < Pipes[Int32.Parse(pipeToLine.Tag.ToString())].Node1.Vector.Y + 100 || ctrlPointLocationX < Pipes[Int32.Parse(pipeToLine.Tag.ToString())].Node1.Vector.Y - 100) ctrlPointLocationY += ctrlPointLocationY * 0.5f;
                    Pipes[Int32.Parse(pipeToLine.Tag.ToString())].ControlPoint = new Vector2(ctrlPointLocationX, ctrlPointLocationY);
        }      
        private void pipeToLine_Click(object sender, EventArgs e)
        {
            Pipes.Where(item => item.Index == Int32.Parse(pipeToLine.Tag.ToString())).Single().PipeType = Pipe.PipeTypes.Line;
        }
        private void pipeProperties_Click(object sender, EventArgs e)
        {
            Form objectPropertiesFRM = new objectProperties(null, Pipes.Where(item => item.Index == Int32.Parse(pipeProperties.Tag.ToString())).Single(), null, this);
            objectPropertiesFRM.ShowDialog();
        }
        private void nodeProperties_Click(object sender, EventArgs e)
        {
           Form objectPropertiesFRM = new objectProperties(Nodes.Where(item => item.Index == Int32.Parse(nodeProperties.Tag.ToString())).Single(), null, null, this);
           objectPropertiesFRM.ShowDialog();
        }
        private void changeToObject1_Click(object sender, EventArgs e)
        {
            Pipes.Where(item => item.Index == Int32.Parse(changeToObject1.Tag.ToString())).Single().ObjectType = Pipe.ObjectTypes.Type1;
        }
        private void changeToObject2_Click(object sender, EventArgs e)
        {
            Pipes.Where(item => item.Index == Int32.Parse(changeToObject1.Tag.ToString())).Single().ObjectType = Pipe.ObjectTypes.Type2;
        }
        private void changeToObject3_Click(object sender, EventArgs e)
        {
            Pipes.Where(item => item.Index == Int32.Parse(changeToObject1.Tag.ToString())).Single().ObjectType = Pipe.ObjectTypes.Type3;
        }
        private void changeToObject4_Click(object sender, EventArgs e)
        {
            Pipes.Where(item => item.Index == Int32.Parse(changeToObject1.Tag.ToString())).Single().ObjectType = Pipe.ObjectTypes.Type4;
        }
        private void deleteLabel_Click(object sender, EventArgs e)
        {
            Labels.Remove(Labels.Where(item => item.Index == Int32.Parse(labelContextMenu.Tag.ToString())).Single());
            drawLabelRectangle = false;
            GLControl.Invalidate();
        }

        private void labelProperties_Click(object sender, EventArgs e)
        {
            LabelProperties labelProperties = new LabelProperties(Labels.Where(item => item.Index == Int32.Parse(labelContextMenu.Tag.ToString())).Single(), this);
            labelProperties.ShowDialog();
        }

        #endregion

        #region GLControl event handlers
        private void GLControl1_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private void GLControl_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            zoom(e);
        }

        private void zoom(System.Windows.Forms.MouseEventArgs e)
        {
            
            int zoom_aux_x, zoom_aux_y;
            //move the zoomed objects
            if (e.Delta < 0)
            {
                if (zoomLevel + 1 != 0) zoomLevel += 1;
                else zoomLevel += 2;
                zoom_aux_x = e.X - e.X / 100 * zoomRatio;
                zoom_aux_y = e.Y - e.Y / 100 * zoomRatio;
                //foreach (Pipe p in Pipes)
                //{
                //    p.auxNode1 = p.Node1;
                //    p.auxNode2 = p.Node2;
                //}
                foreach (Node n in Nodes)
                {
                    n.Vector = new Vector2(n.Vector.X - n.Vector.X / 100 * zoomRatio, n.Vector.Y - n.Vector.Y / 100 * zoomRatio);
                    n.auxVector = n.Vector;
                    n.Vector = new Vector2(n.Vector.X + (e.X - zoom_aux_x), n.Vector.Y + (e.Y - zoom_aux_y));
                    n.auxVector = n.Vector;
                }
                foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                {
                    p.ControlPoint = new Vector2(p.ControlPoint.X - p.ControlPoint.X / 100 * zoomRatio, p.ControlPoint.Y - p.ControlPoint.Y / 100 * zoomRatio);
                    p.ControlPoint = new Vector2(p.ControlPoint.X + (e.X - zoom_aux_x), p.ControlPoint.Y + (e.Y - zoom_aux_y));
                    p.auxControlPoint = p.ControlPoint;
                }
                foreach (gridLine g in Grid.Where(item => item.Type == gridLine._Type.Vertical))
                {
                    g.X1 = g.X1 - g.X1 / 100 * zoomRatio;
                    g.X1 = g.X1 + (e.X - zoom_aux_x);
                    g.X2 = g.X2 - g.X2 / 100 * zoomRatio;
                    g.X2 = g.X2 + (e.X - zoom_aux_x);
                    g.auxX1 = g.X1;
                    g.auxX2 = g.X2;
                }
                foreach (gridLine g in Grid.Where(item => item.Type == gridLine._Type.Horizontal))
                {
                    g.Y1 = g.Y1 - g.Y1 / 100 * zoomRatio;
                    g.Y1 = g.Y1 + (e.Y - zoom_aux_y);
                    g.Y2 = g.Y2 - g.Y2 / 100 * zoomRatio;
                    g.Y2 = g.Y2 + (e.Y - zoom_aux_y);
                    g.auxY1 = g.Y1;
                    g.auxY2 = g.Y2;
                }
                foreach(label lbl in Labels.Where(item => item.Static == false))
                {
                    lbl.Y = lbl.Y - lbl.Y / 100 * zoomRatio;
                    lbl.Y = lbl.Y + (e.Y - zoom_aux_y);
                    lbl.X = lbl.X - lbl.X / 100 * zoomRatio;
                    lbl.X = lbl.X + (e.X - zoom_aux_x);
                    lbl.auxY = lbl.Y;
                    lbl.auxX = lbl.X;
                }
                //foreach(Image img in Images.Where(item => item.Zoomable == true))
                //{
                //    img.X = img.X - img.X / 100 * zoomRatio;
                //    img.auxX = img.X;
                //    img.X = img.X + (e.X - zoom_aux_x);
                //    img.auxX = img.X;

                //    img.Y = img.Y - img.Y / 100 * zoomRatio;
                //    img.auxY = img.Y;
                //    img.Y = img.Y + (e.Y - zoom_aux_y);
                //    img.auxY = img.Y;
                //}
            }
            else
            {
                if (zoomLevel - 1 > 0) zoomLevel -= 1;
                else zoomLevel -= 2;

                zoom_aux_x = e.X + e.X / 100 * zoomRatio;
                zoom_aux_y = e.Y + e.Y / 100 * zoomRatio;
                foreach (Pipe p in Pipes)
                {
                    p.auxNode1 = p.Node1;
                    p.auxNode2 = p.Node2;
                }
                foreach (Node n in Nodes)
                {
                    n.Vector = new Vector2(n.Vector.X + n.Vector.X / 100 * zoomRatio, n.Vector.Y + n.Vector.Y / 100 * zoomRatio);
                    n.Vector = new Vector2(n.Vector.X + (e.X - zoom_aux_x), n.Vector.Y + (e.Y - zoom_aux_y));
                    n.auxVector = n.Vector;
                }
                foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                {
                    p.ControlPoint = new Vector2(p.ControlPoint.X + p.ControlPoint.X / 100 * zoomRatio, p.ControlPoint.Y + p.ControlPoint.Y / 100 * zoomRatio);
                    p.ControlPoint = new Vector2(p.ControlPoint.X + (e.X - zoom_aux_x), p.ControlPoint.Y + (e.Y - zoom_aux_y));
                    p.auxControlPoint = p.ControlPoint;
                }
                foreach (gridLine g in Grid.Where(item => item.Type == gridLine._Type.Vertical))
                {
                    g.X1 = g.X1 + g.X1 / 100 * zoomRatio;
                    g.X1 = g.X1 + (e.X - zoom_aux_x);
                    g.X2 = g.X2 + g.X2 / 100 * zoomRatio;
                    g.X2 = g.X2 + (e.X - zoom_aux_x);
                    g.auxX1 = g.X1;
                    g.auxX2 = g.X2;
                }

                foreach (gridLine g in Grid.Where(item => item.Type == gridLine._Type.Horizontal))
                {
                    g.Y1 = g.Y1 + g.Y1 / 100 * zoomRatio;
                    g.Y1 = g.Y1 + (e.Y - zoom_aux_y);
                    g.Y2 = g.Y2 + g.Y2 / 100 * zoomRatio;
                    g.Y2 = g.Y2 + (e.Y - zoom_aux_y);
                    g.auxY1 = g.Y1;
                    g.auxY2 = g.Y2;
                }
                foreach (label lbl in Labels.Where(item => item.Static == false))
                {
                    lbl.Y = lbl.Y + lbl.Y / 100 * zoomRatio;
                    lbl.Y = lbl.Y + (e.Y - zoom_aux_y);
                    lbl.X = lbl.X + lbl.X / 100 * zoomRatio;
                    lbl.X = lbl.X + (e.X - zoom_aux_x);
                    lbl.auxY = lbl.Y;
                    lbl.auxX = lbl.X;
                }
                //foreach (Image img in Images.Where(item => item.Zoomable == true))
                //{
                //    img.X = img.X - img.X / 100 * zoomRatio;
                //    img.auxX = img.X;
                //    img.X = img.X + (e.X - zoom_aux_x);
                //    img.auxX = img.X;

                //    img.Y = img.Y - img.Y / 100 * zoomRatio;
                //    img.auxY = img.Y;
                //    img.Y = img.Y + (e.Y - zoom_aux_y);
                //    img.auxY = img.Y;
                //}

            }

            refreshGrid();
            foreach(Pipe p in Pipes)
            {
                float PipeLength = (float)Math.Sqrt(Math.Pow((double)(p.Node2.Vector.X - p.Node1.Vector.X), 2) + Math.Pow((double)(p.Node2.Vector.Y - p.Node1.Vector.Y), 2));
                float PipeLength2 = (float)Math.Sqrt(Math.Pow((double)(p.Node1.Vector.X - p.Node2.Vector.X), 2) + Math.Pow((double)(p.Node1.Vector.Y - p.Node2.Vector.Y), 2));
                
                if (PipeLength < 200 || PipeLength2 < 200)
                {
                    p.DrawText = false;
                }
                else
                {
                    p.DrawText = true;
                }
            }
            foreach(Node n in Nodes)
            {
                foreach (Node n2 in Nodes)
                {
                    float NodeDistance = (float)Math.Sqrt(Math.Pow((double)(n2.Vector.X - n.Vector.X), 2) + Math.Pow((double)(n2.Vector.Y - n.Vector.Y), 2));
                    float NodeDistance2 = (float)Math.Sqrt(Math.Pow((double)(n.Vector.X - n2.Vector.X), 2) + Math.Pow((double)(n.Vector.Y - n2.Vector.Y), 2));
                    if (NodeDistance < 150 || NodeDistance2 < 150)
                    {
                        n.DrawText = false;
                        n2.DrawText = false;
                    }
                    else
                    {
                        n.DrawText = true;
                        n2.DrawText = true;
                        
                    }
                   
                }
            }
            GLControl.Invalidate();
        }

        void GLControl_Load(object sender, EventArgs e)
        {
           

            GL.ClearColor(Properties.Settings.Default.BackgroundColor);

            // Set orthographic rendering
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();

            GL.Ortho(GLControl.Bounds.Left, GLControl.Bounds.Right,
       GLControl.Bounds.Bottom, GLControl.Bounds.Top, -1.0, 1.0);

            GL.Viewport(this.GLControl.Size);
           


        }
        void GLControl_Paint(object sender, PaintEventArgs e)
        {
            // Set size of point and width of line

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            drawGrid();


        
            foreach(Image img in Images)
            {
                drawImage(img);
            }


            foreach(label lbl in Labels)
            {
                GL.Color3(Properties.Settings.Default.TextColor);
                if (lbl.font != null) tempFont = lbl.font;
                else tempFont = font;
                textPrinter.Begin();
                textPrinter.Print(lbl.Text, tempFont, Properties.Settings.Default.TextColor, new RectangleF(lbl.X, lbl.Y, textPrinter.Measure(lbl.Text, tempFont).BoundingBox.Width * 2, textPrinter.Measure(lbl.Text, tempFont).BoundingBox.Height * 2));
                textPrinter.End();
                if(lbl.MarkedLabel == true)
                {
                    GL.Begin(PrimitiveType.LineLoop);
                    GL.Color3(Properties.Settings.Default.TextColor);
                    GL.Vertex2(lbl.X - textPrinter.Measure(lbl.Text, tempFont).BoundingBox.Width, lbl.Y - textPrinter.Measure(lbl.Text, tempFont).BoundingBox.Height * 0.5);
                    GL.Vertex2(lbl.X - textPrinter.Measure(lbl.Text, tempFont).BoundingBox.Width, lbl.Y + textPrinter.Measure(lbl.Text, tempFont).BoundingBox.Height*1.5);
                    GL.Vertex2(lbl.X + textPrinter.Measure(lbl.Text, tempFont).BoundingBox.Width*2, lbl.Y + textPrinter.Measure(lbl.Text, tempFont).BoundingBox.Height*1.5);
                    GL.Vertex2(lbl.X + textPrinter.Measure(lbl.Text, tempFont).BoundingBox.Width * 2, lbl.Y - textPrinter.Measure(lbl.Text, tempFont).BoundingBox.Height*0.5);
                    GL.End();
                }
                if(lbl.nodeAssignedIndex != -1)
                {
                    drawLabelAssignment(lbl, Nodes.Where(item => item.Index == lbl.nodeAssignedIndex).Single(), null);
                }
                if(lbl.pipeAssignedIndex != -1)
                {
                    drawLabelAssignment(lbl, null, Pipes.Where(item => item.Index == lbl.pipeAssignedIndex).Single());
                }
            }
            //label rectangle
            if(drawLabelRectangle)
            {
                GL.Color3(Properties.Settings.Default.TextColor);
                GL.Begin(PrimitiveType.LineLoop);
                GL.Vertex2(labelRectangle.X, labelRectangle.Y);
                GL.Vertex2(labelRectangle.X, labelRectangle.Y + labelRectangle.Height);
                GL.Vertex2(labelRectangle.X + labelRectangle.Width, labelRectangle.Y + labelRectangle.Height);
                GL.Vertex2(labelRectangle.X + labelRectangle.Width, labelRectangle.Y);
                GL.End();
            }


            GL.Color3(Properties.Settings.Default.PipeColor);
            if (pipesToolStripMenuItem.Checked == true)
            {
                foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Line))
                {

                        if (drawTextBool && p.DrawText == true)
                        {
                            foreach (object key in p.Data.Properties.Keys)
                            {
                                if (Int32.Parse(p.Data.Properties[key].ToString().Split(';')[3]) == 1)
                                {
                                    string text = key.ToString()[0].ToString().ToUpper() + ": " + p.Data.Properties[key].ToString().Split(';')[0];
                                    textPrinter.Begin();
                                    if (p.Font != null) tempFont = p.Font;
                                    else tempFont = font;
                                    textPrinter.Print(text, tempFont, Properties.Settings.Default.TextColor, new RectangleF((p.Node1.Vector.X + p.Node2.Vector.X) / 2 + float.Parse(p.Data.Properties[key].ToString().Split(';')[1]), (p.Node1.Vector.Y + p.Node2.Vector.Y) / 2 + float.Parse(p.Data.Properties[key].ToString().Split(';')[2]), textPrinter.Measure(text, tempFont).BoundingBox.Width * 2, textPrinter.Measure(text, tempFont).BoundingBox.Height * 2));

                                    textPrinter.End();
                                }
                            }
                        }


                        GL.LineWidth(p.Width);
                        GL.Begin(PrimitiveType.Lines);
                        if (currentAction != Actions.continousLines)
                        {
                            if (p.DrawShadow == true)
                            {
                                GL.Color3(Color.Gray);
                                GL.LineWidth(p.Width + 1);
                                GL.Vertex2(p.Node1.Vector.X, p.Node1.Vector.Y + 2);
                                GL.Vertex2(p.Node2.Vector.X, p.Node2.Vector.Y + 2);
                                GL.Color3(Properties.Settings.Default.PipeColor);
                                GL.LineWidth(p.Width);
                            }
                        }

                        GL.Vertex2(p.Node2.Vector);
                        GL.Vertex2(p.Node1.Vector);
                        GL.End();
                        drawDirection(p);
                        if (p.ObjectType != Pipe.ObjectTypes.Type1) //has object attached 
                        {
                            drawObject(p.ObjectType, p);
                        }
                }


                foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                {
                   
                        BezierCurveQuadric bez = new BezierCurveQuadric(p.Node1.Vector, p.Node2.Vector, p.ControlPoint);
                        Vector2 pos = Vector2.One;

                        for (int points = 1; points <= p.drawnPoints; points++)
                        {
                            GL.PointSize(p.Width);
                            GL.Begin(PrimitiveType.Points);
                            pos = bez.CalculatePoint((float)points / (float)p.drawnPoints);
                            GL.Vertex2(pos.X, pos.Y);
                            GL.End();
                        }
                    

                }
            }
            string prefix = "";
            if (nodesToolStripMenuItem.Checked == true)
            {
                GL.Color3(Properties.Settings.Default.NodeColor);
                foreach (Node n in Nodes)
                {
                       if(Layers.Where(item => item.isVisible).Contains(n.layer))
                       {
                           prefix = "";
                           if (drawTextBool && n.DrawText == true)
                           {
                               foreach (object key in n.Data.Properties.Keys)
                               {
                                   if (Int32.Parse(n.Data.Properties[key].ToString().Split(';')[3]) == 1)
                                   {
                                       if (key.ToString() == "Name") prefix = "";
                                       else prefix = key.ToString()[0].ToString().ToUpper() + ": ";
                                       string text = prefix + n.Data.Properties[key].ToString().Split(';')[0];
                                       textPrinter.Begin();
                                       if (n.Font != null) tempFont = n.Font;
                                       else tempFont = font;
                                       textPrinter.Print(text, tempFont, Properties.Settings.Default.TextColor, new RectangleF(n.Vector.X + float.Parse(n.Data.Properties[key].ToString().Split(';')[1]), n.Vector.Y + float.Parse(n.Data.Properties[key].ToString().Split(';')[2]), textPrinter.Measure(text, tempFont).BoundingBox.Width * 2, textPrinter.Measure(text, tempFont).BoundingBox.Height * 2));

                                       textPrinter.End();
                                   }
                               }
                           }
                           if (currentAction != Actions.continousLines)
                           {
                               if (n.DrawShadow == true)
                               {
                                   GL.Color3(Color.Gray);
                                   GL.PointSize(n.PointSize + 1f);
                                   GL.Vertex2(n.Vector.X + 1, n.Vector.Y + 1);
                                   GL.Color3(Properties.Settings.Default.NodeColor);
                                   GL.PointSize(n.PointSize + 1f);
                               }
                           }


                           drawNode(n);
                       }
                    
                }

            }

            if (controlPointsForCurvedLinesToolStripMenuItem.Checked == true)
            {
                foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                {
                    GL.PointSize(5f);
                    GL.Begin(PrimitiveType.Points);
                    if (selectedControlPointIndex == p.ControlPointIndex)
                        GL.Color3(Properties.Settings.Default.SelectionColor);
                    else
                        GL.Color3(Properties.Settings.Default.ControlPointColor);
                    GL.Vertex2(p.ControlPoint);
                    GL.End();
                }
            }


            if (mouseDown != Point.Empty)
            {
                Point mousePos = MouseLocation; //PointToClient(MouseLocation);
                Rectangle selectedRect = new Rectangle(
                    Math.Min(mouseDown.X, mousePos.X),
                    Math.Min(mouseDown.Y, mousePos.Y),
                    Math.Abs(mousePos.X - mouseDown.X),
                    Math.Abs(mousePos.Y - mouseDown.Y));
                selectionRectangle = selectedRect;
                //e.Graphics.FillRectangle(selectionBrush, selectedRect);
                //e.Graphics.DrawRectangle(framePen, selectedRect);
                GL.LineWidth(1f);
                GL.Color3(Color.Aqua);
                GL.Begin(PrimitiveType.LineLoop);
                GL.Vertex2(selectedRect.X, selectedRect.Y);
                GL.Vertex2(selectedRect.X + selectedRect.Width, selectedRect.Y);
                GL.Vertex2(selectedRect.X + selectedRect.Width, selectedRect.Y + selectedRect.Height);
                GL.Vertex2(selectedRect.X, selectedRect.Y + selectedRect.Height);
                GL.End();
            }


           if(currentAction != Actions.panning)
           {
               textPrinter.Begin();
               textPrinter.Print("X: " + MouseLocation.X + ", Y: " + MouseLocation.Y.ToString(), font, Color.FromArgb(Properties.Settings.Default.BackgroundColor.ToArgb()^0xffffff), new RectangleF(MouseLocation.X + 10, MouseLocation.Y + 5, 100, 100));
               textPrinter.End();
           }

            if(currentAction == Actions.continousLines)
            {
                if(drawTempNode == true)
                {
                    GL.Begin(BeginMode.Points);
                    GL.Color3(Color.Red);
                    GL.Vertex2(tempNode.Vector.X, tempNode.Vector.Y);
                    GL.End();
                }
               
                if(continousLastNodeIndex != -1)
                {
                    GL.Begin(BeginMode.Lines);
                    GL.Color3(Color.Blue);
                    GL.Vertex2(tempPipe.Node1.Vector.X, tempPipe.Node1.Vector.Y);
                    GL.Vertex2(tempPipe.Node2.Vector.X, tempPipe.Node2.Vector.Y);
                    GL.End();
                }
            }

            GLControl.SwapBuffers();

        }
        void GLControl_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            #region continousLines
            if(currentAction == Actions.continousLines)
            {
                if(e.Button == MouseButtons.Left)
                {
                    bool isOver = false;
                    foreach (Node n in Nodes.Where(item => item.Index != tempNode.Index))//item.Vector != new Vector2(e.X, e.Y)
                    {
                        Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                        float distance = (vd.X * vd.X) + (vd.Y * vd.Y);
                        if (distance <= 30)
                        {

                            isOver = true;
                            overlapNode1 = n;
                            break;
                        }

                    }
                    if (!isOver)
                    {
                        Node n = new Node(new Vector2(e.Location.X, e.Location.Y), newNodeIndex());
                        foreach (object key in NodeGlobalProperties.Keys)
                        {
                            n.Data.Properties.Add(key, "empty;-10;-17;1");
                        }
                        
                        if (continousLastNodeIndex == -1)
                        {

                            tempPipe = new Pipe(newPipeIndex(), n, tempNode);
                          
                        }
                        else
                        {
                            
                           
                            Pipe p = new Pipe(newPipeIndex(), Nodes.Where(item => item.Index == continousLastNodeIndex).Single(), n);
                            foreach (object key in PipeGlobalProperties.Keys)
                            {
                                p.Data.Properties.Add(key, "empty;-10;-17;1");
                            }
                            Pipes.Add(p);
                            GLControl.Invalidate();
                        }
                        anteLastNodeContinous = continousLastNodeIndex;
                        Nodes.Add(n);
                        tempPipe.Node1 = Nodes.Where(item => item.Index == n.Index).Single();
                        continousLastNodeIndex = n.Index;
                    }
                    else
                    {
                        bool anteLast = false;
                        foreach (Node n in Nodes.Where(item => item.Index != tempNode.Index))//item.Vector != new Vector2(e.X, e.Y)
                        {
                            Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                            float distance = (vd.X * vd.X) + (vd.Y * vd.Y);
                            if (distance <= 30)
                            {

                                if(n.Index == anteLastNodeContinous)
                                {
                                    anteLast = true;
                                    break;
                                }
                                
                            }

                        }

                           if(!anteLast)
                           {
                               showNotification("Overlapping detected", "You want to merge the 2 nodes that you overlapped?", SystemIcons.Question.Handle, MessageBoxButtons.YesNo, true);
                               
                               if (NotificationResult == "Yes")
                               {
                                   Pipe p = new Pipe(newPipeIndex(), Nodes.Where(item => item.Index == continousLastNodeIndex).Single(), Nodes.Where(item => item.Index == overlapNode1.Index).Single());
                                   foreach (object key in PipeGlobalProperties.Keys)
                                   {
                                       p.Data.Properties.Add(key, "empty;-10;-17;1");
                                   }
                                   Pipes.Add(p);

                                   continousLastNodeIndex = -1;
                                   GLControl.Invalidate();
                               }
                               else
                               {
                                   Node n = new Node(new Vector2(e.Location.X, e.Location.Y), newNodeIndex());
                                   Nodes.Add(n);
                                   foreach (object key in NodeGlobalProperties.Keys)
                                   {
                                       n.Data.Properties.Add(key, "empty;-10;-17;1");
                                   }
                                   Pipe p = new Pipe(newPipeIndex(), Nodes.Where(item => item.Index == continousLastNodeIndex).Single(), n);
                                   foreach (object key in PipeGlobalProperties.Keys)
                                   {
                                       p.Data.Properties.Add(key, "empty;-10;-17;1");
                                   }
                                   Pipes.Add(p);

                                   continousLastNodeIndex = -1;
                                   GLControl.Invalidate();
                               }
                           }
                           else
                           {
                               showNotification("Error", "Cannot place a node over this one!", SystemIcons.Error.Handle, MessageBoxButtons.OK, false);
                                continousLastNodeIndex = -1;
                               GLControl.Invalidate();
                           }
                    }

                   
                    GLControl.Invalidate();
                }
            }
            #endregion
            #region drawNodes
            if (currentAction == Actions.drawNodes)
            {
                if (e.Button == MouseButtons.Left)
                {
                    bool isOver = false;
                    foreach (Node n in Nodes)
                    {
                        Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                        float distance = (vd.X * vd.X) + (vd.Y * vd.Y);
                        if (distance <= 30)
                        {
                           
                            isOver = true;
                            break;
                        }

                    }
                    if(!isOver)
                    {
                        Node newNode = new Node(new Vector2(e.X, e.Y), newNodeIndex());
                        newNode.layer = Layers.Where(item => item.isSelected).Single();
                        //if (openedMap != "")
                       // {
                            if (Nodes.Count > 0)
                            {
                                foreach (object key in NodeGlobalProperties.Keys)
                                {
                                    newNode.Data.Properties.Add(key, "empty;-10;-17;1");
                                }

                            }
                       // }
                        Nodes.Add(newNode);
                    }
                    else
                    {
                        showNotification("Error", "Cannot place a node over one that is already existing!", SystemIcons.Error.Handle, MessageBoxButtons.OK, false);
                    }
                   
                }
            }
            #endregion
            #region labels (text)
            if(currentAction == Actions.label)
            {
                if(e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    showNotification("Information", "You can close the text writing mode by pressing \"Enter\" key.\n Insert new row by pressing SHIFT + ENTER", SystemIcons.Information.Handle, MessageBoxButtons.OK, false);
                    labelTempTextBox.Location = e.Location;
                    labelTempTextBox.Visible = true;
                    labelTempTextBox.Text = "";
                    labelTempTextBox.Multiline = true;
                    labelTempTextBox.Width = 150;
                    labelTempTextBox.Height = 100;
                    labelTempTextBox.ForeColor = Color.White;

                    labelTempTextBox.BackColor = Color.DarkGray;
                    labelTempTextBox.BorderStyle = BorderStyle.Fixed3D;
                }
            }

            #endregion
            #region ContextMenu right click
            if (e.Button == MouseButtons.Right)
            {
                if(drawLabelRectangle == true)
                {
                     float textWidth = 0, textHeight = 0;
                     foreach (label lbl in Labels)
                     {
                         if (lbl.font == null)
                         {
                             textWidth = textPrinter.Measure(lbl.Text, font).BoundingBox.Width;
                             textHeight = textPrinter.Measure(lbl.Text, font).BoundingBox.Height;
                         }
                         else
                         {
                             textWidth = textPrinter.Measure(lbl.Text, lbl.font).BoundingBox.Width;
                             textHeight = textPrinter.Measure(lbl.Text, lbl.font).BoundingBox.Height;
                         }
                         // this.Text = (e.Location.X >= lbl.X - 3 && e.Location.X <= lbl.X + textWidth + 3 && e.Location.Y >= lbl.Y - 3 && e.Location.Y <= lbl.Y + textHeight + 3).ToString() + " " + textWidth.ToString() + " " + textHeight.ToString() + " lblX: " + lbl.X.ToString() + " lblY: " + lbl.Y.ToString() + " MX: " + e.Location.X + " MY: " + e.Location.Y;
                         if (e.Location.X >= lbl.X - 3 && e.Location.X <= lbl.X + textWidth + 3 && e.Location.Y >= lbl.Y - 3 && e.Location.Y <= lbl.Y + textHeight + 3)
                         {
                             labelContextMenu.Tag = lbl.Index.ToString();
                             labelContextMenu.Show(Cursor.Position);
                             return;
                         }
                     }
                    
                }
                foreach (Node n in Nodes)
                {
                    // Get the distance between mouse click and point
                    // http://www.purplemath.com/modules/distform.htm
                    Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                    float distance = (vd.X * vd.X) + (vd.Y * vd.Y);

                    // If the distance is lesser than 10 pixels we have
                    // a valid selection. (10 is same as the size of GLPoint)
                    // Console.WriteLine(distance);
                    if (distance <= 30)
                    {
                        int linesCount = 0;
                        selectedIndex = n.Index;
                        nodeProperties.Tag = n.Index.ToString();
                        deleteNode.Tag = n.Index.ToString();
                        nodeExit.Tag = n.Index.ToString();
                        nodeEnter.Tag = n.Index.ToString();
                        nodeNone.Tag = n.Index.ToString();
                        nodeEnter.ForeColor = Color.Black;
                        nodeExit.ForeColor = Color.Black;
                        nodeNone.ForeColor = Color.Black;
                       
                        if (n.Data.currentEnterExit == AdditionalNodeData.EnterExitType.Enter)
                        {
                            nodeEnter.Enabled = false;
                            nodeExit.Enabled = true;
                            nodeNone.Enabled = true;
                        }
                        else if(n.Data.currentEnterExit == AdditionalNodeData.EnterExitType.Exit)
                        {
                            nodeEnter.Enabled = true;
                            nodeExit.Enabled = false;
                            nodeNone.Enabled = true;
                        }
                        else if(n.Data.currentEnterExit == AdditionalNodeData.EnterExitType.None)
                        {

                            nodeEnter.Enabled = true;
                            nodeExit.Enabled = true;
                            nodeNone.Enabled = false;
                        }
                        if(Pipes.Where(item => item.Node1 == n).Count() > 0)
                        {
                            linesCount += Pipes.Where(item => item.Node1 == n).Count();
                        }
                        if (Pipes.Where(item => item.Node2 == n).Count() > 0)
                        {
                            linesCount += Pipes.Where(item => item.Node2 == n).Count();
                        }
                        if(linesCount > 1)
                        {
                            nodeEnter.ForeColor = Color.Red;
                            nodeExit.ForeColor = Color.Red;
                            nodeEnter.Tag = null;
                            nodeExit.Tag = null;
                        }
                        nodContextMenu.Show(Cursor.Position);
                        break;
                    }
                }
                if (selectedIndex == -1)
                {
                    foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Line))
                    {
                        if (PointOnLineSegment(new Point((int)p.Node1.Vector.X, (int)p.Node1.Vector.Y), new Point((int)p.Node2.Vector.X, (int)p.Node2.Vector.Y), e.Location))
                        {
                            selectedPipeIndex = p.Index;
                            pipeProperties.Tag = selectedPipeIndex.ToString();
                            pipeToLine.Tag = selectedPipeIndex.ToString();
                            deletePipe.Tag = selectedPipeIndex.ToString();
                            changeDirection.Tag = selectedPipeIndex.ToString();
                            Pipe myPipe = Pipes.Where(item => item.Index == p.Index).Single();
                            int pipesFromNode1 = 0, pipesFromNode2 = 0;
                            pipesFromNode1 += Pipes.Where(item => item.Node1.Index == myPipe.Node1.Index && item.Index != selectedPipeIndex).Count();

                            pipesFromNode2 += Pipes.Where(item => item.Node2.Index == myPipe.Node2.Index && item.Index != selectedPipeIndex).Count();

                            pipesFromNode2 += Pipes.Where(item => item.Node2.Index == myPipe.Node1.Index && item.Index != selectedPipeIndex).Count();

                            pipesFromNode1 += Pipes.Where(item => item.Node1.Index == myPipe.Node2.Index && item.Index != selectedPipeIndex).Count();

                            if ((pipesFromNode1 + pipesFromNode2) >= 1)
                            {
                                detachPipe.Enabled = true;
                            }
                            else detachPipe.Enabled = false;
                            detachPipe.Tag = selectedPipeIndex.ToString();
                            addNode.Tag = selectedPipeIndex.ToString() + ";" + e.X.ToString() +";" + e.Y.ToString();
                            if (p.ObjectType == Pipe.ObjectTypes.Type1)
                            {
                                changeToObject1.Enabled = false;
                                changeToObject2.Enabled = true;
                                changeToObject3.Enabled = true;
                                changeToObject4.Enabled = true;
                            }
                            else if (p.ObjectType == Pipe.ObjectTypes.Type2)
                            {
                                changeToObject1.Enabled = true;
                                changeToObject2.Enabled = false;
                                changeToObject3.Enabled = true;
                                changeToObject4.Enabled = true;
                            }
                            else if (p.ObjectType == Pipe.ObjectTypes.Type3)
                            {
                                changeToObject1.Enabled = true;
                                changeToObject2.Enabled = true;
                                changeToObject3.Enabled = false;
                                changeToObject4.Enabled = true;
                            }
                            else if (p.ObjectType == Pipe.ObjectTypes.Type4)
                            {
                                changeToObject1.Enabled = true;
                                changeToObject2.Enabled = true;
                                changeToObject3.Enabled = true;
                                changeToObject4.Enabled = false;
                            }
                            changeToObject1.Tag = p.Index.ToString();
                            changeToObject2.Tag = p.Index.ToString();
                            changeToObject3.Tag = p.Index.ToString();
                            changeToObject4.Tag = p.Index.ToString();
                            pipeToCurve.Enabled = true;
                            pipeToLine.Enabled = false;
                            pipeContextMenu.Show(Cursor.Position);
                            break;
                        }
                    }
                }
                if (selectedIndex == -1)
                {
                    foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                    {
                        BezierCurveQuadric bez = new BezierCurveQuadric(p.Node1.Vector, p.Node2.Vector, p.ControlPoint);
                        Vector2 pos = Vector2.One;

                        for (int points = 1; points <= p.drawnPoints; points++)
                        {

                            pos = bez.CalculatePoint((float)points / (float)p.drawnPoints);
                            if ((int)pos.X == (int)e.Location.X && (int)pos.Y == (int)e.Location.Y)
                            {
                                selectedPipeIndex = p.Index;
                                pipeProperties.Tag = selectedPipeIndex.ToString();
                                deletePipe.Tag = selectedPipeIndex.ToString();
                                pipeToLine.Tag = selectedPipeIndex.ToString();
                                pipeToCurve.Enabled = false;
                                pipeToLine.Enabled = true;
                                pipeContextMenu.Show(Cursor.Position);
                                break;
                            }
                        }
                    }
                }
                if(selectedIndex == -1 && selectedPipeIndex == -1)
                {
                    foreach(Image img in Images)
                    {
                        if(e.Location.X >= img.X && e.Location.X <= img.X + img.Width && e.Location.Y >= img.Y && e.Location.Y <= img.Y + img.Height)
                        {
                            imageProperties.Tag = img.Index.ToString();
                            imageContextMenu.Show(PointToScreen(new Point(e.Location.X + (int)(pipeContextMenu.Width / 1.1) - 39, e.Location.Y + 23)));
                            selectedImageIndex = img.Index;
                            break;
                        }
                    }
                }
            }
            #endregion

            GLControl.Invalidate();
        }
        void GLControl_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            isMouseDown = true;
            #region Panning with middle mouse
            if (e.Button == System.Windows.Forms.MouseButtons.Middle)
            {
                panningBool = true;
               // drawTextBool = false;
                lastAction = currentAction;
                currentAction = Actions.panning;
                if (currentAction == Actions.drawCurves)
                {
                    toolStripButton3.PerformClick();
                }
                else if (currentAction == Actions.drawLines)
                {
                    toolStripButton2.PerformClick();
                }
                else if (currentAction == Actions.drawNodes)
                {
                    toolStripButton1.PerformClick();
                }
                else if (currentAction == Actions.moveNodes)
                {
                    toolStripButton4.PerformClick();
                }
                else if (currentAction == Actions.panning)
                {
                    toolStripButton5.PerformClick();
                }
                else if (currentAction == Actions.select)
                {
                    toolStripButton3.PerformClick();
                }
                Cursor.Current = Cursors.SizeAll;
            }
            #endregion
            #region Move
            if (currentAction == Actions.moveNodes)
            {
                if (e.Button == MouseButtons.Left)
                {
                   // drawTextBool = false;
                    if (selectedNodes.Count > 0)
                    {
                        init_x = e.Location.X;
                        init_y = e.Location.Y;
                    }
                    else
                    {
                        if (selectedIndex == -1)
                        {
                            foreach (Node n in Nodes)
                            {
                                // Get the distance between mouse click and point
                                // http://www.purplemath.com/modules/distform.htm
                                Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                                float distance = (vd.X * vd.X) + (vd.Y * vd.Y);

                                // If the distance is lesser than 10 pixels we have
                                // a valid selection. (10 is same as the size of GLPoint)
                                // Console.WriteLine(distance);
                                if (distance <= 30)
                                {
                                    selectedIndex = n.Index;
                                    break;
                                }

                            }
                            foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                            {
                                Vector2 vd = Vector2.Subtract(p.ControlPoint, new Vector2(e.X, e.Y));
                                float distance = (vd.X * vd.X) + (vd.Y * vd.Y);

                                // If the distance is lesser than 10 pixels we have
                                // a valid selection. (10 is same as the size of GLPoint)
                                // Console.WriteLine(distance);
                                if (distance <= 30)
                                {
                                    selectedControlPointIndex = p.ControlPointIndex;

                                    break;
                                }
                            }
                        }
                        if(selectedIndex == -1 && selectedControlPointIndex == -1 && selectedImageIndex == -1)
                        {
                           foreach(Image img in Images)
                           {
                               if (e.Location.X >= img.X && e.Location.X <= img.X + img.Width && e.Location.Y >= img.Y && e.Location.Y <= img.Y + img.Height)
                               {
                                   selectedImageIndex = img.Index;
                                   init_x = e.Location.X;
                                   init_y = e.Location.Y;
                                   break;
                               }
                           }
                        }
                         if(selectedIndex == -1 && selectedControlPointIndex == -1 && selectedImageIndex == -1)
                         {
                             float textWidth = 0, textHeight = 0;
                             bool found = false;
                             foreach (label lbl in Labels)
                             {
                                 if (lbl.font == null)
                                 {
                                     textWidth = textPrinter.Measure(lbl.Text, font).BoundingBox.Width;
                                     textHeight = textPrinter.Measure(lbl.Text, font).BoundingBox.Height;
                                 }
                                 else
                                 {
                                     textWidth = textPrinter.Measure(lbl.Text, lbl.font).BoundingBox.Width;
                                     textHeight = textPrinter.Measure(lbl.Text, lbl.font).BoundingBox.Height;
                                 }
                                   if (e.Location.X >= lbl.X - 3 && e.Location.X <= lbl.X + textWidth + 3 && e.Location.Y >= lbl.Y - 3 && e.Location.Y <= lbl.Y + textHeight + 3)
                                 {
                                     found = true;
                                     selectedLabelIndex = lbl.Index;
                                     init_x = e.Location.X;
                                     init_y = e.Location.Y;
                                     break;
                                     // MessageBox.Show(lbl.Text);
                                 }
                                 if (found == false)
                                 {
                                     drawLabelRectangle = false;
                                     selectedLabelIndex = -1;
                                 }
                             }
                         }

                    }
                }

            }
            #endregion
            #region drawLines
            else if (currentAction == Actions.drawLines)
            {
                if (e.Button == MouseButtons.Left)
                {
                   // drawTextBool = false;
                    if (selectedIndex == -1)
                    {
                        foreach (Node n in Nodes)
                        {
                            // Get the distance between mouse click and point
                            // http://www.purplemath.com/modules/distform.htm
                            Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                            float distance = (vd.X * vd.X) + (vd.Y * vd.Y);

                            // If the distance is lesser than 10 pixels we have
                            // a valid selection. (10 is same as the size of GLPoint)
                            // Console.WriteLine(distance);
                            if (distance <= 30)
                            {
                                selectedIndex = n.Index;
                                break;
                            }

                        }
                    }
                    else if (selectedIndex != -1 && selectedIndex2 == -1)
                    {
                        foreach (Node n in Nodes)
                        {
                            // Get the distance between mouse click and point
                            // http://www.purplemath.com/modules/distform.htm
                            Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                            float distance = (vd.X * vd.X) + (vd.Y * vd.Y);

                            // If the distance is lesser than 10 pixels we have
                            // a valid selection. (10 is same as the size of GLPoint)
                            // Console.WriteLine(distance);
                            if (distance <= 30)
                            {
                                selectedIndex2 = n.Index;
                                break;
                            }

                        }
                    }
                }
            }
            #endregion
            #region Panning
            else if (currentAction == Actions.panning)
            {
                if (e.Button == MouseButtons.Left || e.Button == System.Windows.Forms.MouseButtons.Middle)
                {
                    //drawTextBool = false;
                    panningBool = true;
                    init_x = e.X;
                    init_y = e.Y;
                    Cursor.Current = Cursors.SizeAll;
                }
            }
#endregion
            #region Select
            else if (currentAction == Actions.select)
            {
                if (e.Button == MouseButtons.Left)
                {
                  //  drawTextBool = false;
                    mouseDown = e.Location;
                    if (selectedNodes.Count > 0)
                    {
                        foreach (Node n in selectedNodes)
                        {
                            n.PointSize = 9f;
                        }
                        selectedNodes.Clear();
                    }

                    if (selectedPipes.Count > 0)
                    {
                        foreach (Pipe p in selectedPipes)
                        {
                            p.Width = 1f;
                        }
                        selectedPipes.Clear();
                    }
                }
            }
            #endregion
            #region DrawCurves
            else if (currentAction == Actions.drawCurves)
            {
                if (e.Button == MouseButtons.Left)
                {
                    //drawTextBool = false;
                    if (selectedIndex == -1)
                    {
                        foreach (Node n in Nodes)
                        {
                            // Get the distance between mouse click and point
                            // http://www.purplemath.com/modules/distform.htm
                            Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                            float distance = (vd.X * vd.X) + (vd.Y * vd.Y);

                            // If the distance is lesser than 10 pixels we have
                            // a valid selection. (10 is same as the size of GLPoint)
                            // Console.WriteLine(distance);
                            if (distance <= 30)
                            {
                                selectedIndex = n.Index;
                                break;
                            }

                        }
                    }
                    else if (selectedIndex != -1 && selectedIndex2 == -1)
                    {
                        foreach (Node n in Nodes)
                        {
                            // Get the distance between mouse click and point
                            // http://www.purplemath.com/modules/distform.htm
                            Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                            float distance = (vd.X * vd.X) + (vd.Y * vd.Y);

                            // If the distance is lesser than 10 pixels we have
                            // a valid selection. (10 is same as the size of GLPoint)
                            // Console.WriteLine(distance);
                            if (distance <= 30)
                            {
                                selectedIndex2 = n.Index;
                                break;
                            }

                        }
                    }
                }
            }
            #endregion

        }
        void GLControl_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            isMouseDown = false;
            panningBool = false;
            //drawTextBool = true;
            #region Move
            if (currentAction == Actions.moveNodes)
            {
                if (e.Button == MouseButtons.Left)
                {
                    if (selectedNodes.Count > 0)
                    {
                        init_x = 0;
                        init_y = 0;
                        foreach (Node n in selectedNodes.Where(item => item.auxVector != item.Vector))
                        {
                            n.auxVector = n.Vector;
                        }
                    }
                    else
                    {
                        if (selectedControlPointIndex != -1)
                        {
                            foreach (Pipe p in Pipes.Where(item => item.ControlPointIndex == selectedControlPointIndex))
                            {
                                p.auxControlPoint = p.ControlPoint;
                            }
                            selectedControlPointIndex = -1;
                        }

                        if (selectedIndex != -1)
                        {
                            foreach (Pipe p in Pipes.Where(item => item.Node1.Vector == Nodes.Where(item2 => item2.Index == selectedIndex).Single().Vector))
                            {
                                p.auxNode1 = p.Node1;
                            }
                            foreach (Pipe p in Pipes.Where(item => item.Node2.Vector == Nodes.Where(item2 => item2.Index == selectedIndex).Single().Vector))
                            {
                                p.auxNode2 = p.Node2;
                            }
                            Nodes.Where(item => item.Index == selectedIndex).Single().auxVector = Nodes.Where(item => item.Index == selectedIndex).Single().Vector;
                            selectedIndex = -1;
                        }
                    }
                    if(selectedImageIndex != -1)
                    {
                        Images.Where(item => item.Index == selectedImageIndex).Single().auxX = Images.Where(item => item.Index == selectedImageIndex).Single().X;
                        Images.Where(item => item.Index == selectedImageIndex).Single().auxY = Images.Where(item => item.Index == selectedImageIndex).Single().Y;
                        selectedImageIndex = -1;

                    }
                    if(selectedLabelIndex != -1)
                    {
                        Labels.Where(item => item.Index == selectedLabelIndex).Single().auxX = Labels.Where(item => item.Index == selectedLabelIndex).Single().X;
                        Labels.Where(item => item.Index == selectedLabelIndex).Single().auxY = Labels.Where(item => item.Index == selectedLabelIndex).Single().Y;
                        this.Text = "Set auxX to: " + Labels.Where(item => item.Index == selectedLabelIndex).Single().auxX.ToString() + " and auxY to: " + Labels.Where(item => item.Index == selectedLabelIndex).Single().auxY.ToString() + " with x, y: " + Labels.Where(item => item.Index == selectedLabelIndex).Single().X.ToString() + ", " + Labels.Where(item => item.Index == selectedLabelIndex).Single().Y.ToString();
                        selectedLabelIndex = -1;
        
                    }
                }
            }
            #endregion
            #region DrawLines
            else if (currentAction == Actions.drawLines)
            {
                if (selectedIndex != -1 && selectedIndex2 != -1)
                {
                    Pipe p = new Pipe(newPipeIndex(), Nodes.Where(item => item.Index == selectedIndex).Single(), Nodes.Where(item => item.Index == selectedIndex2).Single());
                    
                   //if(openedMap != "")
                  // {
                       if (openedMap != "")
                       {
                           if (Pipes.Count > 0)
                           {
                               foreach (object key in PipeGlobalProperties.Keys)
                               {
                                   p.Data.Properties.Add(key, "empty;-10;-17;1");
                               }

                           }
                       }
                   //}
                    Pipes.Add(p);

                    selectedIndex = -1;
                    selectedIndex2 = -1;
                }
            }
            #endregion
            #region Panning
            else if (currentAction == Actions.panning)
            {
                foreach (Node n in Nodes)
                {
                    n.auxVector = n.Vector;
                }
                foreach (Pipe p in Pipes)
                {
                    p.auxNode1 = p.Node1;
                    p.auxNode2 = p.Node2;
                }
                foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                {
                    p.auxControlPoint = p.ControlPoint;
                }
                foreach(Image img in Images.Where(item => item.Static == true))
                {
                    img.auxX = img.X;
                    img.auxY = img.Y;
                }
                foreach(gridLine g in Grid)
                {
                    g.auxX1 = g.X1;
                    g.auxX2 = g.X2;
                    g.auxY1 = g.Y1;
                    g.auxY2 = g.Y2;
                }
                if (e.Button == System.Windows.Forms.MouseButtons.Middle)
                {
                    currentAction = lastAction;
                    if (lastAction == Actions.drawCurves)
                    {
                        toolStripButton3.PerformClick();
                    }
                    else if (lastAction == Actions.drawLines)
                    {
                        toolStripButton2.PerformClick();
                    }
                    else if (lastAction == Actions.drawNodes)
                    {
                        toolStripButton1.PerformClick();
                    }
                    else if (lastAction == Actions.moveNodes)
                    {
                        toolStripButton4.PerformClick();
                    }
                    else if (lastAction == Actions.panning)
                    {
                        toolStripButton5.PerformClick();
                    }
                    else if (lastAction == Actions.select)
                    {
                        toolStripButton3.PerformClick();
                    }
                }
                gridHeight = Math.Abs(Grid[Grid.Where(item => item.Type == gridLine._Type.Horizontal && item.Index + 1 != hMinID && item.Index + 1 != hMaxID && item.Index + 1 < Grid.Count(itemC => itemC.Type == gridLine._Type.Horizontal) && item.Index != hMinID && item.Index != hMaxID && item.Index < Grid.Count(itemC => itemC.Type == gridLine._Type.Horizontal)).OrderByDescending(itemO => itemO.Y1).First().Index + 1].Y1 - Grid[Grid.Where(item => item.Type == gridLine._Type.Horizontal && item.Index + 1 != hMinID && item.Index + 1 != hMaxID && item.Index + 1 < Grid.Count(itemC => itemC.Type == gridLine._Type.Horizontal) && item.Index != hMinID && item.Index != hMaxID && item.Index < Grid.Count(itemC => itemC.Type == gridLine._Type.Horizontal)).OrderByDescending(itemO => itemO.Y1).First().Index].Y1);
                gridWidth = gridHeight;
                
                refreshGrid();
                foreach (gridLine g in Grid.Where(item => item.Type == gridLine._Type.Vertical))
                {
                    g.auxX1 = g.X1;
                    g.auxX2 = g.X2;
                }
                foreach (gridLine g in Grid.Where(item => item.Type == gridLine._Type.Horizontal))
                {
                    g.auxY1 = g.Y1;
                    g.auxY2 = g.Y2;
                }
                foreach(label lbl in Labels.Where(item => item.Static == false))
                {
                    lbl.auxX = lbl.X;
                    lbl.auxY = lbl.Y;
                }
                //if (!backgroundWorker1.IsBusy) backgroundWorker1.RunWorkerAsync();
            }
            #endregion
            #region Select
            else if (currentAction == Actions.select)
            {
                // selectedIndex = -1;
                mouseDown = Point.Empty;
            }
            #endregion
            #region DrawCurves
            else if (currentAction == Actions.drawCurves)
            {
                if (selectedIndex != -1 && selectedIndex2 != -1)
                {
                    float ctrlPointLocationX = (Nodes.Where(item => item.Index == selectedIndex).Single()
                    .Vector.X + Nodes.Where(item => item.Index == selectedIndex2).Single()
                    .Vector.X) / 2;
                    float ctrlPointLocationY = (Nodes.Where(item => item.Index == selectedIndex).Single()
.Vector.Y + Nodes.Where(item => item.Index == selectedIndex2).Single()
.Vector.Y) / 2;
                    if (ctrlPointLocationX < Nodes.Where(item => item.Index == selectedIndex).Single()
.Vector.X + 100 || ctrlPointLocationX < Nodes.Where(item => item.Index == selectedIndex).Single()
.Vector.X - 100) ctrlPointLocationX += ctrlPointLocationX * 0.5f;
                    if (ctrlPointLocationY < Nodes.Where(item => item.Index == selectedIndex).Single()
.Vector.Y + 100 || ctrlPointLocationX < Nodes.Where(item => item.Index == selectedIndex).Single()
.Vector.Y - 100) ctrlPointLocationY += ctrlPointLocationY * 0.5f;
                    Pipes.Add(new Pipe(newPipeIndex(), Nodes.Where(item => item.Index == selectedIndex).Single()
, Nodes.Where(item => item.Index == selectedIndex2).Single()
, Pipe.PipeTypes.Curve, new Vector2(ctrlPointLocationX, ctrlPointLocationY)));
                    selectedIndex = -1;
                    selectedIndex2 = -1;
                    selectedControlPointIndex = -1;
                }
            }
            #endregion
            #region Overlapp
            if (overlappedNodes)
            {
                showNotification("Overlapping detected", "You want to merge the 2 nodes that you overlapped?", SystemIcons.Question.Handle, MessageBoxButtons.YesNo, true);
                if (NotificationResult == "Yes")
                {
                    Node overlapN1 = Nodes.Where(item => item == overlapNode1).Single();
                    foreach (Pipe p in Pipes.Where(item => item.Node1 == overlapN1))
                    {
                        p.Node1 = overlapNode2;
                    }
                    foreach (Pipe p in Pipes.Where(item => item.Node2 == overlapN1))
                    {
                        p.Node2 = overlapNode2;
                    }
                    Nodes.Remove(overlapNode1);
                }
            }
            overlappedNodes = false;
            Point cursorLocation = e.Location;

            if (overlappedNodeWithLine)
            {
                showNotification("Overlapping detected", "Snap the node with the pipe?", SystemIcons.Question.Handle, MessageBoxButtons.YesNo, true);
                if (NotificationResult == "Yes")
                {
                    string[] parameters = new string[4]; // [0] = Pipe ID, [1] = e.X, [2] = e.Y
                    parameters[0] = overlappedNodeWithLinePipe.Index.ToString();
                    parameters[1] = cursorLocation.X.ToString();
                    parameters[2] = cursorLocation.Y.ToString();
                    Node auxNode1 = Pipes.Where(item => item.Index == Int32.Parse(parameters[0])).Single().Node1;
                    Node auxNode2 = Pipes.Where(item => item.Index == Int32.Parse(parameters[0])).Single().Node2;
                    Pipes.Remove(Pipes.Where(item => item.Index == Int32.Parse(parameters[0])).Single());
                    Node nodeToAdd = new Node(new Vector2(Int32.Parse(parameters[1]), Int32.Parse(parameters[2])), newNodeIndex());
                    Nodes.Add(nodeToAdd);
                    foreach (Pipe p in Pipes.Where(item => item.Node1 == overlappedNodeWithLineNode))
                    {
                        p.Node1 = nodeToAdd;
                    }
                    foreach (Pipe p in Pipes.Where(item => item.Node2 == overlappedNodeWithLineNode))
                    {
                        p.Node2 = nodeToAdd;
                    }
                    Pipes.Add(new Pipe(newPipeIndex(), auxNode1, nodeToAdd));
                    Pipes.Add(new Pipe(newPipeIndex(), nodeToAdd, auxNode2));

                    Nodes.Remove(overlappedNodeWithLineNode);
                }
            }
            overlappedNodeWithLine = false;
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                selectedIndex = -1;
                selectedPipeIndex = -1;
                GLControl.Invalidate();
            }
            #endregion
        }
        void GLControl_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if(currentAction != Actions.panning)
            {
                MouseLocation = e.Location;
            }
            if (!isMouseDown)
            {
                #region Highlight objects
                if (selectedNodes.Count == 0)
                {
                    if (selectedNodeIndex == -1)
                    {
                        foreach (Node n in Nodes)
                        {
                            Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                            float distance = (vd.X * vd.X) + (vd.Y * vd.Y);
                            if (distance <= 30)
                            {
                                selectedNodeIndex = n.Index;
                                Nodes.Where(item => item.Index == selectedNodeIndex).Single().PointSize = 12f;
                                foreach (Node n2 in Nodes)
                                {
                                    if (n2.Index != selectedNodeIndex) n2.PointSize = 9f;
                                }
                                /* shadow subsystem */
                                foreach (Node n3 in Nodes.Where(item => item.Subnetwork == n.Subnetwork))
                                {
                                    n3.DrawShadow = true;
                                }
                                foreach(Pipe p in Pipes.Where(item => item.Subnetwork == n.Subnetwork))
                                {
                                    p.DrawShadow = true;
                                }
                                break;
                            }
                        }
                    }
                    else
                    {
                        foreach (Node n in Nodes)
                        {
                            Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                            float distance = (vd.X * vd.X) + (vd.Y * vd.Y);

                            if (distance <= 30)
                            {
                                selectedNodeIndex = n.Index;
                                Nodes.Where(item => item.Index == selectedNodeIndex).Single().PointSize = 12f;
                                foreach (Node n2 in Nodes)
                                {
                                    if (n2.Index != selectedNodeIndex) n2.PointSize = 9f;
                                }
                                /* shadow subsystem */
                                foreach (Node n3 in Nodes.Where(item => item.Subnetwork == n.Subnetwork))
                                {
                                    n3.DrawShadow = true;
                                }
                                foreach (Pipe p in Pipes.Where(item => item.Subnetwork == n.Subnetwork))
                                {
                                    p.DrawShadow = true;
                                }
                                break;
                            }
                            else
                            {
                                selectedNodeIndex = -1;
                                foreach (Node n2 in Nodes.Where(item => item.PointSize == 12f))
                                {
                                    n2.PointSize = 9f;
                                }
                                foreach (Node n3 in Nodes.Where(item => item.Subnetwork == n.Subnetwork))
                                {
                                    n3.DrawShadow = false;
                                }
                                foreach(Pipe p in Pipes.Where(item => item.Subnetwork == n.Subnetwork))
                                {
                                    p.DrawShadow = false;
                                }
                            }

                        }
                    }
                    if (selectedNodeIndex != -1)
                    {
                        foreach (Pipe p in Pipes.Where(item => item.Width == 3f))
                        {
                            p.Width = 1f;
                            selectedPipeIndex = -1;
                        }
                    }
                    if (selectedPipeIndex == -1 && selectedNodeIndex == -1)
                    {
                        foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Line))
                        {
                            if (PointOnLineSegment(new Point((int)p.Node1.Vector.X, (int)p.Node1.Vector.Y), new Point((int)p.Node2.Vector.X, (int)p.Node2.Vector.Y), e.Location))
                            {
                                selectedPipeIndex = p.Index;
                                Pipes.Where(item => item.Index == selectedPipeIndex).Single().Width = 3f;
                                foreach (Pipe p2 in Pipes)
                                {
                                    if (p2.Index != selectedPipeIndex) p2.Width = 1f;
                                }
                                /* shadow subsystem */
                                foreach (Node n3 in Nodes.Where(item => item.Subnetwork == p.Subnetwork))
                                {
                                    n3.DrawShadow = true;
                                }
                                foreach (Pipe p3 in Pipes.Where(item => item.Subnetwork == p.Subnetwork))
                                {
                                    p3.DrawShadow = true;
                                }
                                break;

                            }
                            else
                            {
                                p.Width = 1f;
                                selectedPipeIndex = -1;
                                foreach (Pipe p3 in Pipes.Where(item => item.DrawShadow == true))
                                {
                                    p3.DrawShadow = false;
                                }
                                foreach (Node n3 in Nodes.Where(item => item.Subnetwork == p.Subnetwork))
                                {
                                    n3.DrawShadow = false;
                                }
                            }
                        }
                        if (selectedPipeIndex == -1 && selectedNodeIndex == -1)
                        {
                            foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                            {
                                BezierCurveQuadric bez = new BezierCurveQuadric(p.Node1.Vector, p.Node2.Vector, p.ControlPoint);
                                Vector2 pos = Vector2.One;

                                for (int points = 1; points <= p.drawnPoints; points++)
                                {

                                    pos = bez.CalculatePoint((float)points / (float)p.drawnPoints);
                                    if ((int)pos.X == (int)e.Location.X && (int)pos.Y == (int)e.Location.Y)
                                    {
                                        selectedPipeIndex = p.Index;
                                        Pipes.Where(item => item.Index == selectedPipeIndex).Single().Width = 3f;
                                        /* shadow subsystem */
                                        foreach (Pipe p3 in Pipes.Where(item => item.Subnetwork == p.Subnetwork))
                                        {
                                            p3.DrawShadow = true;
                                        }
                                        foreach (Node n3 in Nodes.Where(item => item.Subnetwork == p.Subnetwork))
                                        {
                                            n3.DrawShadow = true;
                                        }
                                        selectedNodeIndex = -1;
                                        break;
                                    }
                                    else
                                    {
                                        selectedPipeIndex = -1;
                                        foreach (Pipe p2 in Pipes.Where(item => item.Width == 3f))
                                        {
                                            p.Width = 1f;
                                        }
                                        foreach (Pipe p3 in Pipes.Where(item => item.DrawShadow == true))
                                        {
                                            p3.DrawShadow = false;
                                        }
                                        foreach (Node n3 in Nodes.Where(item => item.Subnetwork == p.Subnetwork))
                                        {
                                            n3.DrawShadow = false;
                                        }

                                    }

                                }

                            }
                        }
                    }
                    else
                    {
                        foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Line))
                        {

                            if (PointOnLineSegment(new Point((int)p.Node1.Vector.X, (int)p.Node1.Vector.Y), new Point((int)p.Node2.Vector.X, (int)p.Node2.Vector.Y), e.Location))
                            {
                                break;
                            }
                            else
                            {
                                p.Width = 1f;
                                selectedPipeIndex = -1;
                            }
                        }
                        foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                        {

                            BezierCurveQuadric bez = new BezierCurveQuadric(p.Node1.Vector, p.Node2.Vector, p.ControlPoint);
                            Vector2 pos = Vector2.One;

                            for (int points = 1; points <= p.drawnPoints; points++)
                            {

                                pos = bez.CalculatePoint((float)points / (float)p.drawnPoints);
                                if ((int)pos.X == (int)e.Location.X && (int)pos.Y == (int)e.Location.Y)
                                {
                                    p.Width = 3f;
                                    /* shadow subsystem */
                                    foreach (Pipe p3 in Pipes.Where(item => item.Subnetwork == p.Subnetwork))
                                    {
                                        p3.DrawShadow = true;
                                    }
                                    foreach (Node n3 in Nodes.Where(item => item.Subnetwork == p.Subnetwork))
                                    {
                                        n3.DrawShadow = true;
                                    }
                                    selectedPipeIndex = p.Index;
                                    selectedNodeIndex = -1;
                                    break;
                                }
                                else
                                {
                                    selectedPipeIndex = -1;
                                    foreach (Pipe p2 in Pipes.Where(item => item.Width == 3f))
                                    {
                                        p2.Width = 1f;
                                    }
                                    foreach(Pipe p3 in Pipes.Where(item => item.DrawShadow == true))
                                    {
                                        p3.DrawShadow = false;
                                    }
                                    foreach (Node n3 in Nodes.Where(item => item.Subnetwork == p.Subnetwork))
                                    {
                                        n3.DrawShadow = true;
                                    }
                                }

                            }
                        }
                    }
                }
                if (selectedNodeIndex == -1 && selectedPipeIndex == -1)
                {
                    foreach (Image img in Images)
                    {
                        if (e.Location.X >= img.X && e.Location.X <= img.X + img.Width && e.Location.Y >= img.Y && e.Location.Y <= img.Y + img.Height)
                        {
                            img.Border = true;
                            break;
                        }
                        else img.Border = false;
                    }
                }
                else
                {
                    foreach (Image img in Images.Where(item => item.Border == true))
                    {
                        img.Border = false;
                    }
                }
                if(selectedNodeIndex == -1 && selectedPipeIndex == -1)
                {
                    float textWidth = 0, textHeight = 0;
                    bool found = false;
                    foreach(label lbl in Labels)
                    {
                        if (lbl.font == null)
                        {
                            textWidth = textPrinter.Measure(lbl.Text, font).BoundingBox.Width;
                            textHeight = textPrinter.Measure(lbl.Text, font).BoundingBox.Height;
                        }
                        else
                        {
                            textWidth = textPrinter.Measure(lbl.Text, lbl.font).BoundingBox.Width;
                            textHeight = textPrinter.Measure(lbl.Text, lbl.font).BoundingBox.Height;
                        }
                       // this.Text = (e.Location.X >= lbl.X - 3 && e.Location.X <= lbl.X + textWidth + 3 && e.Location.Y >= lbl.Y - 3 && e.Location.Y <= lbl.Y + textHeight + 3).ToString() + " " + textWidth.ToString() + " " + textHeight.ToString() + " lblX: " + lbl.X.ToString() + " lblY: " + lbl.Y.ToString() + " MX: " + e.Location.X + " MY: " + e.Location.Y;
                        if (e.Location.X >= lbl.X - 3 && e.Location.X <= lbl.X + textWidth + 3 && e.Location.Y >= lbl.Y - 3 && e.Location.Y <= lbl.Y + textHeight + 3)
                        {
                            found = true;
                            labelRectangle.X = lbl.X-3;
                            labelRectangle.Y = lbl.Y-3;
                            labelRectangle.Width = textWidth + 6;
                            labelRectangle.Height = textHeight + 6;
                            drawLabelRectangle = true;
                           // selectedLabelIndex = lbl.Index;
                            break;
                            // MessageBox.Show(lbl.Text);
                        }
                        if (found == false)
                        {
                            drawLabelRectangle = false;
                            //selectedLabelIndex = -1;
                        }
                    }
                }
                #endregion
            }
            #region continousLines
            if(currentAction == Actions.continousLines)
            {
                tempNode.Vector = new Vector2(e.Location.X, e.Location.Y);
                if(continousLastNodeIndex != -1)
                {
                    drawTempNode = true;
                   // tempPipe.Node1 = Nodes.Where(item => item.Index == continousLastNodeIndex).Single();
                    tempPipe.Node2.Vector = new Vector2(e.Location.X, e.Location.Y);
                }
            }
            #endregion
            
            #region MoveNodes
            if (currentAction == Actions.moveNodes)
            {
                #region Move selected objects
                if (selectedNodes.Count > 0 && e.Button == MouseButtons.Left)
                {
                    foreach (Node n in selectedNodes)
                    {
                        n.Vector = new Vector2(n.auxVector.X + (e.X - init_x), n.auxVector.Y + (e.Y - init_y));
                    }
                }
                #endregion
                else
                {
                    if (selectedIndex != -1)
                    {
                        if(Nodes.Count > 1)
                        {
                            foreach (Node n in Nodes.Where(item => item.Index != selectedIndex))
                            {
                                Vector2 vd = Vector2.Subtract(n.Vector, new Vector2(e.X, e.Y));
                                float distance = (vd.X * vd.X) + (vd.Y * vd.Y);
                                if (distance <= 30)
                                {
                                    Nodes.Where(item => item.Index == selectedIndex).Single().Vector = n.Vector;
                                    Nodes.Where(item => item.Index == selectedIndex).Single().PointSize = 9f;
                                    overlappedNodes = true;
                                    overlapNode1 = Nodes.Where(item => item.Index == selectedIndex).Single();
                                    overlapNode2 = n;
                                    overlappedNodeWithLine = false;
                                    break;
                                }
                                else
                                {
                                    Nodes.Where(item => item.Index == selectedIndex).Single().Vector = new Vector2(e.X, e.Y);
                                    Nodes.Where(item => item.Index == selectedIndex).Single().PointSize = 12f;
                                    overlappedNodes = false;
                                }
                            }
                        }
                        else
                        {
                            Nodes.Where(item => item.Index == selectedIndex).Single().Vector = new Vector2(e.X, e.Y);
                        }
                        if(overlappedNodes == false)
                        {
                            /*
                             if (PointOnLineSegment(new Point((int)p.Node1.Vector.X, (int)p.Node1.Vector.Y), new Point((int)p.Node2.Vector.X, (int)p.Node2.Vector.Y), e.Location))
                             */
                            foreach(Pipe p in Pipes.Where(item => item.Node1.Index != selectedIndex && item.Node2.Index != selectedIndex))
                            {
                                if(PointOnLineSegment(new Point((int)p.Node1.Vector.X, (int)p.Node1.Vector.Y), new Point((int)p.Node2.Vector.X, (int)p.Node2.Vector.Y), e.Location))
                                {
                                    p.Width = 3;
                                    overlappedNodes = false;
                                    overlappedNodeWithLine = true;
                                    overlappedNodeWithLineNode = Nodes.Where(item => item.Index == selectedIndex).Single();
                                    overlappedNodeWithLinePipe = p;
                                  //  Nodes.Where(item => item.Index == selectedIndex).Single().Vector = new Vector2();
                                    break;
                                }
                                else
                                {
                                    p.Width = 1;
                                    overlappedNodeWithLine = false;
                                }
                            }
                        }
                        if (Pipes.Where(item => item.Node2 == Nodes.Where(item2 => item2.Index == selectedIndex).Single()
     ).Count() > 0 && Pipes.Where(item => item.Node1 == Nodes.Where(item2 => item2.Index == selectedIndex).Single()
     ).Count() > 0)
                        {
                            foreach (Pipe pMaster in Pipes.Where(item => item.Node2 == Nodes.Where(item2 => item2.Index == selectedIndex).Single()))
                            {
                                nod = pMaster.Node2;
                               // Nodes.Where(item => item.Index == selectedIndex).Single().Vector = new Vector2(e.X, e.Y);
                                foreach (Pipe p in Pipes.Where(item => item.Node2 == nod))
                                {
                                    p.Node2 = Nodes.Where(item => item.Index == selectedIndex).Single();
                                }
                                foreach (Pipe p in Pipes.Where(item => item.Node1 == nod))
                                {
                                    p.Node1 = Nodes.Where(item => item.Index == selectedIndex).Single();
                                }
                            }
                        }

                        if (Pipes.Where(item => item.Node1 == Nodes.Where(item2 => item2.Index == selectedIndex).Single()
     ).Count() > 0)
                        {
                            nod = Pipes.Where(item => item.Node1 == Nodes.Where(item2 => item2.Index == selectedIndex).Single()
     ).First().Node1;
                           // Nodes.Where(item => item.Index == selectedIndex).Single().Vector = new Vector2(e.X, e.Y);
                            Pipes.Where(item => item.Node1 == nod).First().Node1 = Nodes.Where(item => item.Index == selectedIndex).Single();
                            foreach (Pipe p in Pipes.Where(item => item.Node1 == nod))
                            {
                                p.Node1 = Nodes.Where(item => item.Index == selectedIndex).Single();
                            }
                        }
                        if (Pipes.Where(item => item.Node2 == Nodes.Where(item2 => item2.Index == selectedIndex).Single()
     ).Count() > 0)
                        {
                            nod = Pipes.Where(item => item.Node2 == Nodes.Where(item2 => item2.Index == selectedIndex).Single()).First().Node2;
                            //Nodes.Where(item => item.Index == selectedIndex).Single().Vector = new Vector2(e.X, e.Y);
                            Pipes.Where(item => item.Node2 == nod).First().Node2 = Nodes.Where(item => item.Index == selectedIndex).Single();
                            foreach (Pipe p in Pipes.Where(item => item.Node2 == nod))
                            {
                                p.Node2 = Nodes.Where(item => item.Index == selectedIndex).Single();
                            }

                        }

                        
                    }
                    if (selectedControlPointIndex != -1)
                    {
                        if (Pipes.Where(item => item.ControlPointIndex == selectedControlPointIndex).Count() > 0)
                        {

                            foreach (Pipe p in Pipes.Where(item => item.ControlPointIndex == selectedControlPointIndex))
                            {
                                p.ControlPoint = new Vector2(e.X, e.Y);
                            }
                        }
                    }
                    if(selectedImageIndex != -1)
                    {
                        Image img = Images.Where(item => item.Index == selectedImageIndex).Single();
                        img.X = img.auxX + (e.X - init_x);
                        img.Y = img.auxY + (e.Y - init_y);
                       
                    }
                    if(selectedLabelIndex != -1)
                    {
                        Labels.Where(item => item.Index == selectedLabelIndex).Single().X = Labels.Where(item => item.Index == selectedLabelIndex).Single().auxX + (e.X - init_x);
                        Labels.Where(item => item.Index == selectedLabelIndex).Single().Y = Labels.Where(item => item.Index == selectedLabelIndex).Single().auxY + (e.Y - init_y);
                        labelRectangle.X = Labels.Where(item => item.Index == selectedLabelIndex).Single().X;
                        labelRectangle.Y = Labels.Where(item => item.Index == selectedLabelIndex).Single().Y;
                    }
                }
            }
            #endregion
            #region Panning
            else if (currentAction == Actions.panning)
            {
                if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Middle)
                {
                    panningBool = true;
                    foreach (Node n in Nodes)
                    {
                        n.Vector = new Vector2(n.auxVector.X + (e.X - init_x), n.auxVector.Y + (e.Y - init_y));
                    }

                    foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                    {
                        p.ControlPoint = new Vector2(p.auxControlPoint.X + (e.X - init_x), p.auxControlPoint.Y + (e.Y - init_y));
                    }
                    foreach(Image img in Images.Where(item => item.Static == true))
                    {
                        img.X = img.auxX + (e.X - init_x);
                        img.Y = img.auxY + (e.Y - init_y);
                    }

                    foreach(gridLine g in Grid.Where(item => item.Type == gridLine._Type.Horizontal))
                    {
                        g.Y1 = g.auxY1 + (e.Y - init_y);
                        g.Y2 = g.auxY2 + (e.Y - init_y);
                    }
                    foreach(gridLine g in Grid.Where(item => item.Type == gridLine._Type.Vertical))
                    {
                        g.X1 = g.auxX1 + (e.X - init_x);
                        g.X2 = g.auxX2 + (e.X - init_x);
                    }
                    foreach(label lbl in Labels.Where(item => item.Static == false))
                    {
                        lbl.X = lbl.auxX + (e.X - init_x);
                        lbl.Y = lbl.auxY + (e.Y - init_y);
                    }
                    

                }
            }
            #endregion
            #region Select
            else if (currentAction == Actions.select && mouseDown != Point.Empty)
            {
                if (e.Button == MouseButtons.Left)
                {
                    selectedNodes.Clear();
                    selectedPipes.Clear();
                    foreach (Node n in Nodes)
                    {
                        if (n.Vector.X > selectionRectangle.Left && n.Vector.X < selectionRectangle.Right && n.Vector.Y > selectionRectangle.Top && n.Vector.Y < selectionRectangle.Bottom)
                        {
                            n.PointSize = 13f;
                            selectedNodes.Add(n);
                        }
                        else
                        {
                            n.PointSize = 9f;
                            try
                            {
                                selectedNodes.Remove(n);
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    foreach (Pipe p in Pipes)
                    {
                        if (p.Node1.Vector.X > selectionRectangle.Left && p.Node1.Vector.X < selectionRectangle.Right && p.Node1.Vector.Y > selectionRectangle.Top && p.Node1.Vector.Y < selectionRectangle.Bottom && p.Node2.Vector.X > selectionRectangle.Left && p.Node2.Vector.X < selectionRectangle.Right && p.Node2.Vector.Y > selectionRectangle.Top && p.Node2.Vector.Y < selectionRectangle.Bottom)
                        {
                            p.Width = 3f;
                            selectedPipes.Add(p);
                        }
                        else
                        {
                            p.Width = 1f;
                            try
                            {
                                selectedPipes.Remove(p);
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
            }
            #endregion
            #region Cursor settings
            if (panningBool == false)
            {
                if (selectedPipeIndex != -1 || selectedNodeIndex != -1 || selectedControlPointIndex != -1)
                {
                    this.Cursor = Cursors.Hand;
                }
                else
                {
                   // this.Cursor = Cursors.Default;
                }
            }
            else Cursor.Current = Cursors.SizeAll;
            #endregion
            GLControl.Invalidate();
        }

        private void GLControl_MouseLeave(object sender, EventArgs e)
        {
            MouseLocation = new Point(-100, -100);
            drawTempNode = false;
            // tempNode.Vector = new Vector2(-100, -100);
            GLControl.Invalidate();
        }

        #endregion

        #region Toolstrip menu event handlers
        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            t = new Thread(openImage);
            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
            t.Join();
        }
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectedPipes.Clear();
            selectedNodes.Clear();
            PipeGlobalProperties.Clear();
            NodeGlobalProperties.Clear();
            Nodes.Clear();
            Pipes.Clear();
            openedMap = "";
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void nodesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void pipesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            currentAction = Actions.drawNodes;
            toolStripButton1.Checked = true;
            toolStripButton2.Checked = false;
            toolStripButton3.Checked = false;
            toolStripButton5.Checked = false;
            toolStripButton4.Checked = false;
            toolStripButton6.Checked = false;
            toolStripButton9.Checked = false;
            toolStripButton16.Checked = false;
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            currentAction = Actions.drawLines;
            toolStripButton2.Checked = true;
            toolStripButton1.Checked = false;
            toolStripButton3.Checked = false;
            toolStripButton5.Checked = false;
            toolStripButton4.Checked = false;
            toolStripButton6.Checked = false;
            toolStripButton9.Checked = false;
            toolStripButton16.Checked = false;
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            currentAction = Actions.select;
            toolStripButton3.Checked = true;
            toolStripButton2.Checked = false;
            toolStripButton4.Checked = false;
            toolStripButton1.Checked = false;
            toolStripButton5.Checked = false;
            toolStripButton6.Checked = false;
            toolStripButton9.Checked = false;
            toolStripButton16.Checked = false;
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            currentAction = Actions.moveNodes;
            toolStripButton4.Checked = true;
            toolStripButton2.Checked = false;
            toolStripButton3.Checked = false;
            toolStripButton1.Checked = false;
            toolStripButton5.Checked = false;
            toolStripButton6.Checked = false;
            toolStripButton9.Checked = false;
            toolStripButton16.Checked = false;
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            currentAction = Actions.panning;
            toolStripButton5.Checked = true;
            toolStripButton2.Checked = false;
            toolStripButton1.Checked = false;
            toolStripButton4.Checked = false;
            toolStripButton3.Checked = false;
            toolStripButton6.Checked = false;
            toolStripButton9.Checked = false;
            toolStripButton16.Checked = false;
            continousLastNodeIndex = -1;
            GLControl.Invalidate();

        }
        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            currentAction = Actions.drawCurves;
            toolStripButton6.Checked = true;
            toolStripButton5.Checked = false;
            toolStripButton4.Checked = false;
            toolStripButton3.Checked = false;
            toolStripButton2.Checked = false;
            toolStripButton1.Checked = false;
            toolStripButton9.Checked = false;
            toolStripButton16.Checked = false;
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            currentAction = Actions.continousLines;
            toolStripButton9.Checked = true;
            toolStripButton5.Checked = false;
            toolStripButton2.Checked = false;
            toolStripButton1.Checked = false;
            toolStripButton4.Checked = false;
            toolStripButton3.Checked = false;
            toolStripButton6.Checked = false;
            toolStripButton9.Checked = false;
            toolStripButton16.Checked = false;
            continousLastNodeIndex = -1;
            showNotification("Information", "To cancel the \"Free lines\" mode press Escape button.", SystemIcons.Information.Handle, MessageBoxButtons.OK, false);
            GLControl.Invalidate();
        }
        private void toolStripButton16_Click(object sender, EventArgs e)
        {
            currentAction = Actions.label;
            toolStripButton1.Checked = false;
            toolStripButton2.Checked = false;
            toolStripButton3.Checked = false;
            toolStripButton5.Checked = false;
            toolStripButton4.Checked = false;
            toolStripButton6.Checked = false;
            toolStripButton9.Checked = false;
            toolStripButton16.Checked = true;
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void toolStripButton10_Click(object sender, EventArgs e)
        {
            //type 2 pipe
            if(selectedPipes.Count < 1)
            {
                showNotification("Error: None of the pipes are selected", "First select one or more pipes with the selection tool.", SystemIcons.Error.Handle, MessageBoxButtons.OK, false);
            }
            else
            {
                foreach(Pipe p in selectedPipes)
                {
                    Pipes.Where(item => item.Index == p.Index).Single().ObjectType = Pipe.ObjectTypes.Type2;
                }
                GLControl.Invalidate();
            }
        }
        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            if(Nodes.Count > 1)
                for (int i = 0; i <= 4; i++)
                {
                    float h_max = 0, w_max = 0, h_min = 0, w_min = 0;
                    h_max = h_min = Nodes[0].Vector.Y;
                    w_max = w_min = Nodes[0].Vector.X;
                    foreach (Node n in Nodes)
                    {
                        if (w_min > n.Vector.X) w_min = n.Vector.X;
                        if (h_min > n.Vector.Y) h_min = n.Vector.Y;

                    }
                    foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                    {
                        if (w_min > p.ControlPoint.X) w_min = p.ControlPoint.X;
                        if (h_min > p.ControlPoint.Y) h_min = p.ControlPoint.Y;
                    }
                    foreach (Node n in Nodes)
                    {
                        n.Vector = new Vector2(n.Vector.X - w_min, n.Vector.Y - h_min);
                        n.auxVector = new Vector2(n.auxVector.X - w_min, n.auxVector.Y - h_min);
                    }
                    foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                    {
                        p.ControlPoint = new Vector2(p.ControlPoint.X - w_min, p.ControlPoint.Y - h_min);
                        p.auxControlPoint = new Vector2(p.auxControlPoint.X - w_min, p.auxControlPoint.Y - h_min);
                    }
                    foreach (Node n in Nodes)
                    {
                        if (n.Vector.X > w_max) w_max = n.Vector.X;
                        if (n.Vector.Y > h_max) h_max = n.Vector.Y;
                    }
                    foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                    {
                        if (w_max < p.ControlPoint.X) w_max = p.ControlPoint.X;
                        if (h_max < p.ControlPoint.Y) h_max = p.ControlPoint.Y;
                    }

                    foreach (Pipe p in Pipes.Where(item => item.PipeType == Pipe.PipeTypes.Curve))
                    {
                        p.ControlPoint = new Vector2(p.ControlPoint.X * ((GLControl.Size.Width - 50) / w_max) + 25, (p.ControlPoint.Y * ((GLControl.Size.Height - 50) / h_max)) + 25);
                        p.auxControlPoint = new Vector2(p.auxControlPoint.X * ((GLControl.Size.Width - 50) / w_max) + 25, (p.auxControlPoint.Y * ((GLControl.Size.Height - 50) / h_max)) + 25);
                    }

                    foreach (Node n in Nodes)
                    {
                        n.Index = Nodes.IndexOf(n);
                        n.Vector = new Vector2(n.Vector.X * ((GLControl.Size.Width - 50) / w_max) + 25, (n.Vector.Y * ((GLControl.Size.Height - 50) / h_max)) + 25);
                        n.auxVector = new Vector2(n.auxVector.X * ((GLControl.Size.Width - 50) / w_max) + 25, (n.auxVector.Y * ((GLControl.Size.Height - 50) / h_max)) + 25);
                    }
                }
            continousLastNodeIndex = -1;
            GLControl.Invalidate();

        }
        private void toolStripButton11_Click(object sender, EventArgs e)
        {
            //entering node
            if (selectedNodes.Count < 1)
            {
                showNotification("Error: None of the nodes are selected", "First select one or more nodes with the selection tool.", SystemIcons.Error.Handle, MessageBoxButtons.OK, false);
            }
            else
            {
                int imposibleNodes = 0;
                int linesCount;
                foreach (Node n in selectedNodes)
                {
                    linesCount = 0;
                    if (Pipes.Where(item => item.Node1 == n).Count() > 0)
                    {
                        linesCount += Pipes.Where(item => item.Node1 == n).Count();
                    }
                    if (Pipes.Where(item => item.Node2 == n).Count() > 0)
                    {
                        linesCount += Pipes.Where(item => item.Node2 == n).Count();
                    }
                    if (linesCount > 1)
                    {
                        imposibleNodes++;
                    }
                }
                if (imposibleNodes > 0)
                {
                    showNotification("Confirm action", (selectedNodes.Count - imposibleNodes).ToString() + " out of " + selectedNodes.Count.ToString() + " selected nodes can be converted.\n Convert them?", SystemIcons.Question.Handle, MessageBoxButtons.YesNo, true);
                    if (selectedNodes.Count == 1 || imposibleNodes == selectedNodes.Count || NotificationResult == "Yes")
                    {
                            foreach (Node n in selectedNodes)
                            {
                                linesCount = 0;
                                if (Pipes.Where(item => item.Node1 == n).Count() > 0)
                                {
                                    linesCount += Pipes.Where(item => item.Node1 == n).Count();
                                }
                                if (Pipes.Where(item => item.Node2 == n).Count() > 0)
                                {
                                    linesCount += Pipes.Where(item => item.Node2 == n).Count();
                                }
                                if (linesCount < 2)
                                {
                                    Nodes.Where(item => item.Index == n.Index).Single().Data.currentEnterExit = AdditionalNodeData.EnterExitType.Enter;
                                }
                            }
                    }
                }
                GLControl.Invalidate();
            }
        }
        private void toolStripButton14_Click(object sender, EventArgs e)
        {
            //entering node
            if (selectedNodes.Count < 1)
            {
                showNotification("Error: None of the nodes are selected", "First select one or more nodes with the selection tool.", SystemIcons.Error.Handle, MessageBoxButtons.OK, false);
            }
            else
            {
                int imposibleNodes = 0;
                int linesCount;
                foreach (Node n in selectedNodes)
                {
                    linesCount = 0;
                    if (Pipes.Where(item => item.Node1 == n).Count() > 0)
                    {
                        linesCount += Pipes.Where(item => item.Node1 == n).Count();
                    }
                    if (Pipes.Where(item => item.Node2 == n).Count() > 0)
                    {
                        linesCount += Pipes.Where(item => item.Node2 == n).Count();
                    }
                    if (linesCount > 1)
                    {
                        imposibleNodes++;
                    }
                }
                if (imposibleNodes > 0)
                {
                    showNotification("Confirm action", (selectedNodes.Count - imposibleNodes).ToString() + " out of " + selectedNodes.Count.ToString() + " selected nodes can be converted.\n Convert them?", SystemIcons.Question.Handle, MessageBoxButtons.YesNo, true);
                    if (selectedNodes.Count == 1 || imposibleNodes == selectedNodes.Count || NotificationResult == "Yes")
                    {
                        foreach (Node n in selectedNodes)
                        {
                            linesCount = 0;
                            if (Pipes.Where(item => item.Node1 == n).Count() > 0)
                            {
                                linesCount += Pipes.Where(item => item.Node1 == n).Count();
                            }
                            if (Pipes.Where(item => item.Node2 == n).Count() > 0)
                            {
                                linesCount += Pipes.Where(item => item.Node2 == n).Count();
                            }
                            if (linesCount < 2)
                            {
                                Nodes.Where(item => item.Index == n.Index).Single().Data.currentEnterExit = AdditionalNodeData.EnterExitType.Exit;
                            }
                        }
                    }
                }
                GLControl.Invalidate();
            }
        }
        private void toolStripButton15_Click(object sender, EventArgs e)
        {
            //type 2 pipe
            if(selectedPipes.Count < 1)
            {
                showNotification("Error: None of the pipes are selected", "First select one or more pipes with the selection tool.", SystemIcons.Error.Handle, MessageBoxButtons.OK, false);
            }
            else
            {
                foreach(Pipe p in selectedPipes)
                {
                    Pipes.Where(item => item.Index == p.Index).Single().ObjectType = Pipe.ObjectTypes.Type1;
                }
                GLControl.Invalidate();
            }
        }
        private void toolStripButton12_Click(object sender, EventArgs e)
        {
            //type 3 pipe
            if (selectedPipes.Count < 1)
            {
                showNotification("Error: None of the pipes are selected", "First select one or more pipes with the selection tool.", SystemIcons.Error.Handle, MessageBoxButtons.OK, false);
            }
            else
            {
                foreach (Pipe p in selectedPipes)
                {
                    Pipes.Where(item => item.Index == p.Index).Single().ObjectType = Pipe.ObjectTypes.Type3;
                }
                GLControl.Invalidate();
            }
        }
        private void toolStripButton13_Click(object sender, EventArgs e)
        {
            //type 4 pipe
            if (selectedPipes.Count < 1)
            {
                showNotification("Error: None of the pipes are selected", "First select one or more pipes with the selection tool.", SystemIcons.Error.Handle, MessageBoxButtons.OK, false);
            }
            else
            {
                foreach (Pipe p in selectedPipes)
                {
                    Pipes.Where(item => item.Index == p.Index).Single().ObjectType = Pipe.ObjectTypes.Type4;
                }
                GLControl.Invalidate();
            }
        }
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //if (toolStripMenuItem1.Checked == true)
            //{
            //    toolStripButton1.Text = "Nodes (N)";
            //    toolStripButton2.Text = "Lines from nodes (L)";
            //    toolStripButton6.Text = "Curved lines from nodes (C)";
            //    toolStripButton4.Text = "Move (M)";
            //    toolStripButton3.Text = "Select (S)";
            //    toolStripButton5.Text = "Panning (P)";
            //    toolStripButton7.Text = "Fit to screen (F)";
            //}
            //else
            //{
            //    toolStripButton1.Text = "Nodes";
            //    toolStripButton2.Text = "Lines from nodes";
            //    toolStripButton6.Text = "Curved lines from nodes";
            //    toolStripButton4.Text = "Move";
            //    toolStripButton3.Text = "Select";
            //    toolStripButton5.Text = "Panning";
            //    toolStripButton7.Text = "Fit to screen";
            //}
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newToolStripMenuItem.PerformClick();
            t = new Thread(open);
            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
            t.Join();

            this.Text = "NETGAS CAD - " + openedMap;

        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            t = new Thread(save);
            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
            t.Join();
        }
        private void gridToolStripMenuItem_Click(object sender, EventArgs e)
        {
            drawGridBool = gridToolStripMenuItem.Checked;
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void networkSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NetworkSettings ns = new NetworkSettings(this);
            ns.ShowDialog();
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void generalSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void saveAsImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string path = "", extension = "";
            int ok = 0;
            t = new Thread(() =>
            {
                SaveFileDialog save = new SaveFileDialog();

                save.Filter = "JPEG file (*.jpg)|*.jpg|PNG file (*.png)|*.png|PDF File (*.PDF)|*.pdf";
                if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    path = save.FileName;
                    extension = Path.GetExtension(save.FileName).ToString().ToLower();
                    continousLastNodeIndex = -1;
                    ok = 1;
                }


            });
            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
            t.Join();
               if(ok==1)
               {
                  if(extension == ".jpg" || extension == ".png")
                  {
                      Bitmap bmp = GrabScreenshot();
                      bmp.Save(path);
                      bmp.Dispose();
                  }
                  else if(extension == ".pdf")
                  {
                      FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
                      iTextSharp.text.Document doc = new iTextSharp.text.Document();
                      iTextSharp.text.pdf.PdfWriter pdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(doc, fs);
                      doc.Open();
                      Bitmap bmp = GrabScreenshot();
                      bmp.Save("temp_pdf.jpg");
                      iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance("temp_pdf.jpg");
                      if(img.Width > img.Height)
                      {
                          img.RotationDegrees = -90;
                      }
                      img.ScaleToFit(doc.PageSize.Width, doc.PageSize.Height - doc.PageSize.Height / 8);
                      img.SetDpi(300, 300);
                      doc.Add(img);
                      doc.Close();
                      File.Delete("temp_pdf.jpg");
                  }
               }

               GLControl.Invalidate();
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void colorSchemeSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            continousLastNodeIndex = -1;
            ColorSettings CS = new ColorSettings(this);
            CS.ShowDialog();

            GLControl.Invalidate();
        }
        private void controlPointsForCurvedLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void globalSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GlobalNetworkSettings GNS = new GlobalNetworkSettings(this);
            GNS.ShowDialog();
            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void addSubsystemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddSubnetwork add = new AddSubnetwork(this);
            add.ShowDialog();

            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void manageSubsystemsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            manageSubnetworks mng = new manageSubnetworks(this);
            mng.ShowDialog();

            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void drawnTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            drawTextBool = drawnTextToolStripMenuItem.Checked;

            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void fontSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog fontdialog = new FontDialog();
            if (fontdialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                font = fontdialog.Font;
                GLControl.Invalidate();
            }

            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        #endregion

        #region Help methods

        private void drawLabelAssignment(label mylabel, Node n, Pipe p)
        {
            Font tempFont;
            if (mylabel.font == null) tempFont = font;
            else tempFont = mylabel.font;
            
            
                GL.Color3(Color.Red);
                GL.LineWidth(1f);
                GL.Begin(PrimitiveType.LineLoop);
                if(mylabel.assignmentStartPosition == 1)
                {
                    //upper left
                    GL.Vertex2(mylabel.X - textPrinter.Measure(mylabel.Text, tempFont).BoundingBox.Width, mylabel.Y - textPrinter.Measure(mylabel.Text, tempFont).BoundingBox.Height * 0.5);
                }
                else if (mylabel.assignmentStartPosition == 2)
                {
                    //upper right
                    GL.Vertex2(mylabel.X + textPrinter.Measure(mylabel.Text, tempFont).BoundingBox.Width * 2, mylabel.Y - textPrinter.Measure(mylabel.Text, tempFont).BoundingBox.Height * 0.5);
                }
                else if(mylabel.assignmentStartPosition == 3)
                {
                    //lower right
                    GL.Vertex2(mylabel.X + textPrinter.Measure(mylabel.Text, tempFont).BoundingBox.Width * 2, mylabel.Y + textPrinter.Measure(mylabel.Text, tempFont).BoundingBox.Height * 1.5);
                }
                else if (mylabel.assignmentStartPosition == 4)
                {
                    //lower left
                    GL.Vertex2(mylabel.X - textPrinter.Measure(mylabel.Text, tempFont).BoundingBox.Width, mylabel.Y + textPrinter.Measure(mylabel.Text, tempFont).BoundingBox.Height * 1.5);
                }
                if (n != null)
                GL.Vertex2(n.Vector.X, n.Vector.Y);  
                else
                    GL.Vertex2((p.Node1.Vector.X + p.Node2.Vector.X) / 2, (p.Node1.Vector.Y + p.Node2.Vector.Y) / 2);
                
                GL.End();
            
            
        }

        public void showNotification(string Title, string Content, IntPtr icon, MessageBoxButtons buttons, bool Dialog)
        {
            if (notification.IsDisposed) notification = new Notification(this);
            notification.buttonState = buttons;
            notification.panel1.Invalidate();
            notification.mspassed = 0;
            notification.label1.Text = Title;
            notification.label2.Text = Content;
            notification.pictureBox1.Image = Bitmap.FromHicon(icon);
            notification.Opacity = .0;
            notification.timer1.Enabled = true;
            notification.Location = new Point(this.Location.X + this.Width - notification.Width - 15, this.Location.Y + this.Height - notification.Height - 40);
            
                notification.Visible = false;
            if (Dialog)
                notification.ShowDialog();
            else notification.Show();
            
        }

        private void openImage()
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Bitmap bmp = new Bitmap(openFileDialog1.FileName);
                Images.Add(new Image(newImageIndex(), openFileDialog1.FileName, 100, 100, bmp.Width, bmp.Height));
                GLControl.Invalidate();
            }

            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        public void drawImage(Image img)
        {
            Bitmap bitmap = new Bitmap(img.FileName);
            bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
           
            int texture;

           if(img.Border == true)
           {
               GL.Color3(Color.Black);
               GL.Begin(PrimitiveType.Quads);
               GL.Vertex2(img.X - imageBorderWidth, img.Y - imageBorderWidth);
               GL.Vertex2(img.X + img.Width + imageBorderWidth, img.Y - imageBorderWidth);
               GL.Vertex2(img.X + img.Width + imageBorderWidth, img.Y + img.Height + imageBorderWidth);
               GL.Vertex2(img.X - imageBorderWidth, img.Y + img.Height + imageBorderWidth);
               GL.End();
           }
            GL.Color3(Color.Transparent);
            //GL.ClearColor(Color.White);
            GL.Enable(EnableCap.Texture2D);

            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

            GL.GenTextures(1, out texture);
            GL.BindTexture(TextureTarget.Texture2D, texture);

            BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

            bitmap.UnlockBits(data);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);



            GL.BindTexture(TextureTarget.Texture2D, texture);

        

            GL.Begin(PrimitiveType.Quads);
            
            GL.TexCoord2(0.0f, 1.0f); GL.Vertex2(img.X, img.Y);
            GL.TexCoord2(1.0f, 1.0f); GL.Vertex2(img.X + img.Width, img.Y);
            GL.TexCoord2(1.0f, 0.0f); GL.Vertex2(img.X + img.Width, img.Y + img.Height);
            GL.TexCoord2(0.0f, 0.0f); GL.Vertex2(img.X, img.Y + img.Height);

            GL.End();
            bitmap.Dispose();
            GL.Disable(EnableCap.Texture2D);
        }
        public void drawNode(Node n)
        {
            /*
                newX = centerX + ( cosX * (point2X-centerX) + sinX * (point2Y -centerY))
                newY = centerY + ( -sinX * (point2X-centerX) + cosX * (point2Y -centerY))
            */
            float x, y, objectXhalf, objectYhalf, objectWidth = 20, objectHeight = 10;
            Pipe p;
            GL.PointSize(n.PointSize);
            GL.Begin(PrimitiveType.Points);
            if (selectedIndex != -1 && selectedIndex == n.Index)
                GL.Color3(Properties.Settings.Default.SelectionColor);
            else
                GL.Color3(Properties.Settings.Default.NodeColor);
            GL.Vertex2(n.Vector);
            GL.End();
            if (Pipes.Where(item => item.Node1 == n).Count() + Pipes.Where(item => item.Node2 == n).Count() != 1)
            {
                n.Data.currentEnterExit = AdditionalNodeData.EnterExitType.None;
                return;
            }

            if (n.Data.currentEnterExit == AdditionalNodeData.EnterExitType.Enter)
            {
                if (Pipes.Where(item => item.Node1 == n).Count() == 1)
                {
                    objectHeight = n.PointSize;
                    p = Pipes.Where(item => item.Node1 == n).Single();
                    float xDiff = p.Node2.Vector.X - p.Node1.Vector.X;
                    float yDiff = p.Node2.Vector.Y - p.Node1.Vector.Y;
                    Double Angle = (-1) * Math.Atan2(yDiff, xDiff);  // *180.0 / Math.PI;
                    objectXhalf = (p.Node1.Vector.X + p.Node2.Vector.X) / 2;
                    objectYhalf = (p.Node1.Vector.Y + p.Node2.Vector.Y) / 2;

                    GL.Begin(PrimitiveType.Polygon);
                    x = p.Node1.Vector.X + ((float)Math.Cos(Angle) * (p.Node1.Vector.X + objectWidth - p.Node1.Vector.X) + (float)Math.Sin(Angle) * (p.Node1.Vector.Y - p.Node1.Vector.Y));
                    y = p.Node1.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node1.Vector.X + objectWidth - p.Node1.Vector.X) + (float)Math.Cos(Angle) * (p.Node1.Vector.Y - p.Node1.Vector.Y));
                    if (selectedIndex != -1 && selectedIndex == n.Index)
                        GL.Color3(Properties.Settings.Default.SelectionColor);
                    else
                        GL.Color3(Properties.Settings.Default.NodeColor);
                    GL.Vertex2(x, y);
                    x = p.Node1.Vector.X + ((float)Math.Cos(Angle) * (p.Node1.Vector.X - p.Node1.Vector.X) + (float)Math.Sin(Angle) * (p.Node1.Vector.Y + objectHeight / 2 - p.Node1.Vector.Y));
                    y = p.Node1.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node1.Vector.X - p.Node1.Vector.X) + (float)Math.Cos(Angle) * (p.Node1.Vector.Y + objectHeight / 2 - p.Node1.Vector.Y));


                    GL.Vertex2(x, y);
                    x = p.Node1.Vector.X + ((float)Math.Cos(Angle) * (p.Node1.Vector.X - p.Node1.Vector.X) + (float)Math.Sin(Angle) * (p.Node1.Vector.Y - objectHeight / 2 - p.Node1.Vector.Y));
                    y = p.Node1.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node1.Vector.X - p.Node1.Vector.X) + (float)Math.Cos(Angle) * (p.Node1.Vector.Y - objectHeight / 2 - p.Node1.Vector.Y));
                    GL.Vertex2(x, y);
                    GL.End();
                }
                else if (Pipes.Where(item => item.Node2 == n).Count() == 1)
                {
                    objectHeight = n.PointSize;
                    p = Pipes.Where(item => item.Node2 == n).Single();
                    float xDiff = p.Node1.Vector.X - p.Node2.Vector.X;
                    float yDiff = p.Node1.Vector.Y - p.Node2.Vector.Y;
                    Double Angle = (-1) * Math.Atan2(yDiff, xDiff);  // *180.0 / Math.PI;
                    objectXhalf = (p.Node2.Vector.X + p.Node1.Vector.X) / 2;
                    objectYhalf = (p.Node2.Vector.Y + p.Node1.Vector.Y) / 2;

                    GL.Begin(PrimitiveType.Polygon);
                    x = p.Node2.Vector.X + ((float)Math.Cos(Angle) * (p.Node2.Vector.X + objectWidth - p.Node2.Vector.X) + (float)Math.Sin(Angle) * (p.Node2.Vector.Y - p.Node2.Vector.Y));
                    y = p.Node2.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node2.Vector.X + objectWidth - p.Node2.Vector.X) + (float)Math.Cos(Angle) * (p.Node2.Vector.Y - p.Node2.Vector.Y));
                    if (selectedIndex != -1 && selectedIndex == n.Index)
                        GL.Color3(Properties.Settings.Default.SelectionColor);
                    else
                        GL.Color3(Properties.Settings.Default.NodeColor);
                    GL.Vertex2(x, y);
                    x = p.Node2.Vector.X + ((float)Math.Cos(Angle) * (p.Node2.Vector.X - p.Node2.Vector.X) + (float)Math.Sin(Angle) * (p.Node2.Vector.Y + objectHeight / 2 - p.Node2.Vector.Y));
                    y = p.Node2.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node2.Vector.X - p.Node2.Vector.X) + (float)Math.Cos(Angle) * (p.Node2.Vector.Y + objectHeight / 2 - p.Node2.Vector.Y));


                    GL.Vertex2(x, y);
                    x = p.Node2.Vector.X + ((float)Math.Cos(Angle) * (p.Node2.Vector.X - p.Node2.Vector.X) + (float)Math.Sin(Angle) * (p.Node2.Vector.Y - objectHeight / 2 - p.Node2.Vector.Y));
                    y = p.Node2.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node2.Vector.X - p.Node2.Vector.X) + (float)Math.Cos(Angle) * (p.Node2.Vector.Y - objectHeight / 2 - p.Node2.Vector.Y));
                    GL.Vertex2(x, y);
                    GL.End();
                }
            }
            else if(n.Data.currentEnterExit == AdditionalNodeData.EnterExitType.Exit)
            {
                if (Pipes.Where(item => item.Node1 == n).Count() == 1)
                {
                    objectHeight = n.PointSize;
                    p = Pipes.Where(item => item.Node1 == n).Single();
                    float xDiff = p.Node2.Vector.X - p.Node1.Vector.X;
                    float yDiff = p.Node2.Vector.Y - p.Node1.Vector.Y;
                    Double Angle = (-1) * Math.Atan2(yDiff, xDiff);  // *180.0 / Math.PI;
                    objectXhalf = (p.Node1.Vector.X + p.Node2.Vector.X) / 2;
                    objectYhalf = (p.Node1.Vector.Y + p.Node2.Vector.Y) / 2;

                    GL.Begin(PrimitiveType.Polygon);
                    x = p.Node1.Vector.X + ((float)Math.Cos(Angle) * ( p.Node1.Vector.X - objectWidth - p.Node1.Vector.X) + (float)Math.Sin(Angle) * (p.Node1.Vector.Y - p.Node1.Vector.Y));
                    y = p.Node1.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node1.Vector.X - objectWidth - p.Node1.Vector.X) + (float)Math.Cos(Angle) * (p.Node1.Vector.Y - p.Node1.Vector.Y));
                    if (selectedIndex != -1 && selectedIndex == n.Index)
                        GL.Color3(Properties.Settings.Default.SelectionColor);
                    else
                        GL.Color3(Properties.Settings.Default.NodeColor);
                  
                    GL.Vertex2(x, y);
                    x = p.Node1.Vector.X + ((float)Math.Cos(Angle) * (p.Node1.Vector.X - p.Node1.Vector.X) + (float)Math.Sin(Angle) * (p.Node1.Vector.Y + objectHeight / 2 - p.Node1.Vector.Y));
                    y = p.Node1.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node1.Vector.X - p.Node1.Vector.X) + (float)Math.Cos(Angle) * (p.Node1.Vector.Y + objectHeight / 2 - p.Node1.Vector.Y));


                    GL.Vertex2(x, y);
                    x = p.Node1.Vector.X + ((float)Math.Cos(Angle) * (p.Node1.Vector.X - p.Node1.Vector.X) + (float)Math.Sin(Angle) * (p.Node1.Vector.Y - objectHeight / 2 - p.Node1.Vector.Y));
                    y = p.Node1.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node1.Vector.X - p.Node1.Vector.X) + (float)Math.Cos(Angle) * (p.Node1.Vector.Y - objectHeight / 2 - p.Node1.Vector.Y));
                    GL.Vertex2(x, y);
                    GL.End();
                }
                else if (Pipes.Where(item => item.Node2 == n).Count() == 1)
                {
                    objectHeight = n.PointSize;
                    p = Pipes.Where(item => item.Node2 == n).Single();
                    float xDiff = p.Node1.Vector.X - p.Node2.Vector.X;
                    float yDiff = p.Node1.Vector.Y - p.Node2.Vector.Y;
                    Double Angle = (-1) * Math.Atan2(yDiff, xDiff);  // *180.0 / Math.PI;
                    objectXhalf = (p.Node2.Vector.X + p.Node1.Vector.X) / 2;
                    objectYhalf = (p.Node2.Vector.Y + p.Node1.Vector.Y) / 2;

                    GL.Begin(PrimitiveType.Polygon);
                    x = p.Node2.Vector.X + ((float)Math.Cos(Angle) * (p.Node2.Vector.X - objectWidth - p.Node2.Vector.X) + (float)Math.Sin(Angle) * (p.Node2.Vector.Y - p.Node2.Vector.Y));
                    y = p.Node2.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node2.Vector.X - objectWidth - p.Node2.Vector.X) + (float)Math.Cos(Angle) * (p.Node2.Vector.Y - p.Node2.Vector.Y));
                    if (selectedIndex != -1 && selectedIndex == n.Index)
                        GL.Color3(Properties.Settings.Default.SelectionColor);
                    else
                        GL.Color3(Properties.Settings.Default.NodeColor);

                    GL.Vertex2(x, y);
                    x = p.Node2.Vector.X + ((float)Math.Cos(Angle) * (p.Node2.Vector.X - p.Node2.Vector.X) + (float)Math.Sin(Angle) * (p.Node2.Vector.Y + objectHeight / 2 - p.Node2.Vector.Y));
                    y = p.Node2.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node2.Vector.X - p.Node2.Vector.X) + (float)Math.Cos(Angle) * (p.Node2.Vector.Y + objectHeight / 2 - p.Node2.Vector.Y));


                    GL.Vertex2(x, y);
                    x = p.Node2.Vector.X + ((float)Math.Cos(Angle) * (p.Node2.Vector.X - p.Node2.Vector.X) + (float)Math.Sin(Angle) * (p.Node2.Vector.Y - objectHeight / 2 - p.Node2.Vector.Y));
                    y = p.Node2.Vector.Y + ((-1) * (float)Math.Sin(Angle) * (p.Node2.Vector.X - p.Node2.Vector.X) + (float)Math.Cos(Angle) * (p.Node2.Vector.Y - objectHeight / 2 - p.Node2.Vector.Y));
                    GL.Vertex2(x, y);
                    GL.End();
                }
            }
        }

        public void drawObject(Pipe.ObjectTypes objectType, Pipe p)
        {
            //get half location on the pipe
            float objectXhalf = (p.Node1.Vector.X + p.Node2.Vector.X) / 2;
            float objectYhalf = (p.Node1.Vector.Y + p.Node2.Vector.Y) / 2;
            //float PipeLength = Math.Sqrt((double)Math.Pow((double)(p.Node2.Vector.X - p.Node1.Vector.X), 2) + (double)Math.Pow((double)(p.Node2.Vector.Y - p.Node1.Vector.Y), 2));
            float PipeLength = (float)Math.Sqrt(Math.Pow((double)(p.Node2.Vector.X - p.Node1.Vector.X), 2) + Math.Pow((double)(p.Node2.Vector.Y - p.Node1.Vector.Y), 2));
            float xDiff = p.Node2.Vector.X - p.Node1.Vector.X;
            float yDiff = p.Node2.Vector.Y - p.Node1.Vector.Y;
            Double Angle = (-1) * Math.Atan2(yDiff, xDiff);  // *180.0 / Math.PI;
            float x, y;
            float objectHeight = 30f;
                float objectWidth = 30f;

            if(objectType == Pipe.ObjectTypes.Type2)
            {
                
                /*
                newX = centerX + ( cosX * (point2X-centerX) + sinX * (point2Y -centerY))
                newY = centerY + ( -sinX * (point2X-centerX) + cosX * (point2Y -centerY))
                 */
                if(PipeLength < 30)
                {
                    objectWidth = 5f;
                    objectHeight = 5f;
                }
                else if (PipeLength >= 30 && PipeLength <=70)
                {
                    objectWidth = 10f;
                    objectHeight = 10f;
                }
                else if(PipeLength > 70)
                {
                    objectWidth = 25f;
                    objectHeight = 25f;
                }
                // fill with bg color
                GL.Color3(Properties.Settings.Default.BackgroundColor);
                GL.Begin(PrimitiveType.Polygon);
                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf - objectHeight / 2);



                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf + objectHeight / 2);


                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf - objectHeight / 2);



                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf + objectHeight / 2);


                GL.End();






                GL.Color3(Properties.Settings.Default.PipeColor);
                GL.Begin(PrimitiveType.LineLoop);
                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf - objectHeight / 2);



                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1)*(float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf + objectHeight / 2);


                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf - objectHeight / 2);



                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf + objectHeight / 2);


                GL.End();
                
            }
            else if(objectType == Pipe.ObjectTypes.Type3)
            {
                if(PipeLength <= 20)
                {
                    objectWidth = 1f;
                    objectHeight = 1f;
                }
                if (PipeLength > 20 && PipeLength < 30)
                {
                    objectWidth = 8f;
                    objectHeight = 3f;
                }
                else if (PipeLength >= 30 && PipeLength <= 70)
                {
                    objectWidth = 13f;
                    objectHeight = 6f;
                }
                else if (PipeLength > 70 && PipeLength <= 100)
                {
                    objectWidth = 25f;
                    objectHeight = 12f;
                }
                else if (PipeLength >= 100)
                {
                    objectWidth = 30f;
                    objectHeight = 17f;
                }

                GL.Color3(Properties.Settings.Default.BackgroundColor);
                GL.Begin(PrimitiveType.Polygon);


                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 1.2f - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 1.2f - objectYhalf));
                GL.Vertex2(x, y);

                //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf - objectHeight / 1.2);

                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 1.2f - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 1.2f - objectYhalf));
                GL.Vertex2(x, y);

                //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf + objectHeight / 1.2);


                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);

               // GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf + objectHeight / 2);



                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);

               // GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf - objectHeight / 2);

                GL.End();

                GL.Color3(Properties.Settings.Default.PipeColor);

                GL.Begin(PrimitiveType.LineLoop);
      
               
                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 1.2f - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 1.2f - objectYhalf));
                GL.Vertex2(x, y);

                //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf - objectHeight / 1.2);

                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 1.2f - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 1.2f - objectYhalf));
                GL.Vertex2(x, y);

                //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf + objectHeight / 1.2);


                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);

               // GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf + objectHeight / 2);



                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);

               // GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf - objectHeight / 2);

                GL.End();
            }
            else if(objectType == Pipe.ObjectTypes.Type4)
            {

                if (PipeLength <= 20)
                {
                    objectWidth = 1f;
                    objectHeight = 1f;
                }
                if (PipeLength > 20 && PipeLength < 30)
                {
                    objectWidth = 8f;
                    objectHeight = 3f;
                }
                else if (PipeLength >= 30 && PipeLength <= 70)
                {
                    objectWidth = 13f;
                    objectHeight = 6f;
                }
                else if (PipeLength > 70)
                {
                    objectWidth = 30f;
                    objectHeight = 18f;
                }



                GL.Color3(Properties.Settings.Default.BackgroundColor);
                GL.Begin(PrimitiveType.Polygon);

                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);

                //GL.Vertex2(objectXhalf - objectwidth / 2, objectYhalf - objectHeight / 2);


                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf + objectHeight / 2);


                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf);
                GL.End();


                GL.Color3(Properties.Settings.Default.PipeColor);
                GL.Begin(PrimitiveType.LineLoop);

                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);

                //GL.Vertex2(objectXhalf - objectwidth / 2, objectYhalf - objectHeight / 2);


                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf + objectHeight / 2);


                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectYhalf));
                GL.Vertex2(x, y);
                //GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf);
                GL.End();

                GL.Color3(Properties.Settings.Default.PipeColor);
                GL.Begin(PrimitiveType.LineLoop);
                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);

                //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf - objectHeight / 2);

                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);

               // GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf + objectHeight / 2);

                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
               // GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf + objectHeight / 2);

                x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
                GL.Vertex2(x, y);
               // GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf - objectHeight / 2);
                GL.End();

            }
        }
        private void drawDirection(Pipe p)
        {
            if(p.Node1.Data.currentEnterExit == AdditionalNodeData.EnterExitType.Enter || p.Node2.Data.currentEnterExit == AdditionalNodeData.EnterExitType.Enter)
            {
                return;
            }
            float objectXhalf = 0, objectYhalf = 0;
            //float PipeLength = Math.Sqrt((double)Math.Pow((double)(p.Node2.Vector.X - p.Node1.Vector.X), 2) + (double)Math.Pow((double)(p.Node2.Vector.Y - p.Node1.Vector.Y), 2));
            float PipeLength = (float)Math.Sqrt(Math.Pow((double)(p.Node2.Vector.X - p.Node1.Vector.X), 2) + Math.Pow((double)(p.Node2.Vector.Y - p.Node1.Vector.Y), 2));
            float xDiff = p.Node2.Vector.X - p.Node1.Vector.X;
            float yDiff = p.Node2.Vector.Y - p.Node1.Vector.Y;
            Double Angle = (-1) * Math.Atan2(yDiff, xDiff);  // *180.0 / Math.PI; 
            float x, y;
            float objectHeight = 10f;
            float objectWidth = 10f;

            if (PipeLength > 100)
            {
                objectXhalf = (p.Node1.Vector.X + p.Node2.Vector.X) / 2;
                objectYhalf = (p.Node1.Vector.Y + p.Node2.Vector.Y) / 2;
                objectXhalf = (p.Node1.Vector.X + objectXhalf) / 2;
                objectYhalf = (p.Node1.Vector.Y + objectYhalf) / 2;
                objectXhalf = (p.Node1.Vector.X + objectXhalf) / 2;
                objectYhalf = (p.Node1.Vector.Y + objectYhalf) / 2;
            }


            GL.Color3(Properties.Settings.Default.PipeColor);
            GL.Begin(PrimitiveType.Polygon);

            x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
            y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectHeight / 2 - objectYhalf));
            GL.Vertex2(x, y);

            //GL.Vertex2(objectXhalf - objectwidth / 2, objectYhalf - objectHeight / 2);


            x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
            y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf + objectHeight / 2 - objectYhalf));
            GL.Vertex2(x, y);
            //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf + objectHeight / 2);


            x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectYhalf - objectYhalf));
            y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectYhalf - objectYhalf));
            GL.Vertex2(x, y);
            //GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf);
            GL.End();

            GL.Color3(Properties.Settings.Default.PipeColor);

            GL.Begin(PrimitiveType.LineLoop);

            x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectHeight / 2));
            y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectHeight / 2));
            GL.Vertex2(x, y);

            //GL.Vertex2(objectXhalf - objectwidth / 2, objectYhalf - objectHeight / 2);


            x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Sin(Angle) * (objectHeight / 2));
            y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf - objectWidth / 2 - objectXhalf) + (float)Math.Cos(Angle) * (objectHeight / 2));
            GL.Vertex2(x, y);
            //GL.Vertex2(objectXhalf - objectWidth / 2, objectYhalf + objectHeight / 2);


            x = objectXhalf + ((float)Math.Cos(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf));
            y = objectYhalf + ((-1) * (float)Math.Sin(Angle) * (objectXhalf + objectWidth / 2 - objectXhalf));
            GL.Vertex2(x, y);
            //GL.Vertex2(objectXhalf + objectWidth / 2, objectYhalf);
            GL.End();



        }
        public void loadLayersList()
        {
            LayersContainer.Controls.Clear();
            foreach(Layer layer in Layers)
            {
                layerComponent check = new layerComponent(this, layer.Name, layer.isVisible, layer.isSelected);
                LayersContainer.Controls.Add(check);

            }
            GLControl.Invalidate();
        }

        private void checkLayer_MouseClick(object sender, EventArgs e)
        {
            int Index =  Int32.Parse(((CheckBox)sender).Name.ToString()[((CheckBox)sender).Name.Length - 1].ToString());
            foreach(Layer layer in Layers.Where(item => item.isSelected == true))
            {
                layer.isSelected = false;
            }
            
            Layers.Where(item => item.Index == Index).Single().isSelected = true;
            if (Layers.Where(item => item.Index == Index).Single().isVisible == true) Layers.Where(item => item.Index == Index).Single().isVisible = false;
            else Layers.Where(item => item.Index == Index).Single().isVisible = true;
            loadLayersList();
        }
        public static Point Rotate(Point point, Point pivot, double angleDegree)
        {
            double angle = angleDegree * Math.PI / 180;
            double cos = Math.Cos(angle);
            double sin = Math.Sin(angle);
            int dx = point.X - pivot.X;
            int dy = point.Y - pivot.Y;
            double x = cos * dx - sin * dy + pivot.X;
            double y = sin * dx + cos * dy + pivot.X;

            Point rotated = new Point((int)Math.Round(x), (int)Math.Round(y));
            return rotated;
        }

        public int newLabelIndex()
        {
            bool ok = false;
            int newIndex = 0;
            if (Labels.Count > 0)
            {
                while (ok != true)
                {
                    if (Labels.Where(item => item.Index == newIndex).Count() > 0)
                    {
                        newIndex++;
                    }
                    else ok = true;
                }
            }
            return newIndex;
        }
        public int newLayerIndex()
        {
            bool ok = false;
            int newIndex = 0;
            if (Layers.Count > 0)
            {
                while (ok != true)
                {
                    if (Layers.Where(item => item.Index == newIndex).Count() > 0)
                    {
                        newIndex++;
                    }
                    else ok = true;
                }
            }
            return newIndex;
        }
        public int newPipeIndex()
        {
            bool ok = false;
            int newIndex = 0;
            if (Nodes.Count > 0)
            {
                while (ok != true)
                {
                    if (Pipes.Where(item => item.Index == newIndex).Count() > 0)
                    {
                        newIndex++;
                    }
                    else ok = true;
                }
            }
            return newIndex;
        }
        public int newNodeIndex()
        {
            bool ok = false;
            int newIndex = 0;
            if (Nodes.Count > 0)
            {
                while(ok!=true)
                {
                    if (Nodes.Where(item => item.Index == newIndex).Count() > 0)
                    {
                        newIndex++;
                    }
                    else ok = true;
                }
            }
           return newIndex;
        }

        public int newgridLineIndex()
        {
            bool ok = false;
            int newIndex = 0;
            if (Grid.Count > 0)
            {
                while (ok != true)
                {
                    if (Grid.Where(item => item.Index == newIndex).Count() > 0)
                    {
                        newIndex++;
                    }
                    else ok = true;
                }
            }
            return newIndex;
        }

        public int newImageIndex()
        {
            bool ok = false;
            int newIndex = 0;
            if (Nodes.Count > 0)
            {
                while (ok != true)
                {
                    if (Images.Where(item => item.Index == newIndex).Count() > 0)
                    {
                        newIndex++;
                    }
                    else ok = true;
                }
            }
            return newIndex;
        }
        public static bool PointOnLineSegment(Point pt1, Point pt2, Point pt, double epsilon = 3)
        {
            if (pt.X - Math.Max(pt1.X, pt2.X) > epsilon ||
                Math.Min(pt1.X, pt2.X) - pt.X > epsilon ||
                pt.Y - Math.Max(pt1.Y, pt2.Y) > epsilon ||
                Math.Min(pt1.Y, pt2.Y) - pt.Y > epsilon)
                return false;

            if (Math.Abs(pt2.X - pt1.X) < epsilon)
                return Math.Abs(pt1.X - pt.X) < epsilon || Math.Abs(pt2.X - pt.X) < epsilon;
            if (Math.Abs(pt2.Y - pt1.Y) < epsilon)
                return Math.Abs(pt1.Y - pt.Y) < epsilon || Math.Abs(pt2.Y - pt.Y) < epsilon;

            double x = pt1.X + (pt.Y - pt1.Y) * (pt2.X - pt1.X) / (pt2.Y - pt1.Y);
            double y = pt1.Y + (pt.X - pt1.X) * (pt2.Y - pt1.Y) / (pt2.X - pt1.X);

            return Math.Abs(pt.X - x) < epsilon || Math.Abs(pt.Y - y) < epsilon;
        }
        public void open()
        {
            int lineCount = 0, y_current = this.Size.Height;
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.InitializeLifetimeService();
            float h_max = 0, w_max = 0, h_min = 0, w_min = 0;


            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                PipeGlobalProperties.Clear();
                NodeGlobalProperties.Clear();
                openedMap = openFileDialog1.SafeFileName;
                lineCount = File.ReadAllLines(openFileDialog1.FileName).Length;
                StreamReader sr = new StreamReader(openFileDialog1.FileName);
                string line = "";
                int counter = 0, y_max = 0;
                string[] elem = new string[100];
                string[] fields = new string[100];
                for (int i = 1; i <= lineCount; i++)
                {
                    int lastPipeAdded = 0;
                    int index = Nodes.Count;
                    int id_aux1, id_aux2;
                    bool ok1 = true;
                    bool ok2 = true;
                    line = sr.ReadLine();
                    id_aux1 = -1;
                    id_aux2 = -1;
                    counter++;
                    if(counter == 1)
                    {
                        elem = line.Split('\t');
                        fields[8] = elem[8]; //node 1 name
                        fields[9] = elem[9]; // node 2 name
                        fields[10] = elem[10]; //length
                        fields[11] = elem[11]; //diameter
                        fields[12] = elem[12]; //roughness
                        fields[50] = elem[50];
                    }
                    else if (counter != 1)
                    {
                        elem = line.Split('\t');
                        if (counter == 2)
                        {
                            w_min = float.Parse(elem[16]);
                            h_min = float.Parse(elem[17]);
                        }

                        foreach (Node n in Nodes)
                        {
                            if (n.Index == Convert.ToInt32(elem[6]))
                            {
                                ok1 = false;
                                id_aux1 = Nodes.IndexOf(n);
                            }

                        }
                        if (ok1 == true)
                        {
                            Node x = new Node(new Vector2(float.Parse(elem[16]), float.Parse(elem[17])), Convert.ToInt32(elem[6]));
                            /* assign subnetwork if specific subnetworks file does not exist */
                            if (!File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_subnetworks.txt"))
                            {
                                x.Subnetwork = elem[50];
                                if (!Subnetworks.Contains(elem[50]))
                                {
                                    Subnetworks.Add(elem[50]);
                                }
                            }
                            Nodes.Add(x);
                        }
                        foreach (Node n in Nodes)
                        {
                            if (n.Index == Convert.ToInt32(elem[7]))
                            {
                                ok2 = false;
                                id_aux2 = Nodes.IndexOf(n);
                            }

                        }

                        if (ok2 == true)
                        {
                            Node x = new Node(new Vector2(float.Parse(elem[19]), float.Parse(elem[20])), Convert.ToInt32(elem[7]));
                            /* assign subnetwork if specific subnetworks file does not exist */
                            if (!File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_subnetworks.txt"))
                            {
                                x.Subnetwork = elem[50];
                                if (!Subnetworks.Contains(elem[50]))
                                {
                                    Subnetworks.Add(elem[50]);
                                }
                            } 
                            Nodes.Add(x);
                        }
                        if (elem[51] == "0" || elem[51] == "" || elem[51] == "-")
                        {
                            if (id_aux1 != -1 && id_aux2 != -1)
                            {
                                Pipe p = new Pipe(newPipeIndex(), Nodes[id_aux1], Nodes[id_aux2], elem);
                                lastPipeAdded = newPipeIndex();
                                Nodes[id_aux1].Name = elem[8];
                                Nodes[id_aux2].Name = elem[9];
                                Pipes.Add(p);
                            }
                            else if (id_aux1 == -1 && id_aux2 != -1)
                            {

                                Pipe p = new Pipe(newPipeIndex(), Nodes[Nodes.Count - 1], Nodes[id_aux2], elem);
                                lastPipeAdded = newPipeIndex();
                                Nodes[Nodes.Count - 1].Name = elem[8];
                                Nodes[id_aux2].Name = elem[9];
                                Pipes.Add(p);
                            }
                            else if (id_aux1 != -1 && id_aux2 == -1)
                            {
                                Pipe p = new Pipe(Convert.ToInt32(elem[3]), Nodes[id_aux1], Nodes[Nodes.Count - 1], elem);
                                lastPipeAdded = Convert.ToInt32(elem[3]);
                                Nodes[id_aux1].Name = elem[8];
                                Nodes[Nodes.Count - 1].Name = elem[9];
                                Pipes.Add(p);

                            }
                            else if (id_aux1 == -1 && id_aux2 == -1)
                            {
                                Pipe p;
                                //  =================================>  TO BE RECHECKED  <======================  \\
                                if (Pipes.Count - 1 < 0)
                                {
                                    p = new Pipe(newPipeIndex(), Nodes[Nodes.Count - 2], Nodes[Nodes.Count - 1], elem);
                                    lastPipeAdded = newPipeIndex();

                                    Nodes[Nodes.Count - 2].Name = elem[8];
                                    Nodes[Nodes.Count - 1].Name = elem[9];
                                }
                                else
                                {
                                    p = new Pipe(newPipeIndex(), Nodes[Nodes.Count - 2], Nodes[Nodes.Count - 1], elem);
                                    lastPipeAdded = newPipeIndex();

                                    Nodes[Nodes.Count -2].Name = elem[8];
                                    Nodes[Nodes.Count -1].Name = elem[9];
                                }
                                Pipes.Add(p);
                            }
                            if (!File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_nodes.txt") || !File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_pipes.txt"))
                            {
                                Pipes.Where(item => item.Index == lastPipeAdded).Single().Data.Properties.Add("Length", elem[10] + ";10;10;1");
                                Pipes.Where(item => item.Index == lastPipeAdded).Single().Data.Properties.Add("Diameter", elem[11] + ";10;-40;1");
                                Pipes.Where(item => item.Index == lastPipeAdded).Single().Data.Properties.Add("Roughness", elem[12] + ";-10;-10;1");
                            }
                        }
                        else if (elem[51] == "1")
                        {
                            if (id_aux1 != -1 && id_aux2 != -1)
                            {
                                Pipe p = new Pipe(newPipeIndex(), Nodes[id_aux1], Nodes[id_aux2], Pipe.PipeTypes.Curve, new Vector2(float.Parse(elem[52]), float.Parse(elem[53])), elem);
                                lastPipeAdded = newPipeIndex();
                                Pipes.Add(p);
                            }
                            else if (id_aux1 == -1 && id_aux2 != -1)
                            {
                                Pipe p = new Pipe(newPipeIndex(), Nodes[Nodes.Count - 1], Nodes[id_aux2], Pipe.PipeTypes.Curve, new Vector2(float.Parse(elem[52]), float.Parse(elem[53])), elem);
                                lastPipeAdded = newPipeIndex();
                                Pipes.Add(p);
                            }
                            else if (id_aux1 != -1 && id_aux2 == -1)
                            {
                                Pipe p = new Pipe(newPipeIndex(), Nodes[id_aux1], Nodes[Nodes.Count - 1], Pipe.PipeTypes.Curve, new Vector2(float.Parse(elem[52]), float.Parse(elem[53])), elem);
                                lastPipeAdded = newPipeIndex();
                                Pipes.Add(p);

                            }
                            else if (id_aux1 == -1 && id_aux2 == -1)
                            {
                                Pipe p = new Pipe(newPipeIndex(), Nodes[Nodes.Count - 2], Nodes[Nodes.Count - 1], Pipe.PipeTypes.Curve, new Vector2(float.Parse(elem[52]), float.Parse(elem[53])), elem);
                                lastPipeAdded = newPipeIndex();
                                Pipes.Add(p);
                            }
                            if (!File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_nodes.txt") || !File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_pipes.txt"))
                            {
                                Pipes.Where(item => item.Index == lastPipeAdded).Single().Data.Properties.Add("Length", elem[10] + ";10;10;1");
                                Pipes.Where(item => item.Index == lastPipeAdded).Single().Data.Properties.Add("Diameter", elem[11] + ";10;-40;1");
                                Pipes.Where(item => item.Index == lastPipeAdded).Single().Data.Properties.Add("Roughness", elem[12] + ";-10;-10;1");
                            }
                        }
                    }
                }
                sr.Close();
                sr.DiscardBufferedData();
                sr.Dispose(); 


                /*Append to subnetwork*/
                if (File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_subnetworks.txt"))
                {
                    
                    string lineSubnetwork;
                    lineCount = File.ReadAllLines(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_subnetworks.txt").Length;
                    StreamReader srSubnetwork = new StreamReader(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_subnetworks.txt");
                    counter = 0;
                    string[] properties = new string[100];
                    string[] elem2 = new string[100];
                    for (int i = 1; i <= lineCount; i++)
                    {
                        lineSubnetwork = srSubnetwork.ReadLine();
                        counter++;
                        elem2 = lineSubnetwork.ToString().Split('\t');
                        if (counter != 1)
                        {
                            if (elem[2] == "") elem[2] = String.Empty;
                            if (elem2[0] == "Node")
                            {
                                Nodes.Where(item => item.Index == Int32.Parse(elem2[1])).Single().Subnetwork = elem2[2];
                            }
                            else if(elem2[0] == "Pipe")
                            {
                                Pipes.Where(item => item.Index == Int32.Parse(elem2[1])).Single().Subnetwork = elem2[2];
                            }

                            if (!Subnetworks.Contains(elem2[2]))
                            {
                                Subnetworks.Add(elem2[2]);
                            }
                        }
                    }
                    srSubnetwork.Close();
                    srSubnetwork.DiscardBufferedData();
                    srSubnetwork.Dispose();
                }

                /* general data */
                if (File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_general.txt"))
                {

                    line = "";
                    lineCount = File.ReadAllLines(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_general.txt").Length;
                    StreamReader srGeneral = new StreamReader(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_general.txt");
                    counter = 0;
                    string[] elem2 = new string[100];
                    for (int i = 1; i <= lineCount; i++)
                    {
                        line = srGeneral.ReadLine();
                        counter++;
                        elem2 = line.ToString().Split('\t');
                        if (counter != 1)
                        {
                            if (elem2[0] == "Node")
                            {
                                Nodes.Where(item => item.Index == Int32.Parse(elem2[1])).Single().Font = (Font)fontConverter.ConvertFromString(elem2[2]);
                            }
                            else if (elem2[0] == "Pipe")
                            {
                                Pipes.Where(item => item.Index == Int32.Parse(elem2[1])).Single().Font = (Font)fontConverter.ConvertFromString(elem2[2]);
                            }
                        }
                    }
                    srGeneral.Close();
                    srGeneral.DiscardBufferedData();
                    srGeneral.Dispose();
                }


                foreach(Pipe p in Pipes)
                {
                    if(Int32.Parse(p.properties[4]) == 2)
                    {
                        p.ObjectType = Pipe.ObjectTypes.Type2;
                    }
                    else if (Int32.Parse(p.properties[4]) == 3)
                    {
                        p.ObjectType = Pipe.ObjectTypes.Type3;
                    }
                    else if (Int32.Parse(p.properties[4]) == 4)
                    {
                        p.ObjectType = Pipe.ObjectTypes.Type4;
                    }
                    else if (Int32.Parse(p.properties[4]) == 1)
                    {
                        p.ObjectType = Pipe.ObjectTypes.Type1;
                    }

                }
                if (!File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_nodes.txt") || !File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_pipes.txt"))
                {
                    foreach (Node n in Nodes)
                    {
                        n.Data.Properties.Add("Name", n.Name + ";-10;-17;1");
                    }
                }
                //try
                //{
                if (File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_nodes.txt") && File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_nodes.txt"))
                {
                    /* properties */
                    string line2;
                    lineCount = File.ReadAllLines(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_nodes.txt").Length;
                    StreamReader sr2 = new StreamReader(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_nodes.txt");
                    counter = 0;
                    int field = 0;
                    string[] properties = new string[100];
                    string[] elem2 = new string[100];
                    for (int i = 1; i <= lineCount; i++)
                    {
                        counter++;

                        line2 = sr2.ReadLine();
                        
                       
                        if (counter != 1)
                        {
                            elem2 = line2.ToString().Split('\t');
                            for (int j = 1; j < elem2.Count(); j++)
                            {
                                Nodes.Where(item => item.Index == Int32.Parse(elem2[0])).Single().Data.Properties.Add(elem2[j].Split(':').First(), elem2[j].Split(':').ElementAt(1));
                            }
                        }
                    }
                    sr2.Close();
                    sr2.DiscardBufferedData();
                    sr2.Dispose();
                }
                if (File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_nodes.txt") && File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_pipes.txt"))
                {
                    string line3 = "";
                    lineCount = File.ReadAllLines(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_pipes.txt").Length;
                    StreamReader sr3 = new StreamReader(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_pipes.txt");
                    int field = 0;
                    counter = 0;
                    field = 0;
                    string[] properties3 = new string[100];
                    string[] elem3 = new string[100];

                    for (int i = 1; i <= lineCount; i++)
                    {
                        line3 = sr3.ReadLine();
                        counter++;
                        if (counter != 1)
                        {
                            elem3 = line3.ToString().Split('\t');
                            for (int j = 1; j < elem3.Count(); j++)
                            {
                                Pipes.Where(item => item.Index == Int32.Parse(elem3[0])).Single().Data.Properties.Add(elem3[j].Split(':').First(), elem3[j].Split(':').ElementAt(1));
                            }

                        }
                    }
                    sr3.Close();
                }

                if (File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_nodes.txt") && File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_globalproperties.txt"))
                {
                    string line3 = "";
                    lineCount = File.ReadAllLines(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_globalproperties.txt").Length;
                    StreamReader sr3 = new StreamReader(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_globalproperties.txt");
                   
                    counter = 0;

                    string[] properties = new string[100];
                    string[] elem2 = new string[100];

                    for (int i = 1; i <= lineCount; i++)
                    {
                        line3 = sr3.ReadLine();
                        counter++;
                        if (counter != 1)
                        {
                            elem2 = line3.ToString().Split('\t');
                            if(elem2[0] == "Node")
                            {
                                NodeGlobalProperties.Add(elem2[1].ToString().Split(';').ElementAt(0), elem2[1].ToString().Split(':').ElementAt(1));
                            }
                            if (elem2[0] == "Pipe")
                            {
                                PipeGlobalProperties.Add(elem2[1].ToString().Split(';').ElementAt(0), elem2[1].ToString().Split(':').ElementAt(1));
                            }

                        }
                    }
                    sr3.Close();
                    sr3.Dispose();
                }

                if (File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_nodes.txt") && File.Exists(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_labels.txt"))
                {
                    string line3 = "";
                    lineCount = File.ReadAllLines(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_labels.txt").Length;
                    StreamReader srLabels = new StreamReader(openFileDialog1.FileName.ToString().Replace(".txt", "_map") + "\\" + openFileDialog1.SafeFileName.ToString().Replace(".txt", "") + "_labels.txt");

                    counter = 0;
                    string[] elem2 = new string[100];

                    for (int i = 1; i <= lineCount; i++)
                    {
                        line3 = srLabels.ReadLine();
                        counter++;
                        if (counter != 1)
                        { 
                            // sw7.WriteLine("ID\tX_POS\tY_POS\tText\tStatic\tNodeAssignedIndex\tPipeAssignedIndex\tAssignmentStartPosition\tMarkedLabel\tFont");



                            elem = line3.ToString().Split('\t');
                            label lbl = new label(Int32.Parse(elem[0]), elem[3], float.Parse(elem[1]), float.Parse(elem[2]));
                            if (elem[4].ToLower() == "false") lbl.Static = false;
                            else lbl.Static = true;
                            lbl.nodeAssignedIndex = Int32.Parse(elem[5]);
                            lbl.pipeAssignedIndex = Int32.Parse(elem[6]);
                            lbl.assignmentStartPosition = Int32.Parse(elem[7]);
                            if (elem[8].ToLower() == "false") lbl.MarkedLabel = false;
                            else lbl.MarkedLabel = true;
                            if (elem[9].ToLower() == "null") lbl.font = null;
                            else lbl.font = (Font)fontConverter.ConvertFromString(elem[9]);
                            Labels.Add(lbl);

                        }
                    }
                    srLabels.Close();
                    srLabels.Dispose();
                }


                foreach (Node n in Nodes)
                {
                    if (w_min > n.Vector.X) w_min = n.Vector.X;
                    if (h_min > n.Vector.Y) h_min = n.Vector.Y;

                }
                foreach (Node n in Nodes)
                {
                    n.Vector = new Vector2(n.Vector.X - w_min, n.Vector.Y - h_min);
                    n.auxVector = new Vector2(n.auxVector.X - w_min, n.auxVector.Y - h_min);
                }
                foreach (Node n in Nodes)
                {
                    if (n.Vector.X > w_max) w_max = n.Vector.X;
                    if (n.Vector.Y > h_max) h_max = n.Vector.Y;
                }
                foreach (Node n in Nodes)
                {
                    n.Index = Nodes.IndexOf(n);
                    n.Vector = new Vector2(n.Vector.X * ((GLControl.Size.Width - 50) / w_max) + 25, GLControl.Size.Height - (n.Vector.Y * ((GLControl.Size.Height - 50) / h_max)) - 25);
                    n.auxVector = new Vector2(n.auxVector.X * ((GLControl.Size.Width - 50) / w_max) + 25, GLControl.Size.Height - (n.auxVector.Y * ((GLControl.Size.Height - 50) / h_max)) - 25);
                }
                }
            foreach (Pipe p1 in Pipes)
            {
                bool ok1 = true, ok2 = true;
                foreach (Pipe p2 in Pipes)
                {
                    if (p1.Node1 == p2.Node2)
                    {
                        ok1 = false;
                    }
                    if (p1.Node2 == p2.Node1)
                    {
                        ok2 = false;
                    }
                }
                if (ok1 == true)
                {
                    p1.Node1.Data.currentEnterExit = AdditionalNodeData.EnterExitType.Enter;
                }
                if (ok2 == true)
                {
                    p1.Node2.Data.currentEnterExit = AdditionalNodeData.EnterExitType.Exit;
                }
            }

            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void save()
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(save.FileName);
                string line;
                string[] line_f = new string[100];
                line = "Network_id	Start date	End date	Object_id	Type	Name	Index_Node1	Index_Node2	Node1	Node2	Length	Diameter	Roughness	Pimin	Pomax	CL	xi	yi	Hi	xo	yo	Ho	Int_no_of_nodes	x1	H1	x2	H2	Valve Condition	Suply_offtke	CondLimit_1	CondLimit_2	DrawP1_x	DrawP1_y	DrawP2_x	DrawP2_y	DrawP3_x	DrawP3_y	DrawP4_x	DrawP4_y	Proc_p1	Proc_p2	Proc_lpg	Proc_lpcd	init_node	String	Init_Scen_tab	Chart_tab	Noduri_NU	Pipe_mode	Picture" + '\t' + "SubNet_id" + '\t' + "Curved	CurvePoint_X	CurvePoint_Y";
                sw.WriteLine(line);
                line_f = line.Split('\t');
                foreach (Pipe p in Pipes)
                {
                    line = "";
                    for (int i = 0; i <= 2; i++)
                    {
                        bool ok = false;
                        foreach (object key in Pipes[0].Data.Properties.Keys)
                        {
                            if (key.ToString() == line_f[i])
                            {
                                string[] content = new string[100];
                                content = p.Data.Properties[key].ToString().Split(';');
                                ok = true;
                                line += content[0] + '\t';
                            }
                        }
                        if (ok == false)
                        {
                            line += p.properties[i] + '\t';
                        }
                    }

                    line += p.Index.ToString() + '\t';
                    if (p.ObjectType == Pipe.ObjectTypes.Type1)
                    {
                        line += "1" + '\t';
                    }
                    else if (p.ObjectType == Pipe.ObjectTypes.Type2)
                    {
                        line += "2" + '\t';
                    }
                    else if (p.ObjectType == Pipe.ObjectTypes.Type3)
                    {
                        line += "3" + '\t';
                    }
                    else if (p.ObjectType == Pipe.ObjectTypes.Type4)
                    {
                        line += "4" + '\t';
                    }

                    line += p.properties[5] + '\t';
                    line += p.Node1.Index.ToString() + '\t' + p.Node2.Index.ToString() + '\t';
                    {
                        line += p.Node1.Name.ToString() + '\t' + p.Node2.Name.ToString() + '\t';
                    }
                    for (int i = 10; i <= 15; i++)
                    {
                        bool ok = false;
                        foreach (object key in Pipes[0].Data.Properties.Keys)
                        {
                            if (key.ToString() == line_f[i])
                            {
                                string[] content = new string[100];
                                content = p.Data.Properties[key].ToString().Split(';');
                                ok = true;
                                line += content[0] + '\t';
                            }
                        }
                        if (ok == false)
                        {
                            line += p.properties[i] + '\t';
                        }
                    }
                    //line += p.properties[i] + '\t';
                    line += p.Node1.Vector.X.ToString() + '\t' + (this.Size.Height - p.Node1.Vector.Y).ToString() + '\t' + p.properties[18] + '\t' + p.Node2.Vector.X.ToString() + '\t' + (this.Size.Height - p.Node2.Vector.Y).ToString() + '\t';
                    for (int i = 21; i <= 50; i++)
                    {
                        bool ok = false;
                        foreach (object key in Pipes[0].Data.Properties.Keys)
                        {
                            if (key.ToString() == line_f[i])
                            {
                                string[] content = new string[100];
                                content = p.Data.Properties[key].ToString().Split(';');
                                ok = true;
                                line += content[0] + '\t';
                            }
                        }
                        if (ok == false)
                        {
                            line += p.properties[i] + '\t';
                        }
                    }
                    //line += p.properties[i] + '\t';
                    if (p.PipeType == Pipe.PipeTypes.Line)
                        line += "0\t-\t-";
                    else
                        line += "1\t" + p.auxControlPoint.X.ToString() + '\t' + p.auxControlPoint.Y.ToString() + '\t';
                    sw.WriteLine(line);
                }
                sw.Close();
                sw.Dispose();


                if (!Directory.Exists(save.FileName.ToString().Replace(Path.GetExtension(save.FileName).ToString(), "") + "_map\\"))
                {
                    Directory.CreateDirectory(save.FileName.ToString().Replace(Path.GetExtension(save.FileName).ToString(), "") + "_map\\");
                }
                StreamWriter sw2 = new StreamWriter(save.FileName.ToString().Replace(Path.GetExtension(save.FileName).ToString(), "") + "_map\\" + Path.GetFileNameWithoutExtension(save.FileName).ToString() + "_nodes.txt");
                string line2 = "ID\tProperties";
                
                sw2.WriteLine(line2);
                foreach (Node n in Nodes)
                {
                    line2 = n.Index.ToString();
                    foreach (object key in n.Data.Properties.Keys)
                    {
                        line2 += "\t" + key + ":" + n.Data.Properties[key];
                    }
                    sw2.WriteLine(line2);
                }
                sw2.Close();
                sw2.Dispose();

                StreamWriter sw3 = new StreamWriter(save.FileName.ToString().Replace(Path.GetExtension(save.FileName).ToString(), "") + "_map\\" + Path.GetFileNameWithoutExtension(save.FileName).ToString() + "_pipes.txt");
                string line3 = "ID\tProperties";

                sw3.WriteLine(line3);
                foreach (Pipe p in Pipes)
                {
                    line3 = p.Index.ToString();
                    foreach (object key in p.Data.Properties.Keys)
                    {
                        line3 += "\t" + key + ":" + p.Data.Properties[key];
                    }
                    sw3.WriteLine(line3);
                }
                sw3.Close();
                sw3.Dispose();


                StreamWriter sw4 = new StreamWriter(save.FileName.ToString().Replace(Path.GetExtension(save.FileName).ToString(), "") + "_map\\" + Path.GetFileNameWithoutExtension(save.FileName).ToString() + "_subnetworks.txt");
                string line4 = "Object_Type\tObject_ID\tSubnetwork";
                sw4.WriteLine(line4);
                foreach (Node n in Nodes)
                {
                    if(n.Subnetwork != "")
                    sw4.WriteLine("Node\t" + n.Index.ToString() + "\t" + n.Subnetwork);
                    //else
                    //sw4.WriteLine("Node\t" + n.Index.ToString() + "\t" + "empty");
                }
                foreach (Pipe p in Pipes)
                {
                    if(p.Subnetwork != "")
                    sw4.WriteLine("Pipe\t" + p.Index.ToString() + "\t" + p.Subnetwork);
                   // else
                   // sw4.WriteLine("Pipe\t" + p.Index.ToString() + "\t" + "empty");
                }
                sw4.Close();
                sw4.Dispose();

                StreamWriter sw5 = new StreamWriter(save.FileName.ToString().Replace(Path.GetExtension(save.FileName).ToString(), "") + "_map\\" + Path.GetFileNameWithoutExtension(save.FileName).ToString() + "_general.txt");
                sw5.WriteLine("Object_Type\tObject_ID\tFont");
                foreach (Node n in Nodes.Where(item => item.Font != null))
                {
                    sw5.WriteLine("Node\t" + n.Index.ToString() + "\t" + fontConverter.ConvertToString(n.Font));
                }
                foreach (Pipe p in Pipes.Where(item => item.Font != null))
                {
                    sw5.WriteLine("Pipe\t" + p.Index.ToString() + "\t" + fontConverter.ConvertToString(p.Font));
                }
                sw5.Close();
                sw5.Dispose();

                StreamWriter sw6 = new StreamWriter(save.FileName.ToString().Replace(Path.GetExtension(save.FileName).ToString(), "") + "_map\\" + Path.GetFileNameWithoutExtension(save.FileName).ToString() + "_globalproperties.txt");
                sw6.WriteLine("Object_Type\tProperty");
                foreach (object key in NodeGlobalProperties.Keys)
                {
                    sw6.WriteLine("Node\t" + key + ":" + NodeGlobalProperties[key]);
                }
                    foreach(object key in PipeGlobalProperties.Keys)
                {
                    sw6.WriteLine("Pipe\t" + key + ":" + NodeGlobalProperties[key]);
                }
                sw6.Close();
                sw6.Dispose();

                continousLastNodeIndex = -1;


                // LABELS
                StreamWriter sw7 = new StreamWriter(save.FileName.ToString().Replace(Path.GetExtension(save.FileName).ToString(), "") + "_map\\" + Path.GetFileNameWithoutExtension(save.FileName).ToString() + "_labels.txt");
        
                sw7.WriteLine("ID\tX_POS\tY_POS\tText\tStatic\tNodeAssignedIndex\tPipeAssignedIndex\tAssignmentStartPosition\tMarkedLabel\tFont");

                string tempFont;

                foreach (label lbl in Labels)
                {
                    if (lbl.font == null) tempFont = "null";
                    else tempFont = fontConverter.ConvertToString(lbl.font);
                    line = lbl.Index.ToString() + "\t" + lbl.X.ToString() + "\t" + lbl.Y.ToString();
                    line += "\t" + lbl.Text + "\t" + lbl.Static.ToString().ToLower() + "\t" + lbl.nodeAssignedIndex.ToString() + "\t" + lbl.pipeAssignedIndex.ToString() + "\t" + lbl.assignmentStartPosition.ToString() + "\t" + lbl.MarkedLabel.ToString().ToLower() + "\t" + tempFont;
                    sw7.WriteLine(line);
                }
                sw7.Close();
                sw7.Dispose();

                GLControl.Invalidate();
            }

        }
        public void GLControlInvalidate()
        {
            GL.ClearColor(Properties.Settings.Default.BackgroundColor);
            GLControl.Invalidate();
        }
        private void snapTo(Node n, Pipe p)
        {
            if(n != null && p == null)
            {

            }
            else if(n == null && p != null)
            {

            }
        }
        public Bitmap GrabScreenshot()
        {
            if (OpenTK.Graphics.GraphicsContext.CurrentContext == null)
                throw new OpenTK.Graphics.GraphicsContextMissingException();

            Bitmap bmp = new Bitmap(GLControl.ClientSize.Width, GLControl.ClientSize.Height);
            BitmapData data = bmp.LockBits(GLControl.ClientRectangle, System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            GL.ReadPixels(0, 0, GLControl.ClientSize.Width, GLControl.ClientSize.Height, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
            Graphics g = Graphics.FromImage(bmp);
            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Bitmap toReturn = new Bitmap(GLControl.ClientSize.Width*2, GLControl.ClientSize.Height*2);
            g.DrawImage(toReturn, GLControl.ClientSize.Width * 2, GLControl.ClientSize.Height * 2);
            bmp.UnlockBits(data);
            bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
            return bmp;
        }  
        private void drawGrid()
        {
             if(drawGridBool)
             {
                 int width = this.GLControl.Width;
                 int height = this.GLControl.Height;
                 GL.Color3(Color.Gray);
                 GL.ShadeModel(ShadingModel.Smooth);
                 GL.Enable(EnableCap.LineSmooth);
                 GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                 GL.Hint(HintTarget.LineSmoothHint, HintMode.DontCare);
                 GL.LineWidth(0.01f);
                 GL.Begin(PrimitiveType.Lines);
                 foreach (gridLine g in Grid)
                 {
                     /*
                       textPrinter.Begin();
                        textPrinter.Print(text, tempFont, Color.Black, new RectangleF((p.Node1.Vector.X + p.Node2.Vector.X) / 2 + float.Parse(p.Data.Properties[key].ToString().Split(';')[1]), (p.Node1.Vector.Y + p.Node2.Vector.Y) / 2 + float.Parse(p.Data.Properties[key].ToString().Split(';')[2]), textPrinter.Measure(text, tempFont).BoundingBox.Width * 2, textPrinter.Measure(text, tempFont).BoundingBox.Height * 2));
                                textPrinter.End();
                      */

                     if(g.evidentiat)
                     {
                         GL.Color3(Color.Red);
                     }
                     else
                     {
                         GL.Color3(Color.Gray);
                     }
                     GL.Vertex2(new Vector2(g.X1, g.Y1));
                     GL.Vertex2(new Vector2(g.X2, g.Y2));
                }
                GL.End();
           }
           //  else
           //  {
           //      resetGrid();
           //  }
        }
        public void refreshGrid()
        {
            bool need = true;

            while (need == true)
            {

                gridHeight = Math.Abs(Grid[Grid.Where(item => item.Type == gridLine._Type.Horizontal && item.Index + 1 != hMinID && item.Index + 1 != hMaxID && item.Index + 1 < Grid.Count(itemC => itemC.Type == gridLine._Type.Horizontal) && item.Index != hMinID && item.Index != hMaxID && item.Index < Grid.Count(itemC => itemC.Type == gridLine._Type.Horizontal)).OrderByDescending(itemO => itemO.Y1).First().Index + 1].Y1 - Grid[Grid.Where(item => item.Type == gridLine._Type.Horizontal && item.Index + 1 != hMinID && item.Index + 1 != hMaxID && item.Index + 1 < Grid.Count(itemC => itemC.Type == gridLine._Type.Horizontal) && item.Index != hMinID && item.Index != hMaxID && item.Index < Grid.Count(itemC => itemC.Type == gridLine._Type.Horizontal)).OrderByDescending(itemO => itemO.Y1).First().Index].Y1);
                gridWidth = gridHeight;

                if (gridWidth > GLControl.Width)
                {
                    foreach (gridLine g in Grid)
                    {
                        g.maxX1 = g.X1;
                        g.maxX2 = g.X2;
                        g.maxY1 = g.Y1;
                        g.maxY2 = g.Y2;
                    }
                    resetGrid(true);

                    return;
                }
                else if (gridWidth < 9.6)
                {
                    resetGrid(false);

                    return;
                }


                cateDeasupra = Grid.Where(item => item.Type == gridLine._Type.Horizontal && item.Y1 < GLControl.Top).Count();
                cateDedesupt = Grid.Where(item => item.Type == gridLine._Type.Horizontal && item.Y1 > GLControl.Bottom).Count();
                hMinID = Grid.Where(item => item.Type == gridLine._Type.Horizontal).OrderByDescending(line => line.Y1).Last().Index;
                hMaxID = Grid.Where(item => item.Type == gridLine._Type.Horizontal).OrderByDescending(line => line.Y1).First().Index;
                cateStanga = Grid.Where(item => item.Type == gridLine._Type.Vertical && item.X1 < GLControl.Left).Count();
                cateDreapta = Grid.Where(item => item.Type == gridLine._Type.Vertical && item.X1 > GLControl.Right).Count();
                vMinID = Grid.Where(item => item.Type == gridLine._Type.Vertical).OrderByDescending(line => line.X1).Last().Index;
                vMaxID = Grid.Where(item => item.Type == gridLine._Type.Vertical).OrderByDescending(line => line.X1).First().Index;


                if (cateDeasupra < 2 || cateDedesupt < 2 || cateStanga < 2 || cateDreapta < 2)
                {
                    need = true;
                }
                else
                {
                    need = false;
                    return;
                }

                if (cateDedesupt < 2)
                {
                    Grid.Where(item => item.Index == hMinID).Single().Y1 = Grid.Where(item => item.Index == hMaxID).Single().Y1 + gridHeight;
                    Grid.Where(item => item.Index == hMinID).Single().Y2 = Grid.Where(item => item.Index == hMaxID).Single().Y2 + gridHeight;
                }
                if (cateDeasupra < 2)
                {
                    Grid.Where(item => item.Index == hMaxID).Single().Y1 = Grid.Where(item => item.Index == hMinID).Single().Y1 - gridHeight;
                    Grid.Where(item => item.Index == hMaxID).Single().Y2 = Grid.Where(item => item.Index == hMinID).Single().Y2 - gridHeight;
                }

                if (cateDreapta < 2)
                {
                    //luam din stanga punem in dreapta
                    Grid.Where(item => item.Index == vMinID).Single().X1 = Grid.Where(item => item.Index == vMaxID).Single().X1 + gridWidth;
                    Grid.Where(item => item.Index == vMinID).Single().X2 = Grid.Where(item => item.Index == vMaxID).Single().X2 + gridWidth;
                }
                if (cateStanga < 2)
                {
                    //luam din dreapta punem in stanga
                    Grid.Where(item => item.Index == vMaxID).Single().X1 = Grid.Where(item => item.Index == vMinID).Single().X1 - gridWidth;
                    Grid.Where(item => item.Index == vMaxID).Single().X2 = Grid.Where(item => item.Index == vMinID).Single().X2 - gridWidth;
                }

            }

            foreach (gridLine g in Grid.Where(item => item.Type == gridLine._Type.Vertical))
            {
                g.auxX1 = g.X1;
                g.auxX2 = g.X2;
            }
            foreach (gridLine g in Grid.Where(item => item.Type == gridLine._Type.Horizontal))
            {
                g.auxY1 = g.Y1;
                g.auxY2 = g.Y2;
            }

          
         
         
        }
        public void resetGrid(bool zoomIn)
        {


            //oldZoom = zoomLevel;
            if(zoomIn == true)
            {
                //zoomLevel = originalZoomLevel;
                foreach (gridLine g in Grid)
                {
                    g.X1 = g.defaultX1;
                    g.X2 = g.defaultX2;
                    g.Y1 = g.defaultY1;
                    g.Y2 = g.defaultY2;
                    g.auxX1 = g.X1;
                    g.auxX2 = g.X2;
                    g.auxY1 = g.Y1;
                    g.auxY2 = g.Y2;
                }
            }
            else
            {
                foreach (gridLine g in Grid)
                {
                    g.X1 = g.maxX1;
                    g.X2 = g.maxX2;
                    g.Y1 = g.maxY1;
                    g.Y2 = g.maxY2;
                    g.auxX1 = g.X1;
                    g.auxX2 = g.X2;
                    g.auxY1 = g.Y1;
                    g.auxY2 = g.Y2;
                }
                //zoomLevel = oldZoom;
            }



            resizeGrid();
            GLControl.Invalidate();
        }
        private void calculateMaxGrid()
        {
            while (Grid.Where(item => item.Type == gridLine._Type.Horizontal).First().maxY1 == Grid.Where(item => item.Type == gridLine._Type.Horizontal).First().defaultY1)
            {
                zoom(mea);
            }
            for(int i=0;i<=35;i++)
            {
                zoom(mea);
            }
            showNotification("Information", "To cancel the \"Free lines\" mode press Escape button.", SystemIcons.Information.Handle, MessageBoxButtons.OK, false);

        }
        private void resizeGrid()
        {
            foreach (gridLine g in Grid.Where(item => item.Type == gridLine._Type.Vertical))
            {
                g.Y2 = GLControl.Height;
            }
            foreach (gridLine g in Grid.Where(item => item.Type == gridLine._Type.Horizontal))
            {
                g.X2 = GLControl.Width;
            }
        }
        #endregion

        #region Form event handlers


        private void labelTempTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if(e.Shift == false)
                {
                    Labels.Add(new label(newLabelIndex(), labelTempTextBox.Text, labelTempTextBox.Location.X, labelTempTextBox.Location.Y));
                    labelTempTextBox.Visible = false;
                    GLControl.Invalidate();
                    GLControl.Focus();
                }
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();

            GL.Ortho(GLControl.Bounds.Left, GLControl.Bounds.Right, GLControl.Bounds.Bottom, GLControl.Bounds.Top, -1.0, 1.0);
            GL.Viewport(this.GLControl.Size);

            continousLastNodeIndex = -1;
            GLControl.Invalidate();
            resizeGrid();
            notification.Location = new Point(this.Location.X + this.Width - notification.Width - 15, this.Location.Y + this.Height - notification.Height - 40);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
           if(e.Control == false && labelTempTextBox.Focused == false)
           {
               if (e.KeyCode == Keys.N)
               {
                   toolStripButton1.PerformClick();
               }
               if (e.KeyCode == Keys.L)
               {
                   toolStripButton2.PerformClick();
               }
               if (e.KeyCode == Keys.C)
               {
                   toolStripButton6.PerformClick();
               }
               if (e.KeyCode == Keys.M)
               {
                   toolStripButton4.PerformClick();
               }
               if (e.KeyCode == Keys.S)
               {
                   toolStripButton3.PerformClick();
               }
               if (e.KeyCode == Keys.P)
               {
                   toolStripButton5.PerformClick();
               }
               if (e.KeyCode == Keys.F)
               {
                   toolStripButton7.PerformClick();
               }
           }
            if(currentAction == Actions.continousLines)
            {
                if(e.KeyCode == Keys.Escape)
                {
                    continousLastNodeIndex = -1;
                    GLControl.Invalidate();
                }
            }

            if (e.KeyCode == Keys.Delete)
            {
                if (selectedNodes.Count > 0)
                {
                    foreach (Node n in selectedNodes)
                    {
                        int[] removePipeIndexes = new int[2147483591 / Marshal.SizeOf(typeof(int))]; int i = 0;
                        foreach (Pipe p in Pipes.Where(item => item.Node1 == n))
                        {
                            removePipeIndexes[i] = p.Index;
                            i++;
                        }
                        foreach (Pipe p in Pipes.Where(item => item.Node2 == n))
                        {
                            removePipeIndexes[i] = p.Index;
                            i++;
                        }
                        if (i > 0)
                        {
                            for (int j = 0; j < i; j++)
                            {
                                try
                                {
                                    Pipes.Remove(Pipes.Where(item => item.Index == removePipeIndexes[j]).Single());
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                        Nodes.Remove(n);
                        removePipeIndexes = null;
                    }
                    selectedNodes.Clear();
                    selectedPipes.Clear();
                }

                GLControl.Invalidate();
            }

            if (e.Control && e.KeyCode == Keys.X)
            {
                //Thread t = new Thread(cut);
                //t.SetApartmentState(System.Threading.ApartmentState.STA);
                //t.Start();
                //t.Join();
                cut();


            }
            if (e.Control && e.KeyCode == Keys.V)
            {
                //Thread t = new Thread(paste);
                //t.SetApartmentState(System.Threading.ApartmentState.STA);
                //t.Start();
                //t.Join();
                paste();
            }
            if (e.Control && e.KeyCode == Keys.C)
            {
                //Thread t = new Thread(copy);
                //t.SetApartmentState(System.Threading.ApartmentState.STA);
                //t.Start();
                //t.Join();
                copy();
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            calculateMaxGrid();
        }


        private int getNewNode(Node n)
        {
            int index = 0;
            foreach (Node n1 in moveNodes)
            {
                if (n.Vector == n1.Vector)
                {
                    index = moveNodes.IndexOf(n1);
                }
            }
            return index;
        }
        private void copy()
        {
            int index = Nodes.Count;
            int index_pipes = Pipes.Count;
            moveNodes.Clear();
            movePipes.Clear();
            foreach (Node n in selectedNodes)
            {
                index++;
                Node n1 = new Node(n.Vector, newNodeIndex());
                foreach (object key in n.Data.Properties.Keys)
                {
                    n1.Data.Properties.Add(key,n.Data.Properties[key]);
                }
                moveNodes.Add(n1);
            }
            foreach (Pipe p in selectedPipes)
            {
                index_pipes++;
                int index1 = getNewNode(p.Node1), index2 = getNewNode(p.Node2);
                Pipe p1 = new Pipe(newPipeIndex(), moveNodes[getNewNode(p.Node1)], moveNodes[getNewNode(p.Node2)]);
                foreach (object key in p.Data.Properties.Keys)
                {
                    p1.Data.Properties.Add(key, p.Data.Properties[key]);
                }
                movePipes.Add(p1);
            }
            //foreach (Node n in moveNodes)
            //{
            //    n.Index += index;
            //}
            //foreach (Pipe p in movePipes)
            //{
            //    p.Node1.Index += index;
            //    p.Node2.Index += index;
            //    p.auxNode1.Index += index;
            //    
            //}
            selectedNodes.Clear();
            selectedPipes.Clear();
            GLControl.Invalidate();
        }
        private void paste()
        {
            selectedNodes.Clear();
            selectedPipes.Clear();
            foreach (Node n in Nodes)
            {
                n.PointSize = 9f;
            }
            foreach (Pipe p in Pipes)
            {
                p.Width = 1f;
            }
            int index = Nodes.Count;
            int index_pipes = Pipes.Count();
            foreach (Node n in moveNodes)
            {
                //Node n1 = new Node(new Vector2(n.Vector.X + 100, n.Vector.Y + 100), newNodeIndex());
                //Nodes.Add(n1);
                //selectedNodes.Add(n1);
                n.Vector = new Vector2(n.Vector.X + 20, n.Vector.Y + 20);
                n.auxVector = new Vector2(n.auxVector.X + 20, n.auxVector.Y + 20);
                n.Index = newNodeIndex();
                Nodes.Add(n);
                selectedNodes.Add(n);

            }
            foreach (Pipe p in movePipes)
            {
                //p.Node1.Vector = new Vector2(p.Node1.Vector.X + 100, p.Node1.Vector.Y + 100);
                //p.Node2.Vector = new Vector2(p.Node2.Vector.X + 100, p.Node2.Vector.Y + 100);
                p.Node1.auxVector = p.Node1.Vector;
                p.Node2.auxVector = p.Node2.Vector;
                p.Index = newPipeIndex();
                Pipes.Add(p);
                selectedPipes.Add(p);

            }
            moveNodes.Clear();
            movePipes.Clear();
            foreach (Node n in selectedNodes)
            {
                index++;
                n.PointSize = 12f;
                Node n1 = new Node(n.Vector, newNodeIndex());
                foreach (object key in n.Data.Properties.Keys)
                {
                    n1.Data.Properties.Add(key, n.Data.Properties[key]);
                }
                moveNodes.Add(n1);
            }
            foreach (Pipe p in selectedPipes)
            {
                index_pipes++;
                p.Width = 3f;
                Pipe p1 = new Pipe(newPipeIndex(), moveNodes[getNewNode(p.Node1)], moveNodes[getNewNode(p.Node2)]);
                foreach (object key in p.Data.Properties.Keys)
                {
                    p1.Data.Properties.Add(key, p.Data.Properties[key]);
                }
                movePipes.Add(p1);
            }
            toolStripButton4.PerformClick();



            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        private void cut()
        {
            moveNodes.Clear();
            movePipes.Clear();
            foreach (Node n in selectedNodes)
                moveNodes.Add(n);
            foreach (Pipe p in selectedPipes)
                movePipes.Add(p);
            List<Pipe> aux_Pipes = new List<Pipe>();

            foreach (Node n in selectedNodes)
            {
                foreach (Pipe p in Pipes.Where(item => item.Node1 == n || item.Node2 == n))
                    aux_Pipes.Add(p);
                foreach (Pipe p in aux_Pipes)
                    Pipes.Remove(p);
                aux_Pipes.Clear();
                Nodes.Remove(n);
            }
            selectedNodes.Clear();
            selectedPipes.Clear();

            continousLastNodeIndex = -1;
            GLControl.Invalidate();
        }
        #endregion

        private void Form1_Move(object sender, EventArgs e)
        {
            notification.Location = new Point(this.Location.X + this.Width - notification.Width - 15, this.Location.Y + this.Height - notification.Height - 40);
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();

            GL.Ortho(GLControl.Bounds.Left, GLControl.Bounds.Right, GLControl.Bounds.Bottom, GLControl.Bounds.Top, -1.0, 1.0);
            GL.Viewport(this.GLControl.Size);

            continousLastNodeIndex = -1;
            GLControl.Invalidate();
            resizeGrid();
            
        }

        private void splitContainer1_SplitterMoving(object sender, SplitterCancelEventArgs e)
        {
            splitContainer1.Invalidate();
            GLControl.Invalidate();
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }


        private void splitContainer2_SplitterMoving(object sender, SplitterCancelEventArgs e)
        {
            splitContainer1.Invalidate();
            GLControl.Invalidate();
        }

        private void splitContainer3_SplitterMoving(object sender, SplitterCancelEventArgs e)
        {
            splitContainer1.Invalidate();
            GLControl.Invalidate();
        }

        private void splitContainer3_SplitterMoved(object sender, SplitterEventArgs e)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();

            GL.Ortho(GLControl.Bounds.Left, GLControl.Bounds.Right, GLControl.Bounds.Bottom, GLControl.Bounds.Top, -1.0, 1.0);
            GL.Viewport(this.GLControl.Size);

            continousLastNodeIndex = -1;
            GLControl.Invalidate();
            resizeGrid();
        }

        private void splitContainer2_SplitterMoved(object sender, SplitterEventArgs e)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();

            GL.Ortho(GLControl.Bounds.Left, GLControl.Bounds.Right, GLControl.Bounds.Bottom, GLControl.Bounds.Top, -1.0, 1.0);
            GL.Viewport(this.GLControl.Size);

            continousLastNodeIndex = -1;
            GLControl.Invalidate();
            resizeGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Layer newLayer = new Layer(newLayerIndex(), "Layer " + newLayerIndex().ToString());
            Layers.Insert(0, newLayer);
            foreach(Layer layer in Layers)
            {
                layer.isSelected = false;
            }
            newLayer.isSelected = true;
            loadLayersList();
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
           if(Layers.Count > 1)
           {
               Layers.Remove(Layers.Where(item => item.Name == Layers.Where(item2 => item2.isSelected == true).Single().Name).Single());
               Layers[0].isSelected = true;
               loadLayersList();
               
           }
        }

    }

    
}

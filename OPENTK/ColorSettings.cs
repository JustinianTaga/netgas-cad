﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OPENTK
{
    public partial class ColorSettings : Form
    {
        Form1 mainWindow;
        public ColorSettings(Form1 _mainWindow)
        {
            InitializeComponent();
            mainWindow = _mainWindow;
        }
        Color OriginalNodeColor;
        Color OriginalPipeColor;
        Color OriginalControlPointColor;
        Color OriginalSelectionColor;
        Color OriginalBackgroundColor;
        Color OriginalTextColor;
        private void ColorSettings_Load(object sender, EventArgs e)
        {
            OriginalNodeColor = Properties.Settings.Default.NodeColor;
            OriginalPipeColor = Properties.Settings.Default.PipeColor;
            OriginalControlPointColor = Properties.Settings.Default.ControlPointColor;
            OriginalSelectionColor = Properties.Settings.Default.SelectionColor;
            OriginalBackgroundColor = Properties.Settings.Default.BackgroundColor;
            OriginalTextColor = Properties.Settings.Default.TextColor;

            panel1.BackColor = Properties.Settings.Default.NodeColor;
            panel2.BackColor = Properties.Settings.Default.PipeColor;
            panel3.BackColor = Properties.Settings.Default.ControlPointColor;
            panel4.BackColor = Properties.Settings.Default.SelectionColor;
            panel5.BackColor = Properties.Settings.Default.BackgroundColor;
            panel6.BackColor = Properties.Settings.Default.TextColor;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            Properties.Settings.Default.NodeColor = OriginalNodeColor;
            Properties.Settings.Default.PipeColor = OriginalPipeColor;
            Properties.Settings.Default.ControlPointColor = OriginalControlPointColor;
            Properties.Settings.Default.SelectionColor = OriginalSelectionColor;
            Properties.Settings.Default.BackgroundColor = OriginalBackgroundColor;
            Properties.Settings.Default.TextColor = OriginalTextColor;
            mainWindow.GLControlInvalidate();
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            if(colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                panel1.BackColor = colorDialog1.Color;
                Properties.Settings.Default.NodeColor = panel1.BackColor;
                Properties.Settings.Default.Save();
                mainWindow.GLControlInvalidate();
            }
        }

        private void panel2_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                panel2.BackColor = colorDialog1.Color;
                Properties.Settings.Default.PipeColor = panel2.BackColor;
                Properties.Settings.Default.Save();
                mainWindow.GLControlInvalidate();
            }
        }

        private void panel3_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                panel3.BackColor = colorDialog1.Color;
                Properties.Settings.Default.ControlPointColor = panel3.BackColor;
                Properties.Settings.Default.Save();
                mainWindow.GLControlInvalidate();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.NodeColor = panel1.BackColor;
            Properties.Settings.Default.PipeColor = panel2.BackColor;
            Properties.Settings.Default.ControlPointColor = panel3.BackColor;
            Properties.Settings.Default.SelectionColor = panel4.BackColor;
            Properties.Settings.Default.BackgroundColor = panel5.BackColor;
            Properties.Settings.Default.Save();
            mainWindow.GLControlInvalidate();
            this.Close();
        }

        private void panel4_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                panel4.BackColor = colorDialog1.Color;
                Properties.Settings.Default.SelectionColor = panel4.BackColor;
                Properties.Settings.Default.Save();
                mainWindow.GLControlInvalidate();
            }
        }

        private void panel5_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                panel5.BackColor = colorDialog1.Color;
                Properties.Settings.Default.BackgroundColor = panel5.BackColor;
                Properties.Settings.Default.Save();
                mainWindow.GLControlInvalidate();
            }
        }

        private void panel6_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                panel6.BackColor = colorDialog1.Color;
                Properties.Settings.Default.TextColor = panel6.BackColor;
                Properties.Settings.Default.Save();
                mainWindow.GLControlInvalidate();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OPENTK
{
    public partial class AddProperty : Form
    {
        objectProperties mainWindow;
        GlobalNetworkSettings GNS;
        public AddProperty(objectProperties _mainWindow, GlobalNetworkSettings _GNS)
        {
            InitializeComponent();
            mainWindow = _mainWindow;
            GNS = _GNS;
            if(GNS != null)
            {
                label3.Visible = true;
                if (GNS.keyToModify != "") textBox1.Text = GNS.keyToModify;
            }
            else if(mainWindow != null)
            {
                label3.Visible = false;
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
         if(mainWindow != null)
         {
             if (mainWindow.selectedNode != null)
             {
                 mainWindow.mainWindow.Nodes.Where(item => item.Index == mainWindow.selectedNode.Index).Single().Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                 mainWindow.reloadNode();
                 mainWindow.mainWindow.GLControlInvalidate();
                 this.Close();
             }
             else if (mainWindow.selectedPipe != null)
             {
                 mainWindow.mainWindow.Pipes.Where(item => item.Index == mainWindow.selectedPipe.Index).Single().Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                 mainWindow.reloadPipe();
                 mainWindow.mainWindow.GLControlInvalidate();
                 this.Close();
             }
         }
         else if(GNS != null)
         {

             #region Add property
             if(GNS.keyToModify == "")
             {
                 if (GNS.comboBox1.SelectedItem.ToString() == "All elements")
                 {
                     foreach (Node n in GNS.mainWindow.Nodes)
                     {
                         if (!n.Data.Properties.ContainsKey(textBox1.Text))
                             n.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                         else n.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";

                     }
                     foreach (Pipe p in GNS.mainWindow.Pipes)
                     {
                         if (!p.Data.Properties.ContainsKey(textBox1.Text))
                             p.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                         else p.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                     }
                     if (!GNS.mainWindow.PipeGlobalProperties.ContainsKey(textBox1.Text))
                         GNS.mainWindow.PipeGlobalProperties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                     else GNS.mainWindow.PipeGlobalProperties[textBox1.Text] = textBox2.Text + ";10;10;1";

                     if (!GNS.mainWindow.NodeGlobalProperties.ContainsKey(textBox1.Text))
                         GNS.mainWindow.NodeGlobalProperties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                     else
                         GNS.mainWindow.NodeGlobalProperties[textBox1.Text] = textBox2.Text + ";10;10;1";


                 }
                 else if (GNS.comboBox1.SelectedItem.ToString() == "Nodes only")
                 {
                     foreach (Node n in GNS.mainWindow.Nodes)
                     {
                         if (!n.Data.Properties.ContainsKey(textBox1.Text))
                             n.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                         else n.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                     }

                     if (!GNS.mainWindow.NodeGlobalProperties.ContainsKey(textBox1.Text))
                         GNS.mainWindow.NodeGlobalProperties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                     else
                         GNS.mainWindow.NodeGlobalProperties[textBox1.Text] = textBox2.Text + ";10;10;1";

                 }
                 else if (GNS.comboBox1.SelectedItem.ToString() == "Pipes only")
                 {
                     foreach (Pipe p in GNS.mainWindow.Pipes)
                     {
                         if (!p.Data.Properties.ContainsKey(textBox1.Text))
                             p.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                         else p.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                     }

                     if (!GNS.mainWindow.PipeGlobalProperties.ContainsKey(textBox1.Text))
                         GNS.mainWindow.PipeGlobalProperties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                     else GNS.mainWindow.PipeGlobalProperties[textBox1.Text] = textBox2.Text + ";10;10;1";
                 }
                 else if (GNS.comboBox1.SelectedIndex == 3)
                 {
                     if (GNS.comboBox2.SelectedIndex == 1)
                     {
                         //node is entering node
                         foreach (Node n in GNS.mainWindow.Nodes.Where(item => item.Data.currentEnterExit == AdditionalNodeData.EnterExitType.Enter))
                         {
                             if (!n.Data.Properties.ContainsKey(textBox1.Text))
                                 n.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else n.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }
                     }
                     else if (GNS.comboBox2.SelectedIndex == 2)
                     {
                         //node is exiting node
                         foreach (Node n in GNS.mainWindow.Nodes.Where(item => item.Data.currentEnterExit == AdditionalNodeData.EnterExitType.Exit))
                         {
                             if (!n.Data.Properties.ContainsKey(textBox1.Text))
                                 n.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else n.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }
                     }
                     else if (GNS.comboBox2.SelectedIndex == 2)
                     {
                         //node is exiting node
                         foreach (Node n in GNS.mainWindow.Nodes.Where(item => item.Data.currentEnterExit == AdditionalNodeData.EnterExitType.None))
                         {
                             if (!n.Data.Properties.ContainsKey(textBox1.Text))
                                 n.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else n.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }
                     }
                 }
                 else if (GNS.comboBox1.SelectedIndex == 4)
                 {
                     if (GNS.comboBox2.SelectedIndex == 0)
                     {
                         //pipes where no attached object
                         foreach (Pipe p in GNS.mainWindow.Pipes.Where(item => item.ObjectType == Pipe.ObjectTypes.Type1))
                         {
                             if (!p.Data.Properties.ContainsKey(textBox1.Text))
                                 p.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else p.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }
                     }
                     else if (GNS.comboBox2.SelectedIndex == 1)
                     {
                         foreach (Pipe p in GNS.mainWindow.Pipes.Where(item => item.ObjectType == Pipe.ObjectTypes.Type2))
                         {
                             if (!p.Data.Properties.ContainsKey(textBox1.Text))
                                 p.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else p.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }

                     }
                     else if (GNS.comboBox2.SelectedIndex == 2)
                     {
                         foreach (Pipe p in GNS.mainWindow.Pipes.Where(item => item.ObjectType == Pipe.ObjectTypes.Type3))
                         {
                             if (!p.Data.Properties.ContainsKey(textBox1.Text))
                                 p.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else p.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }


                     }
                     else if (GNS.comboBox2.SelectedIndex == 3)
                     {
                         foreach (Pipe p in GNS.mainWindow.Pipes.Where(item => item.ObjectType == Pipe.ObjectTypes.Type4))
                         {
                             if (!p.Data.Properties.ContainsKey(textBox1.Text))
                                 p.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else p.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }

                     }
                 }
             }
             #endregion
             #region Modify property
             if(GNS.keyToModify != "")
             {
                 if (GNS.comboBox1.SelectedItem.ToString() == "Nodes only")
                 {
                     foreach (Node n in GNS.mainWindow.Nodes)
                     {
                         if (!n.Data.Properties.ContainsKey(textBox1.Text))
                             n.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                         else n.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                     }

                     if (!GNS.mainWindow.NodeGlobalProperties.ContainsKey(textBox1.Text))
                         GNS.mainWindow.NodeGlobalProperties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                     else
                         GNS.mainWindow.NodeGlobalProperties[textBox1.Text] = textBox2.Text + ";10;10;1";

                 }
                 else if (GNS.comboBox1.SelectedItem.ToString() == "Pipes only")
                 {
                     foreach (Pipe p in GNS.mainWindow.Pipes)
                     {
                         if (!p.Data.Properties.ContainsKey(textBox1.Text))
                             p.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                         else p.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                     }

                     if (!GNS.mainWindow.PipeGlobalProperties.ContainsKey(textBox1.Text))
                         GNS.mainWindow.PipeGlobalProperties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                     else GNS.mainWindow.PipeGlobalProperties[textBox1.Text] = textBox2.Text + ";10;10;1";
                 }
                 else if (GNS.comboBox1.SelectedIndex == 3)
                 {
                     if (GNS.comboBox2.SelectedIndex == 1)
                     {
                         //node is entering node
                         foreach (Node n in GNS.mainWindow.Nodes.Where(item => item.Data.currentEnterExit == AdditionalNodeData.EnterExitType.Enter))
                         {
                             if (!n.Data.Properties.ContainsKey(textBox1.Text))
                                 n.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else n.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }
                     }
                     else if (GNS.comboBox2.SelectedIndex == 2)
                     {
                         //node is exiting node
                         foreach (Node n in GNS.mainWindow.Nodes.Where(item => item.Data.currentEnterExit == AdditionalNodeData.EnterExitType.Exit))
                         {
                             if (!n.Data.Properties.ContainsKey(textBox1.Text))
                                 n.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else n.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }
                     }
                     else if (GNS.comboBox2.SelectedIndex == 2)
                     {
                         //node is exiting node
                         foreach (Node n in GNS.mainWindow.Nodes.Where(item => item.Data.currentEnterExit == AdditionalNodeData.EnterExitType.None))
                         {
                             if (!n.Data.Properties.ContainsKey(textBox1.Text))
                                 n.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else n.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }
                     }
                 }
                 else if (GNS.comboBox1.SelectedIndex == 4)
                 {
                     if (GNS.comboBox2.SelectedIndex == 0)
                     {
                         //pipes where no attached object
                         foreach (Pipe p in GNS.mainWindow.Pipes.Where(item => item.ObjectType == Pipe.ObjectTypes.Type1))
                         {
                             if (!p.Data.Properties.ContainsKey(textBox1.Text))
                                 p.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else p.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }
                     }
                     else if (GNS.comboBox2.SelectedIndex == 1)
                     {
                         foreach (Pipe p in GNS.mainWindow.Pipes.Where(item => item.ObjectType == Pipe.ObjectTypes.Type2))
                         {
                             if (!p.Data.Properties.ContainsKey(textBox1.Text))
                                 p.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else p.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }

                     }
                     else if (GNS.comboBox2.SelectedIndex == 2)
                     {
                         foreach (Pipe p in GNS.mainWindow.Pipes.Where(item => item.ObjectType == Pipe.ObjectTypes.Type3))
                         {
                             if (!p.Data.Properties.ContainsKey(textBox1.Text))
                                 p.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else p.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }


                     }
                     else if (GNS.comboBox2.SelectedIndex == 3)
                     {
                         foreach (Pipe p in GNS.mainWindow.Pipes.Where(item => item.ObjectType == Pipe.ObjectTypes.Type4))
                         {
                             if (!p.Data.Properties.ContainsKey(textBox1.Text))
                                 p.Data.Properties.Add(textBox1.Text, textBox2.Text + ";10;10;1");
                             else p.Data.Properties[textBox1.Text] = textBox2.Text + ";10;10;1";
                         }

                     }
                 }
             }
             #endregion
             GNS.mainWindow.GLControlInvalidate();

             this.Close();
         }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

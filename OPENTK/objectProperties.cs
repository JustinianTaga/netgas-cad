﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using System.Reflection;
using System.IO;

namespace OPENTK
{
    public partial class objectProperties : Form
    {
        public Node selectedNode;
        public Pipe selectedPipe;
        public Image selectedImage;
        double aspectRatio;
        Bitmap bmpImage;
        int okLoaded = 0;
        TextBox txt = new TextBox();
        TextBox txt2 = new TextBox();
        double defWidth, defHeight;
        int xLoc = 13, yLoc = 13;
        CheckBox chk = new CheckBox();
        Font buttonsFont;
        public Form1 mainWindow;
        public objectProperties(Node n, Pipe p, Image img, Form1 _mainWindow)
        {
            InitializeComponent();
            mainWindow = _mainWindow;
            buttonsFont = button1.Font;
            if (n == null) selectedNode = null;
            if (p == null) selectedPipe = null;
            if (img == null) selectedImage = null;
            if (n != null)
            {
                selectedNode = n;
                this.Text = n.Index.ToString();
                reloadNode();
                comboBox1.Items.Clear();
                if (mainWindow.Subnetworks.Count > 0)
                {
                    foreach (string name in mainWindow.Subnetworks)
                    {
                        comboBox1.Items.Add(name);
                    }

                    if (comboBox1.Items.Contains(selectedNode.Subnetwork))
                    {
                        comboBox1.SelectedItem = selectedNode.Subnetwork;
                    }
                    else
                    {
                        comboBox1.SelectedIndex = 0;
                    }
                }
                else
                {
                    comboBox1.Enabled = false;
                    comboBox1.Items.Add("No subnetworks existing");
                    comboBox1.SelectedIndex = 0;
                }
            }
            else if(p != null)
            {
                selectedPipe = p;
                reloadPipe();
                comboBox1.Items.Clear();
                if (mainWindow.Subnetworks.Count > 0)
                {
                    foreach (string name in mainWindow.Subnetworks)
                    {
                        comboBox1.Items.Add(name);
                    }
                    if (comboBox1.Items.Contains(selectedPipe.Subnetwork))
                    {
                        comboBox1.SelectedItem = selectedPipe.Subnetwork;
                    }
                    else
                    {
                        comboBox1.SelectedIndex = 0;
                    }
                }
                else
                {
                    comboBox1.Enabled = false;
                    comboBox1.Items.Add("No subsystems existing");
                    comboBox1.SelectedIndex = 0;
                }
            }
            else if(img != null)
            {
                selectedImage = img;
                Label lbl = new Label();
                lbl.Text = "Width: ";
                txt.Name = "Width";
                txt.Text = selectedImage.Width.ToString();
                lbl.Location = new Point(xLoc, yLoc);
                xLoc = 120;
                txt.Location = new Point(xLoc, yLoc);
                txt.KeyDown += new KeyEventHandler(txt_KeyDown);
                this.Controls.Add(lbl);
                this.Controls.Add(txt);
                xLoc = 13;
                yLoc += 30;
                this.Height += 30;
                Label lbl2 = new Label();
                lbl2.Text = "Height: ";
                bmpImage = new Bitmap(selectedImage.FileName);
               
                txt2.Name = "Height";
                txt2.Text = selectedImage.Height.ToString();
                lbl2.Location = new Point(xLoc, yLoc);
                xLoc = 120;
                txt2.Location = new Point(xLoc, yLoc);
                txt2.KeyDown += new KeyEventHandler(txt2_KeyDown);
                this.Controls.Add(lbl2);
                this.Controls.Add(txt2);
                xLoc = 13;
                yLoc += 30;
                this.Height += 30;
                chk.Location = new Point(xLoc, yLoc);
                chk.Text = "Keep aspect ratio";
                chk.Checked = true;
                this.Controls.Add(chk);
                this.Height += 30;
                 aspectRatio = Double.Parse(txt.Text) / Double.Parse(txt2.Text);
                 defWidth = double.Parse(txt.Text);
                 defHeight = double.Parse(txt2.Text);
            }
        }
        public void reloadNode()
        {
            okLoaded = 0;
            panel1.Controls.Clear();
            this.Invalidate();
            xLoc = 13;
            yLoc = 13;
            Node oldSelectedNode = selectedNode;
            selectedNode = mainWindow.Nodes.Where(item => item.Index == oldSelectedNode.Index).Single();
            foreach (object key in selectedNode.Data.Properties.Keys)
            {
                Label lbl = new Label();
                lbl.Text = key.ToString();
                lbl.Location = new Point(xLoc, yLoc);
                panel1.Controls.Add(lbl);
                xLoc += 100;

                TextBox txt = new TextBox();
                txt.Text = selectedNode.Data.Properties[key].ToString().Split(';').First();
                txt.Location = new Point(xLoc, yLoc);
                txt.Tag = key.ToString();
                panel1.Controls.Add(txt);
                txt.BringToFront();
                xLoc += txt.Width + 10;
                Button btn = new Button();
                btn.Tag = lbl.Text;
                btn.Text = "Remove";
                btn.Location = new Point(xLoc, yLoc);
                btn.Click += new EventHandler(remove_Click);
                panel1.Controls.Add(btn);

                xLoc = 13;
                yLoc += 30;
                Label lbl2 = new Label();
                lbl2.Text = "Text X location:";
                lbl2.Location = new Point(xLoc, yLoc);
                panel1.Controls.Add(lbl2);
                CheckBox chk = new CheckBox();
                chk.Location = new Point(btn.Location.X, yLoc - 3); //new Point(xLoc, yLoc - 3);
                chk.Text = "Display text";
                chk.Tag = key.ToString();
                chk.CheckedChanged += new EventHandler(showText_CheckChanged);
                if (Int32.Parse(selectedNode.Data.Properties[key].ToString().Split(';')[3]) == 1)
                {
                    chk.Checked = true;
                }
                panel1.Controls.Add(chk);
                yLoc += 30;
                xLoc = 13;
                TrackBar track = new TrackBar();
                track.Minimum = -50;
                track.Maximum = 50;
                track.Value = Int32.Parse(selectedNode.Data.Properties[key].ToString().Split(';')[1]);
                track.Width = lbl.Width + txt.Width + btn.Width;
                track.Location = new Point(xLoc, yLoc);
                track.Tag = key.ToString();
                track.ValueChanged += new EventHandler(xLoc_ValueChanged);
                track.BringToFront();
                panel1.Controls.Add(track);


                xLoc = 13;
                yLoc += 50;
                Label lbl3 = new Label();
                lbl3.Text = "Text Y location:";
                lbl3.Location = new Point(xLoc, yLoc);
                panel1.Controls.Add(lbl3);
                lbl3.BringToFront();
                yLoc += 30;
                xLoc = 13;
                TrackBar track2 = new TrackBar();
                track2.Minimum = -50;
                track2.Maximum = 50;
                track2.Value = Int32.Parse(selectedNode.Data.Properties[key].ToString().Split(';')[2]);
                track2.Width = lbl.Width + txt.Width + btn.Width;
                track2.Location = new Point(xLoc, yLoc);
                track2.Tag = key.ToString();
                track2.BringToFront();
                track2.ValueChanged += new EventHandler(yLoc_ValueChanged);
                panel1.Controls.Add(track2);

                yLoc += 40;
                xLoc = 13;
                Panel panelDelimiter = new Panel();
                panelDelimiter.BackColor = Color.Black;
                panelDelimiter.Height = 2;
                panelDelimiter.Width = track.Width;
                panelDelimiter.Location = new Point(xLoc, yLoc);

                panel1.Controls.Add(panelDelimiter);
                panelDelimiter.BringToFront();
                yLoc += 50;
              
            }
            foreach(Control ctrl in panel1.Controls)
            {
                if(ctrl is Button) ctrl.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
                else ctrl.Anchor = (AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right);
            }
            mainWindow.GLControlInvalidate();
            okLoaded = 1;
        }

        private void showText_CheckChanged(object sender, EventArgs e)
        {
          if(selectedNode != null)
          {
              if (okLoaded == 1)
              {
                  string oldData = mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Data.Properties[((CheckBox)sender).Tag.ToString()].ToString();
                  int checkValue = 0;
                  if (((CheckBox)sender).Checked == true) checkValue = 1;
                  mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Data.Properties[((CheckBox)sender).Tag.ToString()] = oldData.Split(';')[0] + ";" + oldData.Split(';')[1] + ";" + oldData.Split(';')[2] + ";" + checkValue.ToString();
                  mainWindow.GLControlInvalidate();
              }
          }
          else if(selectedPipe != null)
          {
              if (okLoaded == 1)
              {
                  string oldData = mainWindow.Pipes.Where(item => item.Index == selectedPipe.Index).Single().Data.Properties[((CheckBox)sender).Tag.ToString()].ToString();
                  int checkValue = 0;
                  if (((CheckBox)sender).Checked == true) checkValue = 1;
                  mainWindow.Pipes.Where(item => item.Index == selectedPipe.Index).Single().Data.Properties[((CheckBox)sender).Tag.ToString()] = oldData.Split(';')[0] + ";" + oldData.Split(';')[1] + ";" + oldData.Split(';')[2] + ";" + checkValue.ToString();
                  mainWindow.GLControlInvalidate();
              }
          }
        }

        private void xLoc_ValueChanged(object sender, EventArgs e)
        {
          if(selectedNode != null)
          {
              string oldData = mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Data.Properties[((TrackBar)sender).Tag.ToString()].ToString();
              mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Data.Properties[((TrackBar)sender).Tag.ToString()] = oldData.Split(';')[0] + ";" + Int32.Parse(((TrackBar)sender).Value.ToString()) + ";" + oldData.Split(';')[2] + ";" + oldData.Split(';')[3];
              mainWindow.GLControlInvalidate();
          }
          else if(selectedPipe != null)
          {
              string oldData = mainWindow.Pipes.Where(item => item.Index == selectedPipe.Index).Single().Data.Properties[((TrackBar)sender).Tag.ToString()].ToString();
              mainWindow.Pipes.Where(item => item.Index == selectedPipe.Index).Single().Data.Properties[((TrackBar)sender).Tag.ToString()] = oldData.Split(';')[0] + ";" + Int32.Parse(((TrackBar)sender).Value.ToString()) + ";" + oldData.Split(';')[2] + ";" + oldData.Split(';')[3];
              mainWindow.GLControlInvalidate();
          }

        }

        private void yLoc_ValueChanged(object sender, EventArgs e)
        {
            if(selectedNode != null)
            {
                string oldData = mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Data.Properties[((TrackBar)sender).Tag.ToString()].ToString();
                mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Data.Properties[((TrackBar)sender).Tag.ToString()] = oldData.Split(';')[0] + ";" + oldData.Split(';')[1] + ";" + Int32.Parse(((TrackBar)sender).Value.ToString()) + ";" + oldData.Split(';')[3];
                mainWindow.GLControlInvalidate();
            }
            else if(selectedPipe != null)
            {
                string oldData = mainWindow.Pipes.Where(item => item.Index == selectedPipe.Index).Single().Data.Properties[((TrackBar)sender).Tag.ToString()].ToString();
                mainWindow.Pipes.Where(item => item.Index == selectedPipe.Index).Single().Data.Properties[((TrackBar)sender).Tag.ToString()] = oldData.Split(';')[0] + ";" + oldData.Split(';')[1] + ";" + Int32.Parse(((TrackBar)sender).Value.ToString()) + ";" + oldData.Split(';')[3];
                mainWindow.GLControlInvalidate();
            }
        }

        private void remove_Click(object sender, EventArgs e)
        {
            if(selectedNode != null)
            {
                Button btn = ((Button)sender);
                selectedNode.Data.Properties.Remove(btn.Tag.ToString());
                foreach(Node n in mainWindow.Nodes)
                {
                    n.Data.Properties.Remove(btn.Tag.ToString());
                }
                reloadNode();
            }
            else if(selectedPipe != null)
            {
                Button btn = ((Button)sender);
                selectedPipe.Data.Properties.Remove(btn.Tag.ToString());
                foreach(Pipe p in mainWindow.Pipes)
                {
                    p.Data.Properties.Remove(btn.Tag.ToString());
                }
                reloadPipe();
            }
        }

        public void reloadPipe()
        {
            panel1.Controls.Clear();
            xLoc = 13;
            yLoc = 13;
            okLoaded = 0;
            foreach (object key in selectedPipe.Data.Properties.Keys)
            {
                this.Text = "Pipe " + selectedPipe.Index.ToString() + " properties";
                Label lbl = new Label();
                lbl.Text = key.ToString();
                lbl.Location = new Point(xLoc, yLoc);
                panel1.Controls.Add(lbl);
                xLoc += 100;

                TextBox txt = new TextBox();
                txt.Text = selectedPipe.Data.Properties[key].ToString().Split(';').First();
                txt.Location = new Point(xLoc, yLoc);
                txt.Tag = key.ToString();
                panel1.Controls.Add(txt);
                txt.BringToFront();
                xLoc += txt.Width + 10;
                Button btn = new Button();
                btn.Tag = lbl.Text;
                btn.Text = "Remove";
                btn.Location = new Point(xLoc, yLoc);
                btn.Click += new EventHandler(remove_Click);
                panel1.Controls.Add(btn);

                xLoc = 13;
                yLoc += 30;
                Label lbl2 = new Label();
                lbl2.Text = "Text X location:";
                lbl2.Location = new Point(xLoc, yLoc);
                panel1.Controls.Add(lbl2);
                CheckBox chk = new CheckBox();
                chk.Location = new Point(btn.Location.X, yLoc - 3); //new Point(xLoc, yLoc - 3);
                chk.Text = "Display text";
                chk.Tag = key.ToString();
                chk.CheckedChanged += new EventHandler(showText_CheckChanged);
                if (Int32.Parse(selectedPipe.Data.Properties[key].ToString().Split(';')[3]) == 1)
                {
                    chk.Checked = true;
                }
                panel1.Controls.Add(chk);
                yLoc += 30;
                xLoc = 13;
                TrackBar track = new TrackBar();
                track.Minimum = -100;
                track.Maximum = 100;
                track.Value = Int32.Parse(selectedPipe.Data.Properties[key].ToString().Split(';')[1]);
                track.Width = lbl.Width + txt.Width + btn.Width;
                track.Location = new Point(xLoc, yLoc);
                track.Tag = key.ToString();
                track.ValueChanged += new EventHandler(xLoc_ValueChanged);
                track.BringToFront();
                panel1.Controls.Add(track);


                xLoc = 13;
                yLoc += 50;
                Label lbl3 = new Label();
                lbl3.Text = "Text Y location:";
                lbl3.Location = new Point(xLoc, yLoc);
                panel1.Controls.Add(lbl3);
                lbl3.BringToFront();
                yLoc += 30;
                xLoc = 13;
                TrackBar track2 = new TrackBar();
                track2.Minimum = -100;
                track2.Maximum = 100;
                track2.Value = Int32.Parse(selectedPipe.Data.Properties[key].ToString().Split(';')[2]);
                track2.Width = lbl.Width + txt.Width + btn.Width;
                track2.Location = new Point(xLoc, yLoc);
                track2.Tag = key.ToString();
                track2.BringToFront();
                track2.ValueChanged += new EventHandler(yLoc_ValueChanged);
                panel1.Controls.Add(track2);

                yLoc += 40;
                xLoc = 13;
                Panel panelDelimiter = new Panel();
                panelDelimiter.BackColor = Color.Black;
                panelDelimiter.Height = 2;
                panelDelimiter.Width = track.Width;
                panelDelimiter.Location = new Point(xLoc, yLoc);

                panel1.Controls.Add(panelDelimiter);
                panelDelimiter.BringToFront();
                yLoc += 50;
            }
            okLoaded = 1;
        }

        private void txt2_KeyDown(object sender, EventArgs e)
        {
            if (chk.Checked == true)
            {

                Double newWidth = (double.Parse(txt2.Text) * aspectRatio);
                //Double newHeight = (Double.Parse(txt.Text) / aspectRatio);
                txt.Text = newWidth.ToString();

            }
        }

        private void txt_KeyDown(object sender, EventArgs e)
        {
            if (chk.Checked == true)
            {
                
               // Double newWidth = (Double.Parse(txt2.Text) / aspectRatio);
                this.Text = aspectRatio.ToString();
               Double newHeight = (double.Parse(txt.Text) * aspectRatio);
                txt2.Text = newHeight.ToString();

            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
           if(selectedNode == null && selectedPipe != null)
           {
               if(comboBox1.Enabled == true)
               {
                   mainWindow.Pipes.Where(item => item.Index == selectedPipe.Index).Single().Subnetwork = comboBox1.SelectedItem.ToString();
               }
               foreach (Control lbl in panel1.Controls)
               {
                   if (lbl is Label)
                   {
                       foreach (Control txt in panel1.Controls)
                       {
                           if (txt is TextBox)
                           {
                               if (txt.Tag.ToString() == lbl.Text)
                               {
                                   string oldData = mainWindow.Pipes.Where(item => item.Index == selectedPipe.Index).Single().Data.Properties[txt.Tag].ToString();
                                   mainWindow.Pipes.Where(item => item.Index == selectedPipe.Index).Single().Data.Properties[txt.Tag] = txt.Text + ";" + oldData.Split(';')[1] + ";" + oldData.Split(';')[2] + ";" + oldData.Split(';')[3];
                                   mainWindow.GLControlInvalidate();
                               }
                           }
                       }
                   }
               }
           }
           else if (selectedNode != null && selectedPipe == null)
           {
               if (comboBox1.Enabled == true)
               {
                   mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Subnetwork = comboBox1.SelectedItem.ToString();
               }
                foreach(Control lbl in panel1.Controls)
                {
                   if(lbl is Label)
                   {
                       foreach (Control txt in panel1.Controls)
                       {
                          if(txt is TextBox)
                          {
                              if (txt.Tag.ToString() == lbl.Text)
                              {
                                  string oldData = mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Data.Properties[txt.Tag].ToString();
                                  mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Data.Properties[txt.Tag] = txt.Text + ";" + oldData.Split(';')[1] + ";" + oldData.Split(';')[2] + ";" + oldData.Split(';')[3];
                                  mainWindow.GLControlInvalidate();
                                 // mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Data.Properties[lbl.Text] = //txt.Text;
                              }
                          }
                       }
                   }
                }
                this.Close();
                mainWindow.GLControlInvalidate();
           }
           else if(selectedNode == null && selectedPipe == null && selectedImage != null)
           {
                   foreach (Control t in this.Controls)
                   {
                       if (t is TextBox)
                       {
                           if (t.Name == "Width") 
                           mainWindow.Images.Where(item => item.Index == selectedImage.Index).Single().Width = float.Parse(t.Text);
                           if (t.Name == "Height")
                               mainWindow.Images.Where(item => item.Index == selectedImage.Index).Single().Height = float.Parse(t.Text);
                       }
                   }
           }
           this.Close();
           mainWindow.GLControlInvalidate();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AddProperty addProperty = new AddProperty(this, null);
            addProperty.ShowDialog();
        }

        private void objectProperties_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (selectedNode != null)
            {
                mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Font = null;

            }
            else if (selectedPipe != null)
            {
                mainWindow.Pipes.Where(item => item.Index == selectedPipe.Index).Single().Font = null;
                

            }
            mainWindow.GLControlInvalidate();
            button4.Font = buttonsFont;
            button5.Font = buttonsFont;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FontDialog fontDialog = new FontDialog();
            if (fontDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (selectedNode != null)
                {
                    mainWindow.Nodes.Where(item => item.Index == selectedNode.Index).Single().Font = fontDialog.Font;
                    selectedNode.Font = fontDialog.Font;
                    groupBox1.Font = fontDialog.Font;
                }
                else if (selectedPipe != null)
                {
                    mainWindow.Pipes.Where(item => item.Index == selectedPipe.Index).Single().Font = fontDialog.Font;
                    selectedPipe.Font = fontDialog.Font;
                    groupBox1.Font = fontDialog.Font;
                }
                mainWindow.GLControlInvalidate();
                button4.Font = buttonsFont;
                button5.Font = buttonsFont;
            }
        }
    }
}

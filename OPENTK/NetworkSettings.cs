﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OPENTK
{
    public partial class NetworkSettings : Form
    {
        Form1 mainWindow;
        public NetworkSettings(Form1 _form1)
        {
            InitializeComponent();
            mainWindow = _form1;
        }

        private void NetworkSettings_Load(object sender, EventArgs e)
        {
            loadNetworkTypes();
        }


        private void loadNetworkTypes()
        {
            comboBox1.Items.Add("Gas");
            comboBox1.Items.Add("Liquid");
            comboBox1.Items.Add("Gas & Condensed");
            if (mainWindow.NetworkType == Form1.NetworkTypes.Gas)
                comboBox1.SelectedIndex = 0;
            if (mainWindow.NetworkType == Form1.NetworkTypes.Liquid)
                comboBox1.SelectedIndex = 1;
            if (mainWindow.NetworkType == Form1.NetworkTypes.GasCondensed)
                comboBox1.SelectedIndex = 2;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
                mainWindow.NetworkType = Form1.NetworkTypes.Gas;
            if (comboBox1.SelectedIndex == 1)
                mainWindow.NetworkType = Form1.NetworkTypes.Liquid;
            if (comboBox1.SelectedIndex == 2)
                mainWindow.NetworkType = Form1.NetworkTypes.GasCondensed;

            mainWindow.toolStripStatusLabel1.Text = "Network type: " + comboBox1.SelectedItem.ToString();

            this.Close();
        }

    }
}

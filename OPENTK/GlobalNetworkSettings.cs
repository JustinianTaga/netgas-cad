﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OPENTK
{
    public partial class GlobalNetworkSettings : Form
    {
        public Form1 mainWindow;
        public string keyToModify = "";
        public GlobalNetworkSettings(Form1 _mainWindow)
        {
            InitializeComponent();
            mainWindow = _mainWindow;
            
        }

        private void GlobalNetworkSettings_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Add("All elements");
            comboBox1.Items.Add("Nodes only");
            comboBox1.Items.Add("Pipes only");
            comboBox1.Items.Add("Specific Nodes");
            comboBox1.Items.Add("Specific Pipes");
            comboBox1.SelectedIndex = 0;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            keyToModify = "";
            AddProperty addProperty = new AddProperty(null, this);
            addProperty.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if(comboBox1.SelectedIndex == 3)
            {
                comboBox2.Items.Clear();
                comboBox2.Items.Add("Node type is not \"Entering node\" and not \"Exiting node\"");
                comboBox2.Items.Add("Node type is \"Entering node\"");
                comboBox2.Items.Add("Node type is \"Exiting node\"");
                comboBox2.SelectedIndex = 0;
                groupBox2.Enabled = true;
                comboBox3.Items.Clear();
                if (mainWindow.NodeGlobalProperties.Keys.Count > 0)
                {
                    foreach (object key in mainWindow.NodeGlobalProperties.Keys)
                    {
                        comboBox3.Items.Add(key.ToString());
                    }
                    button3.Enabled = true;
                    comboBox3.SelectedIndex = 0;
                }
                else
                {
                    comboBox3.Items.Add("No global properties for nodes");
                    comboBox3.SelectedIndex = 0;
                    comboBox3.Enabled = false;
                    button3.Enabled = true;
                }

            }
            else if(comboBox1.SelectedIndex == 4)
            {
                comboBox2.Items.Clear();
                comboBox2.Items.Add("Pipe has no attached object");
                comboBox2.Items.Add("Pipe has attached object of type 2");
                comboBox2.Items.Add("Pipe has attached object of type 3");
                comboBox2.Items.Add("Pipe has attached object of type 4");
                comboBox2.SelectedIndex = 0;
                groupBox2.Enabled = true;
                comboBox3.Items.Clear();
                if (mainWindow.PipeGlobalProperties.Keys.Count > 0)
                {
                    foreach (object key in mainWindow.PipeGlobalProperties.Keys)
                    {
                        comboBox3.Items.Add(key.ToString());
                    }
                    comboBox3.SelectedIndex = 0;
                    button3.Enabled = true;
                }
                else
                {
                    comboBox3.Items.Add("No global properties for pipes");
                    comboBox3.SelectedIndex = 0;
                    button3.Enabled = false;
                    comboBox3.Enabled = false;
                }
            }
            else if(comboBox1.SelectedIndex == 1)
            {
                comboBox3.Items.Clear();
                if(mainWindow.NodeGlobalProperties.Keys.Count > 0)
                {
                    foreach (object key in mainWindow.NodeGlobalProperties.Keys)
                    {
                        comboBox3.Items.Add(key.ToString());
                    }
                    comboBox3.SelectedIndex = 0;
                    comboBox3.Enabled = true;
                }
                else
                {
                    comboBox3.Items.Add("No global properties for nodes");
                    comboBox3.SelectedIndex = 0;
                    comboBox3.Enabled = false;
                }
                groupBox2.Enabled = false;
                comboBox2.Items.Clear();
            }
            else if(comboBox1.SelectedIndex == 2)
            {
                comboBox3.Items.Clear();
                if (mainWindow.PipeGlobalProperties.Keys.Count > 0)
                {
                    foreach (object key in mainWindow.PipeGlobalProperties.Keys)
                    {
                        comboBox3.Items.Add(key.ToString());
                    }
                    comboBox3.SelectedIndex = 0;
                    comboBox3.Enabled = true;
                }
                else
                {
                    comboBox3.Items.Add("No global properties for pipes");
                    comboBox3.SelectedIndex = 0;
                    comboBox3.Enabled = false;
                }
                groupBox2.Enabled = false;
                comboBox2.Items.Clear();
            }
            else
            {
                comboBox3.Items.Clear();
                button3.Enabled = false;
                comboBox3.Enabled = false;
                comboBox3.Items.Add("No available options");
                comboBox3.SelectedIndex = 0;
                groupBox2.Enabled = false;
                comboBox2.Items.Clear();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            keyToModify = comboBox3.SelectedItem.ToString();
            AddProperty addProperty = new AddProperty(null, this);
            addProperty.ShowDialog();
        }

    }
}

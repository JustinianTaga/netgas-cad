﻿using OpenTK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OPENTK
{
    public class Pipe
    {
        private string _subnetwork= "";
        private bool _drawShadow = false;
        public Layer layer;
        public bool DrawText = true;
        public bool DrawShadow
        {
            get
            {
                return _drawShadow;
            }
            set
            {
                _drawShadow = value;
            }
        }

        public enum PipeTypes
        {
            Line,
            Curve
        }

        public enum ObjectTypes
        {
            Type1,
            Type2,
            Type3,
            Type4
        }

        public string Subnetwork
        {
            get
            {
                return _subnetwork;
            }
            set
            {
                _subnetwork = value;
            }
        }
        private System.Drawing.Font _font = null;
        public System.Drawing.Font Font
        {
            get
            {
                return _font;
            }
            set
            {
                _font = value;
            }
        }

        public ObjectTypes ObjectType = ObjectTypes.Type1;
        public PipeTypes PipeType = PipeTypes.Line;
        public Vector2 ControlPoint, auxControlPoint;
        public int drawnPoints = 1500, ControlPointIndex = 0;
           public Node auxNode1, auxNode2;
           public Node Node1;
           public Node Node2;
        public string[] properties = new string[100];
        public float Width = 1f;
        public int Index;
        public AdditionalPipeData Data = new AdditionalPipeData();
        public Pipe(int ID, Node node1, Node node2)
        {
            Index = ID;
            Node1 = node1;
            Node2 = node2;
            auxNode1 = node1;
            auxNode2 = node2;
            Data.ID = ID;
            ControlPointIndex = ID;
            for (int i = 0; i <= 53; i++)
                properties[i] = "-";
            properties[16] = ((int)node1.Vector.X).ToString();
            properties[17] = ((int)node1.Vector.Y).ToString();
            properties[19] = ((int)node2.Vector.X).ToString();
            properties[20] = ((int)node2.Vector.Y).ToString();
            properties[3] = Index.ToString();
        }

        public Pipe(int ID, Node node1, Node node2, string[] p)
        {
            Index = ID;
            Node1 = node1;
            Node2 = node2;
            auxNode1 = node1;
            auxNode2 = node2;
            Data.ID = ID;
            ControlPointIndex = ID;
            for (int i = 0; i <= 53; i++)
                properties[i] = p[i];
        }

        public Pipe(int ID, Node node1, Node node2, PipeTypes type, Vector2 controlPoint)
        {
            Index = ID;
            Node1 = node1;
            Node2 = node2;
            auxNode1 = node1;
            auxNode2 = node2;
            Data.ID = ID;
            PipeType = type;
            ControlPoint = controlPoint;
            ControlPointIndex = ID;
            auxControlPoint = controlPoint;
            for (int i = 0; i <= 53; i++)
                properties[i] = "-";
            properties[16] = ((int)node1.Vector.X).ToString();
            properties[17] = ((int)node1.Vector.Y).ToString();
            properties[19] = ((int)node2.Vector.X).ToString();
            properties[20] = ((int)node2.Vector.Y).ToString();
            properties[3] = Index.ToString();
            properties[51] = "1";
            properties[52] = controlPoint.X.ToString();
            properties[53] = controlPoint.Y.ToString();
        }

        public Pipe(int ID, Node node1, Node node2, PipeTypes type, Vector2 controlPoint, string[] p)
        {
            Index = ID;
            Node1 = node1;
            Node2 = node2;
            auxNode1 = node1;
            auxNode2 = node2;
            Data.ID = ID;
            PipeType = type;
            ControlPoint = controlPoint;
            ControlPointIndex = ID;
            auxControlPoint = controlPoint;
            for (int i = 0; i <= 53; i++)
                properties[i] = p[i];
        }
    }
}

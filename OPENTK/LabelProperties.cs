﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OPENTK
{
    public partial class LabelProperties : Form
    {
        label selectedLabel;
        Form1 mainWindow;
        Font buttonsFont;
        public LabelProperties(label lbl, Form1 _mainWindow)
        {
            InitializeComponent();
            selectedLabel = lbl;
            mainWindow = _mainWindow;
            buttonsFont = button1.Font;
            if (selectedLabel.font != null)
            {
                this.groupBox1.Font = selectedLabel.font;
            }
            else
            {
                this.groupBox1.Font = mainWindow.font;
            }
            button5.Font = buttonsFont;
            button4.Font = buttonsFont;
            this.textBox1.Text = selectedLabel.Text;
            if(selectedLabel.Static == false)
            {
                radioButton2.Checked = true;
            }
            else
            {
                radioButton1.Checked = true;
            }
            if(selectedLabel.nodeAssignedIndex == -1 && selectedLabel.pipeAssignedIndex == -1)
            {
                 comboBox1.SelectedIndex = 0;
                 comboBox2.Items.Clear();
                 comboBox2.Enabled = false;
                 groupBox2.Enabled = false;
            }
            else if(selectedLabel.nodeAssignedIndex != -1)
            {
                comboBox1.SelectedIndex = 1;
                comboBox2.SelectedItem = "Node " + selectedLabel.nodeAssignedIndex.ToString();
                groupBox2.Enabled = true;
            }
            else if(selectedLabel.pipeAssignedIndex != -1)
            {
                comboBox1.SelectedIndex = 2;
                comboBox2.SelectedItem = "Pipe " + selectedLabel.pipeAssignedIndex.ToString();
                groupBox2.Enabled = true;
            }

            if (selectedLabel.MarkedLabel == true)
            {
                checkBox1.Checked = true;
            }
            else
            {
                checkBox1.Checked = false;
            }
            if (selectedLabel.assignmentStartPosition == 1) radioPos1.Checked = true;
            else if (selectedLabel.assignmentStartPosition == 2) radioPos2.Checked = true;
            else if (selectedLabel.assignmentStartPosition == 3) radioPos3.Checked = true;
            else if (selectedLabel.assignmentStartPosition == 4) radioPos4.Checked = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().Text = textBox1.Text;
            mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().font = this.groupBox1.Font;
            if(radioButton1.Checked == true)
            {
                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().Static = true;
            }
            else
            {
                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().Static = false;
            }
            if(comboBox1.SelectedIndex == 1)
            {
                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().nodeAssignedIndex = Int32.Parse(comboBox2.SelectedItem.ToString().Split(' ')[1]);
                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().pipeAssignedIndex = -1;
            }
            else if(comboBox1.SelectedIndex == 2)
            {
                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().pipeAssignedIndex = Int32.Parse(comboBox2.SelectedItem.ToString().Split(' ')[1]);

                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().nodeAssignedIndex = -1;
            }
            else if(comboBox1.SelectedIndex == 0)
            {
                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().nodeAssignedIndex = -1;
                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().pipeAssignedIndex = -1;
            }
            if (checkBox1.Checked == true) mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().MarkedLabel = true;
            else mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().MarkedLabel = false;

            if (radioPos1.Checked == true)
            {
                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().assignmentStartPosition = 1;
            }
            if (radioPos2.Checked == true)
            {
                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().assignmentStartPosition = 2;
            }
            if (radioPos3.Checked == true)
            {
                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().assignmentStartPosition = 3;
            }
            if (radioPos4.Checked == true)
            {
                mainWindow.Labels.Where(item => item.Index == selectedLabel.Index).Single().assignmentStartPosition = 4;
            }


            this.Close();
            mainWindow.GLControlInvalidate();
        }

        private void button5_Click(object sender, EventArgs e)
        {
              FontDialog fontDialog = new FontDialog();
              if (fontDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
              {
                  this.groupBox1.Font = fontDialog.Font;
                  mainWindow.GLControlInvalidate();
                  button5.Font = buttonsFont;
                  button4.Font = buttonsFont;
              }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.groupBox1.Font = mainWindow.font;
            button5.Font = buttonsFont;
            button4.Font = buttonsFont;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex != 0)
            {
                if(comboBox1.SelectedIndex == 1)
                {
                    comboBox2.Items.Clear();
                    if(mainWindow.Nodes.Count > 0)
                    {
                        comboBox2.Enabled = true;
                        groupBox2.Enabled = true;
                        foreach (Node n in mainWindow.Nodes)
                        {
                            comboBox2.Items.Add("Node " + n.Index.ToString());
                        }
                    }
                    else
                    {
                        comboBox2.Items.Add("No nodes on the form.");
                        comboBox2.Enabled = false;
                        groupBox2.Enabled = false;
                    }
                    comboBox2.SelectedIndex = 0;
                }
                else if (comboBox1.SelectedIndex == 2)
                {
                    comboBox2.Items.Clear();
                    if (mainWindow.Pipes.Count > 0)
                    {
                        comboBox2.Enabled = true;
                        groupBox2.Enabled = true;
                        foreach (Pipe p in mainWindow.Pipes)
                        {
                            comboBox2.Items.Add("Pipe " + p.Index.ToString());
                        }
                    }
                    else
                    {
                        comboBox2.Items.Add("No pipes on the form.");
                        comboBox2.Enabled = false;
                        groupBox2.Enabled = false;
                    }
                    comboBox2.SelectedIndex = 0;
                }
            }
            else
            {
                comboBox2.Items.Clear();
                comboBox2.Enabled = false;
                groupBox2.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            mainWindow.GLControl.Invalidate();
        }

    }
}

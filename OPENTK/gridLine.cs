﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OPENTK
{
    public class gridLine
    {
        public float X1, X2, Y1, Y2;
        public float defaultX1, defaultX2, defaultY1, defaultY2;
        public float auxX1, auxX2, auxY1, auxY2;
        public float maxX1, maxX2, maxY1, maxY2;
        public bool evidentiat = false;
        public enum _Type
        {
            Horizontal,
            Vertical
        }
        public int Index;
        public _Type Type;
        // - |
        public gridLine(int _ID, float x1, float y1, float x2, float y2, _Type type)
        {
            Index = _ID;
            Type = type;
            X1 = x1;
            X2 = x2;
            Y1 = y1;
            Y2 = y2;
            auxX1 = x1;
            auxX2 = x2;
            auxY1 = y1;
            auxY2 = y2;
            defaultX1 = x1;
            defaultX2 = x2;
            defaultY1 = y1;
            defaultY2 = y2;
            maxX1 = x1;
            maxX2 = x2;
            maxY1 = y1;
            maxY2 = y2;
        }
        public gridLine()
        {

        }
    }
}

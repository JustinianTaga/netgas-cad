﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace OPENTK
{
    public class AdditionalPipeData
    {

        int _id;
        string _objectType = "Pipe";
        public Hashtable Properties = new Hashtable();
        [CategoryAttribute("Pipe data")]
        [DisplayName("ID")]
        [Description("ID of the selected Pipe")]
        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        [CategoryAttribute("Pipe data")]
        [DisplayName("Object Type")]
        [Description("Type of the selected object")]
        public string ObjectType
        {
            get
            {
                return _objectType;
            }
            set
            {
                _objectType = value;
            }
        }
    }
}

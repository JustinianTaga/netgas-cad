﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OPENTK
{
    public partial class AddSubnetwork : Form
    {
        Form1 mainWindow;
        public AddSubnetwork(Form1 _mainWindow)
        {
            InitializeComponent();
            mainWindow = _mainWindow;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach(Node n in mainWindow.Nodes)
            {
                n.Subnetwork = textBox1.Text;
            }
            foreach(Pipe p in mainWindow.Pipes)
            {
                p.Subnetwork = textBox1.Text;
            }

            MessageBox.Show("Subsystem set successfully for all existing elements!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            button2.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!mainWindow.Subnetworks.Contains(textBox1.Text))
            {
                mainWindow.Subnetworks.Add(textBox1.Text);
                button2.Enabled = true;
                MessageBox.Show("Subsystem added successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Cannot add subsystem \"" + textBox1.Text + "\"!\nSubsystem already exists!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

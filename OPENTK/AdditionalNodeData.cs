﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace OPENTK
{
    public class AdditionalNodeData
    {
        public Hashtable Properties = new Hashtable();
        int _id;
        string _objectType = "Node";
        
        public enum EnterExitType
        {
            None,
            Exit,
            Enter
        }

        EnterExitType _ENEX = EnterExitType.None;


        [CategoryAttribute("Node data")]
        [DisplayName("ID")]
        [Description("ID of the selected Node")]
        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }


       
        [CategoryAttribute("Node data")]
        [DisplayName("Object Type")]
        [Description("Type of the selected object")]
        public string ObjectType
        {
            get
            {
                return _objectType;
            }
            set
            {
                _objectType = value;
            }
        }
        [CategoryAttribute("Node type (Enter / Exit)")]
        [DisplayName("Node type")]
        [Description("Node type (Enter / Exit")]
         public EnterExitType currentEnterExit
        {
            get
            {
                return _ENEX;
            }
            set
            {
                _ENEX = value;
            }
        }
    }
}

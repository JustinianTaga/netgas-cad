﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OPENTK
{
    public class Layer
    {
        public string Name;
        public int Index;
        public bool isSelected = false;
        public bool isVisible = true;
        public Layer(int _Index, string _Name)
        {
            Name = _Name;
            Index = _Index;
        }
    }
}

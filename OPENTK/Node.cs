﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OPENTK
{
    public class Node
    {
        private string _subnetwork = "";
        public Vector2 auxVector;
        private string _Name = "";
        public Layer layer;
        public bool DrawText = true;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }
        private bool _drawShadow = false;
        public bool DrawShadow
        {
            get
            {
                return _drawShadow;
            }
            set
            {
                _drawShadow = value;
            }
        }

        private Vector2 _vector;
        public float PointSize = 9f;
        public AdditionalNodeData Data = new AdditionalNodeData();
        public Vector2 Vector
        {
            get
            {
                return _vector;
            }
            set
            {
               _vector = value;
            }
        }
        
        public int Index;

        public string Subnetwork
        {
            get
            {
                return _subnetwork;
            }
            set
            {
                _subnetwork = value;
            }
        }
        private System.Drawing.Font _font = null;
        public System.Drawing.Font Font
        {
            get
            {
                return _font;
            }
            set
            {
                _font = value;
            }
        }
        public Node(Vector2 vector2, int ID)
        {
            Vector = vector2;
            Index = ID;
            auxVector = vector2;
            Data.ID = ID;
        }

    }
    
}

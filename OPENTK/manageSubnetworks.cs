﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OPENTK
{
    public partial class manageSubnetworks : Form
    {
        Form1 mainWindow;
        public manageSubnetworks(Form1 _mainWindow)
        {
            InitializeComponent();
            this.mainWindow = _mainWindow;
            reloadCombobox();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string oldName = comboBox1.SelectedItem.ToString();
            mainWindow.Subnetworks[mainWindow.Subnetworks.IndexOf(comboBox1.SelectedItem.ToString())] = textBox1.Text;
            reloadCombobox();
            textBox1.Text = "";
            MessageBox.Show("Network " + oldName + " modified successfully!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void reloadCombobox()
        {
            if(mainWindow.Subnetworks.Count == 0)
            {
                comboBox1.Enabled = false;
                comboBox1.Items.Add("No subnetworks existing");
                comboBox1.SelectedIndex = 0;
                textBox1.Enabled = false;
                button1.Enabled = false;
            }
            else
            {
                comboBox1.Items.Clear();
                comboBox1.Items.Clear();
                textBox1.Enabled = true;
                foreach (string subName in this.mainWindow.Subnetworks)
                {
                    comboBox1.Items.Add(subName);
                }
                comboBox1.SelectedIndex = 0;
                button1.Enabled = true;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string oldName = comboBox1.SelectedItem.ToString();
            mainWindow.Subnetworks.RemoveAt(mainWindow.Subnetworks.IndexOf(comboBox1.SelectedItem.ToString()));
            MessageBox.Show("Network " + oldName + " removed successfully!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OPENTK
{
    public partial class layerComponent : UserControl
    {
        private string _Text;
        private bool _checkState = false, _isSelected = false;
        private Form1 mainWindow;
        public string Text
        {
            get
            {
                return _Text;
            }
            set
            {
                _Text = value;
                this.label1.Text = _Text;
            }
        }

        public bool CheckState
        {
            get
            {
                return _checkState;
            }
            set
            {
                _checkState = value;
                this.checkBox1.Checked = value;
            }
        }
        public bool isSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
            }
        }
        public layerComponent(Form1 _mainWindow, string text, bool checkstate, bool isSelected)
        {
            InitializeComponent();

            mainWindow = _mainWindow;
            this.Width = mainWindow.LayersContainer.Width - 5;
            _Text = text;
            _checkState = checkstate;
            this.checkBox1.Checked = checkstate;
            this.label1.Text = text;
            if (isSelected) this.BackColor = Color.LightGray;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            if (isSelected)
            {
                isSelected = false;
                this.BackColor = SystemColors.Control;
                this.mainWindow.Layers.Where(item => item.Name == Text).Single().isSelected = false;
            }
            else
            {
                isSelected = true;
                this.BackColor = Color.LightGray;
                foreach(Layer layer in this.mainWindow.Layers)
                {
                    layer.isSelected = false;
                }
                this.mainWindow.Layers.Where(item => item.Name == Text).Single().isSelected = true;
                mainWindow.loadLayersList();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.mainWindow.Layers.Where(item => item.Name == Text).Single().isVisible = checkBox1.Checked;
            this.mainWindow.GLControlInvalidate();
        }

    }
}
